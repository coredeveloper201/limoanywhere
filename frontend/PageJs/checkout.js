var journyDiscrption = window.localStorage.getItem("rateSetInformation");
if (typeof(journyDiscrption) == 'string') {
    journyDiscrption = JSON.parse(journyDiscrption);

}

if (journyDiscrption.serviceType == 'AIRA' || journyDiscrption.serviceType == 'AIRD') {

    $("#service_name").attr("placeholder", "Airline Name *");
    $("#service_no").attr("placeholder", "Flight # *");
    $("#service_from").attr("placeholder", "Originating From");
    $("#service_note").attr("placeholder", "Flight/Pick Up Note");

} else if (journyDiscrption.serviceType == 'SEAA' || journyDiscrption.serviceType == 'SEAD') {

    $("#service_name").attr("placeholder", "Cruise liner family name *");
    $("#service_no").attr("placeholder", "Cruise ship name *");
    $("#service_from").attr("placeholder", "Originating From");
    $("#service_note").attr("placeholder", "Seaport Pick Up Note");

} else if (journyDiscrption.serviceType == 'FTS' || journyDiscrption.serviceType == 'TTS') {

    $("#service_name").attr("placeholder", "Train Name *");
    $("#service_no").attr("placeholder", "Train No *");
    $("#service_from").attr("placeholder", "Originating From");
    $("#service_note").attr("placeholder", "Train Pick Up Note");
} else if (journyDiscrption.serviceType == 'HRLY') {

    $("#service_name").css("display", "none");
    $("#service_from").css("display", "none");
    $("#service_no").css("display", "none");

} else if (journyDiscrption.serviceType == 'PTP') {

    $("#service_name").css("display", "none");
    $("#service_from").css("display", "none");
    $("#service_no").css("display", "none");
    $("#service_from_div").css("display", "none");
    $("#service_note").attr("placeholder", "Notes/Comments");
}


/*write year for the cardtype year selection*/
var current_year = new Date().getFullYear('yy').toString().substr(2, 2);
var select_year = '<option value>YY</option>';
for (i = 0; i <= 10; i++) {

    select_year += '<option value=' + current_year + '>' + current_year + '</option>';
    current_year++;
}

$('#cardYear').html(select_year);

/*fill the payment details*/

var getPaymentdetails = localStorage.getItem('card_details');

if (getPaymentdetails != null) {

    if (typeof(getPaymentdetails) == 'string') {

        getPaymentdetails = JSON.parse(getPaymentdetails);
    }


    $('#paymentType').val(getPaymentdetails.payment_type);
    $('#cardNumber').val(getPaymentdetails.cardNumber);
    $('#cardMonth').val(getPaymentdetails.cardMonth);
    $('#cardYear').val(getPaymentdetails.cardYear);
    $('#cardCcv').val(getPaymentdetails.cardccvNumber);
    $('#cardHolderName').val(getPaymentdetails.cardHolderName);
    $('#cardHolderAddress').val(getPaymentdetails.cardHolderAddress);
    $('#cardHolderZipCode').val(getPaymentdetails.cardHolderZipCode);

    var getCreaditSurcharge = localStorage.getItem('creadit_card_surcharg');

    var totalAmount = $('.totalPassengerAmount').html().replace('$', '');


    if (getPaymentdetails.payment_type == 'CC') {
        $('#payment_disclaimer').css('display', 'none');
        if (getCreaditSurcharge != null) {

            var totalAmout1 = (parseFloat(totalAmount) * getCreaditSurcharge) / 100;
            getCreaditSurcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

            var responseObjHTML = "<tr class='child creaditSurcharge' id='creaditCharge' ><td >Credit Card Surcharge</td><td class='creaditSurcharge_value text-right' >" + getCreaditSurcharge + "</td></tr>";
            //totalAmount=parseFloat(totalAmount+getCreaditSurcharge);

            totalAmount = parseFloat(Math.round((parseFloat(totalAmount) + parseFloat(getCreaditSurcharge)) * 100) / 100).toFixed(2);
            $('#passengerSelectVehicleRate').append(responseObjHTML);
            $('.totalPassengerAmount').html("$" + totalAmount);

        }


    } else {

        if (getPaymentdetails.payment_type == 'CASH') {

            $('#payment_disclaimer').css('display', 'block');
        } else {

            $('#payment_disclaimer').css('display', 'none');
        }


        if ($('#creaditCharge').length) {
            $('.creaditSurcharge').remove();
            var totalAmout1 = (parseFloat(totalAmount) * getCreaditSurcharge) / 100;
            getCreaditSurcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);

            totalAmount = parseFloat(Math.round((parseFloat(totalAmount) - parseFloat(getCreaditSurcharge)) * 100) / 100).toFixed(2);
            $('.totalPassengerAmount').html("$" + totalAmount);

        }

    }


}

/*Change The Book Button Text According To Select The Radio Buttons*/
$('input[type=radio][name=selectradio]').change(function () {

    var selected_radio = $('input[name=selectradio]:checked').val();

    if (selected_radio == 'No') {
        $('.passengerReservationBook').text('Submit and Finalize');
    } else if (selected_radio == 'Yes') {
        $('.passengerReservationBook').text('Book & Add Trip');
    }

});


$('[data-toggle="popover"]').popover();
var getChildValue = localStorage.getItem('isChildRide');

if (getChildValue) {
    $('#riding_child_btn').show();
} else {
    $('#riding_child_btn').hide();
}

$('#bckButton').on("click", function () {

    window.location.href = "select-vehicle.html";

});

/*click on the book the trip button*/
$('.passengerReservationBook').on("click", function () {

    $('#refresh_overlay').css('display', 'block');
    var firstName = $('#passengerFirstName').val();

    var refrenceNo = $('#reference').val();
    var otherComment = $('#otherComment').val();
    var grpname = $('#grpname').val();

    var lastName = $('#passengerLastName').val();
    var pass_full_name = firstName + ' ' + lastName;
    // var passengerMobileNumber = $('#passengerMobileNumber').val();
    var passengerMobileNumber = $("#passengerMobileNumber").intlTelInput("getNumber", intlTelInputUtils.numberFormat.E164);
    var passengerEmailName = $('#passengerEmailName').val();
    var numberOfPassengerInput = $('.numberOfPassenger').val();


    var customer_info = {
        'first_name': firstName,
        'last_name': lastName,
        'mobile_no': passengerMobileNumber,
        'email_address': passengerEmailName
    };

    localStorage.setItem("first_customer_info", JSON.stringify(customer_info));
    var promocode = '';
    var promoType = ''
    var offercode = [];
    var jurneyDetail = window.localStorage.getItem("rateSetInformation");
    var getJorney = '';
    if (typeof(jurneyDetail) == 'string') {

        getJorney = JSON.parse(jurneyDetail);

    }

    /*service type details*/
    var service_name = $('#service_name').val();

    if (service_name == '') {
        if (getJorney.serviceType == 'AIRA') {

            alert('start validation');
            alert('Please fill Airport line or FBO name.');

            $('#refresh_overlay').css('display', 'none');
            return false;
        } else if (getJorney.serviceType == 'SEAA') {

            alert('Please fill Cruise liner family name.');

            $('#refresh_overlay').css('display', 'none');
            return false;
        } else {


        }


    }

    var service_number = $('#service_no').val();

    if (service_number == '') {
        if (getJorney.serviceType == 'AIRA') {

            alert('Please fill Flight or FBO Tail No.');

            $('#refresh_overlay').css('display', 'none');
            return false;
        } else if (getJorney.serviceType == 'SEAA') {

            alert('please fill Cruise liner Cruise ship name.');

            $('#refresh_overlay').css('display', 'none');
            return false;

        } else {


        }

    }
    var service_from = $('#service_from').val();
    var service_note = $('#service_note').val();


    if (firstName == '') {
        alert('Please type your first name');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }
    if (lastName == '') {
        alert('Please type your last name');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }
    if (passengerEmailName == '') {
        alert('Please type your email address');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }

    var discount_amount = 0;


    var discount_amount = 0;
    if ($('.discountAmount').html()) {
        discount_amount = $('.discountAmountStart').html();
        promocode = $('#promocode').val();

        if ($('.vouchercheck').is(":checked")) {
            offercode = {"offertype": 1, "promocode": promocode};
        } else {
            offercode = {"offertype": 2, "promocode": promocode};

        }


    }

    var specl_package_discount = 0;
    var spcl_package_code = '';
    if ($('.specialPackageDiscount1').html()) {

        specl_package_discount = $('#discountAmount').text();
        spcl_package_code = localStorage.getItem('add_on_promo_code');
    }

    var specil_package_name = '';

    if ($('.SpecialPckData').text()) {

        specil_package_name = $('.SpecialPckData').text();
    }

    var specialAmount = 0;

    if ($('.specialAmount').html()) {

        specialAmount = $('.specialAmountStart').html();
    }

    var add_onDiscount = 0;

    if ($('.specialPackageDiscount').html()) {

        add_onDiscount = $('#specialPackageAmount').text();
    }


    var cncierge_fee = 0;
    if ($('.concirge_fee').html()) {

        cncierge_fee = $('.concirge_fee').html();

    }

    var greeterfee = 0;
    if ($('.greeterfee').html()) {

        greeterfee = $('.greeterfee').html();

    }

    var total_amount = $('.totalPassengerAmount').html();

    var chiltsheet_type = '';
    if ($('#childValueStorhere').text()) {

        chiltsheet_type = $('#childValueStorhere').text();
    }

    var childSeatValue = 0;
    if ($('.childSeatExtraRate').html()) {

        childSeatValue = $('.childSeatExtraRate').html();

    }

    var credtitCharge = 0;

    if ($('.creaditSurcharge_value').html()) {

        credtitCharge = $('.creaditSurcharge_value').html();
    }


    var getChild = localStorage.getItem('total_child');
    if (getChild != null) {

        var totalChildSeat = localStorage.getItem('total_child');

    }
    var specialPackage = 0;
    if ($('.specialAmountStart').html()) {

        specialPackage = $('.specialAmountStart').html();
    }

    var specialPackageDiscount = 0;

    if ($('.specialPackageDiscount').html()) {

        specialPackageDiscount = $('#specialPackageAmount').html();
    }

    var child_store_value = '';
    if ($('#childValueStorhere').html()) {

        child_store_value = $('#childValueStorhere').html();
    }

    var additionalPassengerDetail = [];
    if(intlTelInputValid == 0){
        alert('Invalid Number. Please insert valid one.');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }
    if ($('#passenger_details').html() != '') {
        var i = 0;
        $('.add_passenger_info_div').each(function (index, result) {

            var FName = $(this).find('.firstname_' + i).html();
            var LName = $(this).find('.lastname_' + i).html();
            var MNumber = $(this).find('.mobilenumber_' + i).html();
            var Email = $(this).find('.email_id_' + i).html();
            additionalPassengerDetail.push({
                "firstname": FName,
                "lastname": LName,
                "MNumber": MNumber,
                "Email_id": Email
            });
            i++;
        });
    }

    localStorage.setItem("extra_customer_info", JSON.stringify(additionalPassengerDetail));

    var cardType = $('#paymentType').val();

    if (cardType == '') {

        alert('Please Select Payment Type');

        $('#refresh_overlay').css('display', 'none');
        return false;
    }


    var cardNumber = $('#cardNumber').val();
    if (cardNumber == '') {
        alert('Please Fill Card Number');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }


    var cardMonth = $('#cardMonth').val();
    if (cardMonth == '') {
        alert('Please Select Month');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }

    var cardCcv = $('#cardCcv').val();
    if (cardCcv == '') {
        alert('Please Enter ccv number');
        return false;
        $('#refresh_overlay').css('display', 'none');
    }


    var current_year = new Date().getFullYear();
    var current_month = new Date().getMonth();
    var cardYear = $('#cardYear').val();
    if (cardYear == '') {
        alert('Please Select Year');
        $('#refresh_overlay').css('display', 'none');
        return false;
    }

    if (cardYear == current_year) {
        if (current_month > cardMonth) {
            alert('Card Expiry Month should be greater or equal to current month');
            $('#refresh_overlay').css('display', 'none');
            return false;
        }
    }

    var cardHolderName = $('#cardHolderName').val();

    if (cardHolderName == '') {

        alert('Please Fill Card Holder Name');

        $('#refresh_overlay').css('display', 'none');
        return false;
    }
    var cardHolderAddress = $('#cardHolderAddress').val();

    if (cardHolderAddress == '') {

        alert('Please Fill Address');

        $('#refresh_overlay').css('display', 'none');
        return false;
    }
    var cardHolderZipCode = $('#cardHolderZipCode').val();

    if (cardHolderZipCode == '') {

        alert('Please fill zip code');

        $('#refresh_overlay').css('display', 'none');
        return false;
    }


    /*============================================*/
    var cardTotalValue;
    if (cardType) {
        cardNumber = $('#cardNumber').val();
        cardMonth = $('#cardMonth').val();
        cardYear = $('#cardYear').val();
        cardHolderName = $('#cardHolderName').val();
        cardHolderAddress = $('#cardHolderAddress').val();
        cardHolderZipCode = $('#cardHolderZipCode').val();
        cardccvNumber = $('#cardCcv').val();
        cardTotalValue = {
            "payment_type": cardType,
            "cardNumber": cardNumber,
            "cardMonth": cardMonth,
            "cardYear": cardYear,
            "cardHolderName": cardHolderName,
            "cardHolderAddress": cardHolderAddress,
            "cardHolderZipCode": cardHolderZipCode,
            "cardccvNumber": cardccvNumber
        };

        localStorage.setItem('card_details', JSON.stringify(cardTotalValue));
    }


    var grandTotal = $('.totalPassengerAmount').html();
    grandTotal = grandTotal.replace("$", "");
    var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");

    if (typeof(getLocalStorageValue) == 'string') {

        getLocalStorageValue = JSON.parse(getLocalStorageValue);
    }

    var admin_email = getLocalStorageValue[0].email;


    var bookingInfo = window.localStorage.getItem("bookingInfo");
    var select_term = $('input[name=check_box]:checked').val();
    if (select_term != 'term_condition') {
        alert('Please select terms and conditions');

        $('#refresh_overlay').css('display', 'none');
        return false;
    }

    /*
             if (cardType != 'CASH') {
                 alert(1);
                 $("#stripe_submit").click();
                 alert(2);
                 setTimeout(function() {

                     var stripeToken = $("#stripeToken").val();

                     var total_price = $('.totalPassengerAmount').html().replace('$', '');



                     var jsonData = { "action": "passengerReservation", "jurneyDetail": jurneyDetail, "firstName": firstName, "lastName": lastName, "passengerMobileNumber": passengerMobileNumber, "passengerEmailName": passengerEmailName, "service_name": service_name, "service_number": service_number, "service_from": service_from, "service_note": service_note, "numberOfPassengerInput": numberOfPassengerInput, "bookingInfo": bookingInfo, "grandTotal": grandTotal, "cardTotalValue": cardTotalValue, "additionalPassengerDetail": additionalPassengerDetail, "refrenceNo": refrenceNo, "otherComment": otherComment, "grpname": grpname, "offercode": offercode, "childSeatValue": childSeatValue, "totalChild": totalChildSeat, "specialPackage": specialPackage, "specialPackageDiscount": specialPackageDiscount, "extrachildSheet": child_store_value, "paymentType": cardType, "stripeToken": stripeToken, "total_price": total_price };

                     if (typeof(getLocalStorageValue) == 'string') {

                         getLocalStorageValue = JSON.parse(getLocalStorageValue);
                     }

                     var _ServerpathVehicle = _SERVICEPATHSERVICECLIENT;

    console.log("jsonData....",jsonData);


                     $.ajax({
                         url: _SERVICEPATHSERVICECLIENT,
                         type: 'POST',
                         data: jsonData,
                         success: function(response) {

                            var responseObj = response;

                            if (typeof(response) == "string" && response != 'payment_failed') {
                                 responseObj = JSON.parse(response);
                             }

                                 var selected_radio = $('input[name=selectradio]:checked').val();
                                 localStorage.removeItem('getQuete_exit');
                                 localStorage.setItem('return_trip', selected_radio);
                                 localStorage.setItem('trip_confirmation', responseObj.data.confirmation_number);
                                 sendMessage(bookingInfo, firstName, jsonData);
                                 sendHtmlEmail(admin_email, pass_full_name, passengerMobileNumber, grpname, cardHolderName, cardType, service_note, chiltsheet_type, childSeatValue, promocode, discount_amount, cncierge_fee, greeterfee, credtitCharge, specil_package_name, specialAmount, add_onDiscount, specl_package_discount, spcl_package_code, otherComment, total_amount, responseObj.data.confirmation_number, bookingInfo, jurneyDetail);


                             responseObj = JSON.parse(response);
                             if (responseObj.data.ResponseText == 'payment_failed') {
                                 alert('Payment failed please try again')
                                 $('#refresh_overlay').css('display', 'none');
                             }


                         },

                         error: function() {

                             alert("Some Error occur please submit again");
                             $('#refresh_overlay').css('display', 'none');

                         }
                     });



                 }, 2000);

             } else {

             */


    var jsonData = {
        "action": "passengerReservation",
        "jurneyDetail": jurneyDetail,
        "firstName": firstName,
        "lastName": lastName,
        "passengerMobileNumber": passengerMobileNumber,
        "passengerEmailName": passengerEmailName,
        "service_name": service_name,
        "service_number": service_number,
        "service_from": service_from,
        "service_note": service_note,
        "numberOfPassengerInput": numberOfPassengerInput,
        "bookingInfo": bookingInfo,
        "grandTotal": grandTotal,
        "cardTotalValue": cardTotalValue,
        "additionalPassengerDetail": additionalPassengerDetail,
        "refrenceNo": refrenceNo,
        "otherComment": otherComment,
        "grpname": grpname,
        "offercode": offercode,
        "childSeatValue": childSeatValue,
        "totalChild": totalChildSeat,
        "specialPackage": specialPackage,
        "specialPackageDiscount": specialPackageDiscount,
        "extrachildSheet": child_store_value,
        "paymentType": cardType
    };

    if (typeof(getLocalStorageValue) == 'string') {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
    }
    var _ServerpathVehicle = _SERVICEPATHCLIENT;


    var shortInfo = JSON.parse(localStorage.rateSetInformation);
    jsonData['user_id'] = shortInfo.user_id;
    var BookingInfo = JSON.parse(localStorage.bookingInfo);
    jsonData['paymentGateway'] = {
        service_type: BookingInfo.updateVehicleInfoservices,
        vehicle_code: BookingInfo.vehicle_code,
        pickUp: BookingInfo.pickuplocation,
        dropOff: BookingInfo.dropupaddress,
        pickUp_code: '0',
        dropOff_code: '0',
        action: 'getPaymentSetup'
    };
    // console.log(JSON.stringify(jsonData));
    $.ajax({
        url: _SERVICEPATHSERVICECLIENT,
        type: 'POST',
        data: jsonData,
        success: function (response) {


            var responseObj = response;


            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }


            var selected_radio = $('input[name=selectradio]:checked').val();
            localStorage.removeItem('getQuete_exit');
            localStorage.setItem('return_trip', selected_radio);
            if(responseObj.data !== undefined){
                var confirmation_number = responseObj.data.confirmation_number;
                localStorage.setItem('trip_confirmation', responseObj.data.confirmation_number);
            }
            if(responseObj.confirmation_number !== undefined){
                var confirmation_number = responseObj.confirmation_number;
                localStorage.setItem('trip_confirmation', responseObj.confirmation_number);
            }
            //sendMessage(bookingInfo, firstName, jsonData);
            sendHtmlEmail(admin_email, pass_full_name, passengerMobileNumber, grpname, cardHolderName, cardType, service_note, chiltsheet_type, childSeatValue, promocode, discount_amount, cncierge_fee, greeterfee, credtitCharge, specil_package_name, specialAmount, add_onDiscount, specl_package_discount, spcl_package_code, otherComment, total_amount, confirmation_number, bookingInfo, jurneyDetail, service_name, service_number, service_from, additionalPassengerDetail, child_store_value);


        },

        error: function () {

            alert("Some Error occur please submit again");
            $('#refresh_overlay').css('display', 'none');

        }
    });


    /* } */


});


/*function to call send message to admin*/

function sendMessage(bookingInfo, passenger_name, jsonData) {

    var get_email_notfy = localStorage.getItem('email_notify_data');

    if (get_email_notfy != null) {

        if (typeof(get_email_notfy) == 'string') {

            get_email_notfy = JSON.parse(get_email_notfy);

        }

    }
    var messageData = {
        "email_notify": get_email_notfy,
        "bookingInfo": bookingInfo,
        "passenger_name": passenger_name,
        "jsonData": jsonData
    };
    $.ajax({
        url: "phpfile/admin_message.php",
        type: 'POST',
        data: messageData,
        success: function (response) {

            //$('#refresh_overlay').css('display','none');
            //localStorage.removeItem('email_notify_data');
            //window.location.href="thank_you.html";
        },

        error: function () {

            alert("Some Error occur please submit again");
            $('#refresh_overlay').css('display', 'none');

        }
    });

}

var abcd = '';

/*function to call send email to passenger*/

function sendHtmlEmail(paseengerEmail, passenger_name, passenger_phone, group_name, booking_con, payment_type, service_note, chiltsheet_type, childSeatValue, promocode, discount_amount, cncierge_fee, greeterfee, credtitCharge, specil_package_name, specialAmount, add_onDiscount, specl_package_discount, spcl_package_code, otherComment, total_amount, reservationId, bookingInfo, jurneyDetail, service_name, service_number, service_from, additionalPassengerDetail, child_store_value) {
    $('#refresh_overlay').css('display', 'block');
    var special_promo_code = localStorage.getItem('spcl_package_promo');
    var auto_promo_code = localStorage.getItem('auto_promo_code');
    var get_email_notfy = localStorage.getItem('email_notify_data');
    var luggageJson = localStorage.getItem('luggageJson');
    luggageJson = JSON.parse(luggageJson);
    if (get_email_notfy != null) {

        if (typeof(get_email_notfy) == 'string') {

            get_email_notfy = JSON.parse(get_email_notfy);

        }

    }


    var bookerDetail = [];
    if ($('#booker_details').html() != '') {
        var i = 0;
        $('.booker_info_div').each(function (index, result) {

            var FName = $(this).find('.firstname_' + i).html();
            var LName = $(this).find('.lastname_' + i).html();
            var MNumber = $(this).find('.mobilenumber_' + i).html();
            var Email = $(this).find('.email_id_' + i).html();
            bookerDetail.push({"firstname": FName, "lastname": LName, "MNumber": MNumber, "Email_id": Email});
            i++;
        });
    }


    //return false;

    promocode = ( $('#promocode').length == 0 ) ? '' : $('#promocode').val();
    var addon_promocode = ( $('#special_promocode').length == 0 ) ? '' : $('#special_promocode').val();

    specil_package_name = '';
    var specialPckData = $('.SpecialPckData');
    var length = specialPckData.length;
    specialPckData.each(function (index, element) {
        var package_id = $(this).attr('package_id');
        var pack_id = package_id.split('_')[0];
        specil_package_name = specil_package_name + pack_id + '---' + $(this).find('span:first').text();

        if (index != (length - 1)) { // if not last item
            specil_package_name = specil_package_name + '@@@';
        }
    });

    var refrenceNo = $('#reference').val();

    var emailData = {
        "passenger_email": paseengerEmail,
        "small": luggageJson.small,
        "medium": luggageJson.medium,
        "large": luggageJson.large,
        "passenger_name": passenger_name,
        "passenger_phone": passenger_phone,
        "group_name": group_name,
        "booking_contact": booking_con,
        "payment_type": payment_type,
        "service_note": service_note,
        "chiltsheet_type": chiltsheet_type,
        "childSeatValue": childSeatValue,
        "promocode": promocode,
        addon_promocode: addon_promocode,
        "discount_amount": discount_amount,
        "cncierge_fee": cncierge_fee,
        "greeterfee": greeterfee,
        "credtitCharge": credtitCharge,
        "specil_package_name": specil_package_name,
        "specialAmount": specialAmount,
        "add_onDiscount": add_onDiscount,
        "specl_package_discount": specl_package_discount,
        "spcl_package_code": spcl_package_code,
        "otherComment": otherComment,
        "special_promo_code": special_promo_code,
        "auto_promo_code": auto_promo_code,
        "total_amount": total_amount,
        "trip_id": reservationId,
        "email_notify": get_email_notfy,
        "bookingInfo": bookingInfo,
        "jurneyDetail": jurneyDetail,
        "service_name": service_name,
        "service_number": service_number,
        "service_from": service_from,
        "additionalPassengerDetail": additionalPassengerDetail,
        "bookerDetail": bookerDetail,
        "additional_child_seat": child_store_value,
        "refrenceNo": refrenceNo
    };
    $.ajax({
        url: "phpfile/passenger_email.php",
        type: 'POST',
        data: emailData,
        success: function (response) {

            $('#refresh_overlay').css('display', 'none');
            //localStorage.removeItem('email_notify_data');
            window.location.href = "thank_you.html";
        },

        error: function () {

            alert("Some Error occur please submit again");
            $('#refresh_overlay').css('display', 'none');

        }
    });

}


$('#addPackage').on('click', function () {


    $('#specialPackageRequest').modal("show");

})

/*submit button end*/

$('#riding_child_btn').on("click", function () {
    $('#myModal1').modal("show");
});
$('#childSeatEdit').on("click", function () {
    $('#childSeatFrom')[0].reset();
    var getChildFieldValue = $('#childValueStorhere').html();
    var getChildFieldValueArray = getChildFieldValue.split(',');
    $.each(getChildFieldValueArray, function (index, result) {
        var resultArray = result.split('(');
        if (/^[a-zA-Z0-9- ]*$/.test(resultArray[1]) == false) {
            resultArray[1] = resultArray[1].replace(')', '');
        }
        $('#childSeatDiv' + (index + 1)).show();
        $('#typeChildSeat' + (index + 1)).val(resultArray[0]).show();
        $('#qntSeats' + (index + 1)).val(resultArray[1]).show();

    });
    $('#myModal1').modal('show');


});


//add the concerse feee

var concerse_fees = localStorage.getItem('concierge_fee');

if (concerse_fees != null) {
    $('#concerse_fee').show();
    $('#concerge_amount').html(concerse_fees);
    var getOptional_content = localStorage.getItem('optional_greeter_content');

    if (getOptional_content != null) {

        $('#qr_disclaimer').attr("data-content", getOptional_content);
    }

} else {

    $('#concerse_fee').hide();
}

var seaport_fee = localStorage.getItem('seaport_fee');

if (seaport_fee != null) {
    $('#seaport_fee').show();
    $('#seaport_amount').html(seaport_fee);
} else {

    $('#seaport_fee').hide();
}

$('input[type=checkbox][name=passenger]').change(function () {
    if ($(this).is(":checked")) {
        $('#airlinecode').hide();
    } else if ($(this).is(":not(:checked)")) {
        $('#airlinecode').show();
    }
    /* if (this.value == 'flightdetail') {
         $('#airlinecode').hide();
     }
     else{
         $('#airlinecode').show();
     }*/
});
$('#special_request_btn').on("click", function () {
    $('#specialRequest').modal();
});
var paytp_count = 0;
$('#paymentType').change(function () {
    var option = $(this).val();
    console.log(option);
    if(option !== ''){
        console.log('here');
        $('.cash_dislaimer_box').hide();
        var getCreaditSurcharge = localStorage.getItem('creadit_card_surcharg');
        var totalAmount = $('.totalPassengerAmount').html().replace('$', '');
        if (option == 'CC' || option == 'OTHER') {
            paytp_count++;
            if (option == 'CC') {
                $("#creditCardsGroup").show();
            }
            else {
                $("#creditCardsGroup").hide();
            }


            if (paytp_count == 1) {
                $('#payment_disclaimer').css('display', 'none');
                if (getCreaditSurcharge != null) {
                    $('.creaditSurcharge').show();
                    if ($('.creaditSurcharge').length > 0) {

                        var totalAmout1 = (parseFloat(totalAmount) * getCreaditSurcharge) / 100;
                        getCreaditSurcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                        // var responseObjHTML = "<tr class='child creaditSurcharge' id='creaditCharge' ><td >Credit Card Surcharge</td><td class='creaditSurcharge_value text-right' >" + totalAmout1.toFixed(2) + "</td></tr>";
                        totalAmount = parseFloat(Math.round((parseFloat(totalAmount) + parseFloat(getCreaditSurcharge)) * 100) / 100).toFixed(2);
                        // $('#passengerSelectVehicleRate').append(responseObjHTML);
                        $('.totalPassengerAmount').html("$" + totalAmount);
                    } else {
                        var totalAmout1 = (parseFloat(totalAmount) * getCreaditSurcharge) / 100;
                        getCreaditSurcharge = parseFloat(Math.round(totalAmout1 * 100) / 100).toFixed(2);
                        var responseObjHTML = "<tr class='child creaditSurcharge' id='creaditCharge' ><td >Credit Card Surcharge</td><td class='creaditSurcharge_value text-right' >" + totalAmout1.toFixed(2) + "</td></tr>";
                        totalAmount = parseFloat(Math.round((parseFloat(totalAmount) + parseFloat(getCreaditSurcharge)) * 100) / 100).toFixed(2);
                        $('#passengerSelectVehicleRate').append(responseObjHTML);
                        $('.totalPassengerAmount').html("$" + totalAmount);


                    }


                }
            }
            var deposit_amt = $('#deposit_amt').val();
            if ($('#deposit_amt_type').val() == 'pcntg') {
                var total_last_pcntg = (totalAmount * deposit_amt) / 100;
                var total_last = totalAmount - total_last_pcntg;
            } else {
                var total_last_pcntg = deposit_amt;
                var total_last = totalAmount - total_last_pcntg;
            }

            total_last = total_last.toFixed(2);

            total_last_pcntg = parseFloat(total_last_pcntg).toFixed(2);
            $('.deposit_total').html('$' + total_last_pcntg);
            $('.remaining_total').html('$' + total_last);


        }
        else {

            $('.deposit_total').html($('#total_last_pcntg').val());
            $('.remaining_total').html($('#total_last').val());
            $('.creaditSurcharge').hide();
            paytp_count = 0;


            var totalPassengerAmount = $('.totalPassengerAmount').html().replace('$', '');
            var creaditSurcharge_value = 0;
            if ($('.creaditSurcharge_value').length > 0) {

                creaditSurcharge_value = $('.creaditSurcharge_value').html().replace('$', '');

            }

            var totalAmount = parseFloat(totalPassengerAmount) - parseFloat(creaditSurcharge_value);
            totalAmount = totalAmount.toFixed(2);
            $('.totalPassengerAmount').html("$" + totalAmount);

            if (option == 'CASH') {
                var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");
                if (typeof(getLocalStorageValue) == 'string') {
                    getLocalStorageValue = JSON.parse(getLocalStorageValue);
                }
                var user_id = getLocalStorageValue[0].user_id;
                $.ajax({
                    url: _SERVICEPATHCLIENT,
                    type: 'POST',
                    data: "action=getDisclaimer&user_id=" + user_id,
                    success: function (response) {


                        if (typeof(response) == "string") {
                            response = JSON.parse(response);

                        }
                        console.log("response...", response);
                        if (response.code == 1002) {
                            $('.cash_dislaimer_box').show();

                            $('#cashDeclaimer').html(response.data[0].cash_disclamer);

                        }


                    }

                })
                // $('.card_payment').css('display', 'none');
                $('#payment_disclaimer').css('display', 'block');
            } else {

                $('.cash_dislaimer_box').hide();
                $('#cashDeclaimer').html('');
                /* $('.card_payment').css('display','block');*/
                /*$('#payment_disclaimer').css('display','none');*/
            }
            /* $('.deposit_total_tbl').css('display','none');
             $('.remaining_total_tbl').css('display','none');*/
            /*   if($('#creaditCharge').length){
               $('.creaditSurcharge').remove();
               var totalAmout1=(parseFloat(totalAmount)*getCreaditSurcharge)/100;
               getCreaditSurcharge= parseFloat(Math.round(totalAmout1 * 100)/100).toFixed(2);

               totalAmount=parseFloat(Math.round((parseFloat(totalAmount)-parseFloat(getCreaditSurcharge))* 100)/100).toFixed(2);
               $('.totalPassengerAmount').html("$"+totalAmount);

            }*/

        }
    }
});