 $('input[type=radio][name=optradio]').change(function() {
        if (this.value == 'yes') {
            $('#stopLoc').show();
        }
        else if (this.value == 'no') {
            $('#stopLoc').hide();
        }
    });
    $('#services').change(function()
    {
        
        var option = $(this).find('option:selected').val();
        if(option=='AIRA'){
            $('#intFlt').show();    
        }
        else{
            $('#intFlt').hide();
        }
    });
     
     
    $('#services').change(function()
    {
        
        var option = $(this).find('option:selected').val();
        if(option=='HRLY'){
            $('#triphr').show();
            $('#droploc').hide();
            $('#seadrop').hide();   
        }else if(option=='CH'){
            
            $('#triphr').show();
            $('#droploc').hide();
            $('#seadrop').hide();  
        }
        else if(option=='SEAA'){
            $('#seapick').show();
            $('#pickloc').hide();
            $('#seadrop').show();
            $('#droploc').hide();
            $('#triphr').hide();
        }else if(option=='SEAD'){

            $('#seapick').show();
            $('#pickloc').hide();
            $('#seadrop').show();
            $('#droploc').hide();
            $('#triphr').hide();

        }
         else if(option=='AIRD'){
            //$('.changeatt').attr('id', 'droplocation');
            //$('.changeid').attr('id', 'pickuplocation');
            $('#triphr').hide();
            $('#droploc').show();
            $('#seapick').hide();
            $('#to_airport_pickup').show();
            $('#seadrop').hide();
        }else{
            
            $('#triphr').hide();
            $('#droploc').show();
            $('#seapick').hide();
            $('#from_airport_pickup').show();
            $('#seadrop').hide();
        }
    });

        $('input[type=radio][name=map]').change(function() {
        if (this.value == 'yes') {
            $("#smallmap").hide();
            $("#bigmap").show();
            
        }
        if (this.value == 'no'){
            $("#smallmap").show();
            $("#bigmap").hide();          
        }

   }); 

var options = {
    types: ['(cities)'],
     componentRestrictions: {country: "us"}
	 
   };
          
           google.maps.event.addDomListener(window, 'load', function () {
                var places = new google.maps.places.Autocomplete(document.getElementById('droplocation'),options);
            });
			google.maps.event.addDomListener(window, 'load', function () {
               var places = new google.maps.places.Autocomplete(document.getElementById('dropseaport'),options);
             });
// 			 google.maps.event.addDomListener(window, 'load', function () {
//      var places = new google.maps.places.Autocomplete(document.getElementById('pickup_input'),options);
// });



var locationdata = [];
    
    $.ajax({
            url : "webservice/service.php",
            type : 'post',
            data : 'action=GetAirports',
            dataType : 'json',
            success : function(data){
                if(data.ResponseText == 'OK'){
                    $.each(data.Airports.Airport, function( index, locdata ){
                        locationdata.push(locdata.AirportCode+' ('+locdata.AirportName+')');
                    });
                  var locationData = locationdata;
                    $( "#pickuplocation" ).autocomplete({
                        source: locationData
                    });
                    

                }
          }});

getServiceTypes();
function getServiceTypes()
          {
            var serviceTypeData = [];
    $.ajax({
            url : "webservice/service.php",
            type : 'post',
            data : 'action=GetServiceTypes',
            dataType : 'json',
            success : function(data){
                //console.log(data);
                if(data.ResponseText == 'OK'){

                    var ResponseHtml='';
                    $.each(data.ServiceTypes.ServiceType, function( index, result){
                     ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                     console.log(result);
                                    });
                 
                 
                 
                 $('#services').html(ResponseHtml);
                   
                }
      }});
 }
 searportLocation(); 
 function searportLocation()
          {
            
            var locationdata1 = [];
    
    $.ajax({
            url : "webservice/service.php",
            type : 'post',
            data : 'action=GetSeaports',
            dataType : 'json',
            success : function(data){
                //console.log(data);
                if(data.ResponseText == 'OK'){
                    $.each(data.Seaports.Seaport, function( index, locdata ){
                        locationdata1.push(locdata.SeaportCode+' ('+locdata.SeaportName+')');
                    });
                  var seaport_locationdata1 = locationdata1;
                  console.log(seaport_locationdata1);
                    $( "#pickupseaport").autocomplete({
                        source: seaport_locationdata1
                    });
                }
          }});

 }

var mapLoc={

    mapContainer: document.getElementById('map-container'),
     map: null,


   init: function() {
    //var param=parseURLParams(window.location.href);
     alert('map');
    var latLng = new google.maps.LatLng(40.7479304, -73.987354);
       mapLoc.map = new google.maps.Map(mapLoc.mapContainer, {
       zoom: 13,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Show directions onload
    //Demo.getDirections();
  }
};
var getinfo={
       _Serverpath:"webservice/service_client.php",

getAirportName:function(){
 
 var getUserId=window.localStorage.getItem('companyInfo');



         if(typeof(getUserId)=="string")
                {   

                    getUserId=JSON.parse(getUserId);

                }

              var fd = new FormData();
                fd.append("action","getAirportName");
                fd.append("user_id",getUserId[0].id)

    $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData:false,
            contentType:false,
            data:fd
    }).done(function(result){

          console.log(result);
           alert(result);

   });

}


};
google.maps.event.addDomListener(window, 'load', mapLoc.init);
getinfo.getAirportName();