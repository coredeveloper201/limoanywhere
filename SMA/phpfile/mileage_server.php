<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
 * Method:             getRateMatrix()
 * InputParameter:        matrix_name
 * Return:             get Rate Matrix
 *****************************************************************/
function getRateMatrix()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query1 = "SELECT id,name from mileage_rate_matrix where user_id='" . $_REQUEST['user_id'] . "'";
        $resource1 = operations($query1);
        for ($j = 0; $j < count($resource1); $j++) {
            $arr[] = array(
                "id" => $resource1[$j]['id'],
                "name" => $resource1[$j]['name']
            );
        }
        if (count($arr) > 0 && gettype($arr) != "boolean") {
            $result = global_message(200, 1007, $arr);

        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             setMilesRate()
 * InputParameter:
 * Return:             set Miles Rate
 *****************************************************************/
function setMilesRate()
{
    if (isset($_REQUEST['sma_id']) && (isset($_REQUEST['vehicle_code'])) && (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) && (isset($_REQUEST['matrix_name']) && !empty($_REQUEST['matrix_name'])) && (isset($_REQUEST['add_miles']) && !empty($_REQUEST['add_miles'])) && (isset($_REQUEST['add_miles_rate']) && !empty($_REQUEST['add_miles_rate']))) {
        $userId = $_REQUEST['user_id'];
        $minimum_base_rate = (isset($_REQUEST['minimum_base_rate']) && !empty($_REQUEST['minimum_base_rate'])) ? $_REQUEST['minimum_base_rate'] : 0;
        $VehicleCode = explode(',', $_REQUEST['vehicle_code']);
        $addSma = explode(',', $_REQUEST['sma_id']);
        $addMiles = explode(',', $_REQUEST['add_miles']);
        $addMilesRate = explode(',', $_REQUEST['add_miles_rate']);
        $fixed_amount = explode(',', $_REQUEST['base_rate']);
        $miles_increase_percent = $_REQUEST['percent_increase'];

        $query = "insert into mileage_rate_matrix(name,user_id,miles_increase_percent,peak_hour_id,garage_to_garage,on_demand,garage_location) value('" . $_REQUEST['matrix_name'] . "','" . $userId . "','" . $miles_increase_percent . "','" . $_REQUEST['pickHrsDatabase'] . "','" . $_REQUEST['garage_to_garage'] . "','" . $_REQUEST['on_demand'] . "','" . $_REQUEST['garage_location'] . "')";
        $mileage_matrix_id = operations($query);
        for ($i = 0; $i < count($VehicleCode); $i++) {
            $Vehquery = "insert into mileage_vehicle(mileage_matrix_id,vehicle_code,user_id) value('" . $mileage_matrix_id . "','" . $VehicleCode[$i] . "','" . $userId . "')";
            $resource1 = operations($Vehquery);
        }
        for ($j = 0; $j < count($addSma); $j++) {
            $Smaquery = "insert into mileage_sma(mileage_matrix_id,sma_id,user_id) value('" . $mileage_matrix_id . "','" . $addSma[$j] . "','" . $userId . "')";
            $resource2 = operations($Smaquery);
        }
        for ($k = 0; $k < count($addMiles); $k++) {
            if ($k == 0) {
                $fromMiles = 0;
                $toMiles = $addMiles[$k];
            } else {
                $fromMiles = $addMiles[$k - 1] + 1;
                $toMiles = $addMiles[$k];
            }
            $Milesquery = "insert into rate_calculate_mileage(mileage_matrix_id,rate,from_mileage,to_mileage,fixed_amount,user_id) value('" . $mileage_matrix_id . "','" . $addMilesRate[$k] . "','" . $fromMiles . "','" . $toMiles . "','" . $fixed_amount[$k] . "','" . $userId . "')";
            $insertId = operations($Milesquery);
        }
        $result = global_message(200, 1008, $insertId);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             getRateMatrixList()
 * InputParameter:        user_id
 * Return:             get Rate Matrix List
 *****************************************************************/
function getRateMatrixList()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "Select * from mileage_rate_matrix where user_id='" . $_REQUEST['user_id'] . "' order by name asc";
        $resource = operations($query);
        $contents = array();
        if (count($resource) > 0 && gettype($resource) != "boolean") {
            for ($i = 0; $i < count($resource); $i++) {
                $vehicle_code = '';
                $sma_name = '';
                $sma_id = '';
                $Vehquery = "Select vehicle_code from mileage_vehicle where mileage_matrix_id=" . $resource[$i]['id'];
                $resource1 = operations($Vehquery);
                for ($j = 0; $j < count($resource1); $j++) {
                    $vehicle_code .= $resource1[$j]['vehicle_code'] . ',';
                }
                $Smaquery = "Select sma_id,sma_name from mileage_sma,sma where sma.id=mileage_sma.sma_id AND mileage_sma.mileage_matrix_id=" . $resource[$i]['id'];
                $resource2 = operations($Smaquery);
                for ($k = 0; $k < count($resource2); $k++) {
                    $sma_name .= $resource2[$k]['sma_name'] . ', ';
                    $sma_id .= $resource2[$k]['sma_id'] . ',';
                }
                $Minquery = "Select fixed_amount from rate_calculate_mileage where mileage_matrix_id=" . $resource[$i]['id'] . " AND from_mileage=0";
                $resource3 = operations($Minquery);
                $contents[$i]['id'] = $resource[$i]['id'];
                $contents[$i]['min_fare'] = $resource3[0]['fixed_amount'];
                $contents[$i]['sma_id'] = $sma_id;
                $contents[$i]['sma_name'] = $sma_name;
                $contents[$i]['vehicle_code'] = $vehicle_code;
                $contents[$i]['garage_to_garage'] = $resource[$i]['garage_to_garage'];
                $contents[$i]['on_demand'] = $resource[$i]['on_demand'];
                $contents[$i]['garage_location'] = $resource[$i]['garage_location'];
                $contents[$i]['matrix_name'] = $resource[$i]['name'];
                $contents[$i]['miles_increase_percent'] = $resource[$i]['miles_increase_percent'];
                $contents[$i]['peak_hour_id'] = $resource[$i]['peak_hour_id'];
            }

        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             getRateMatrixList()
 * InputParameter:        rate_matrix_id
 * Return:             get Rate Matrix List
 *****************************************************************/
function editRateMatrix()
{
    if ((isset($_REQUEST['rate_matrix_id']) && !empty($_REQUEST['rate_matrix_id']))) {
        $query = "Select * from rate_calculate_mileage where mileage_matrix_id=" . $_REQUEST['rate_matrix_id'];
        $resource = operations($query);
        $contents = array();
        for ($i = 0; $i < count($resource); $i++) {
            $contents[$i]['miles'] = $resource[$i]['to_mileage'];
            $contents[$i]['fixed_amount'] = $resource[$i]['fixed_amount'];
            $contents[$i]['rate'] = $resource[$i]['rate'];
        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             deleteMilesRate()
 * InputParameter:        rate_matrix_id
 * Return:             delete Miles Rate
 *****************************************************************/
function deleteMilesRate()
{
    if ((isset($_REQUEST['rate_matrix_id']) && !empty($_REQUEST['rate_matrix_id']))) {
        $rowId = $_REQUEST['rate_matrix_id'];
        $query = "delete from rate_calculate_mileage where mileage_matrix_id='" . $rowId . "'";
        $resource = operations($query);
        $queryDelete = "delete  from mileage_rate_matrix where id='" . $rowId . "'";
        $resource1 = operations($queryDelete);
        $queryDelete1 = "delete  from mileage_sma where mileage_matrix_id='" . $rowId . "'";
        $resource2 = operations($queryDelete1);
        $queryDelete2 = "delete  from mileage_vehicle where mileage_matrix_id='" . $rowId . "'";
        $resource3 = operations($queryDelete2);
        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             checkUniqueMatrix()
 * InputParameter:        rate_matrix_id
 * Return:             check Unique Matrix
 *****************************************************************/
function checkUniqueMatrix()
{
    $arr = [];
    $query1 = "Select * from mileage_rate_matrix where name='" . $_REQUEST['matrix_name'] . "' AND user_id='" . $_REQUEST['user_id'] . "'";
    $resource1 = operations($query1);
    for ($j = 0; $j < count($resource1); $j++) {
        $arr[] = array(
            "id" => $resource1[$j]['id']
        );
    }

    if(count($arr) == 0){
        $arr[] = array(
            'id' => null
        );
    }
    $result = global_message(200, 1007, $arr);
    return $result;
}