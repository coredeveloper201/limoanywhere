<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

	/*****************************************************************
	Method:             setCondDate()
	InputParameter:     cs_id
	Return:             setCondDate
	*****************************************************************/
	function setCondDate()
	{	
	 	if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	   	{
	      	$userId=$_REQUEST['user_id'];
		  	$start_time=(isset($_REQUEST['start_time']) && !empty($_REQUEST['start_time']))?$_REQUEST['start_time']:'12:00 AM';
		  	$end_time=(isset($_REQUEST['end_time']) && !empty($_REQUEST['end_time']))?$_REQUEST['end_time']:'11:59 PM';
			$end_time=explode(' ',$end_time);
		  	if($end_time[1]==pm){
				$end_time1=explode(':',$end_time[0]);
			  	if($end_time1[0]==12)
			  	{
				  	$end_time2=$end_time1[0];
		  		  	$end_time=$end_time2.":".$end_time1[1];
			  	}
			  	else
			  	{
				  	$end_time2=$end_time1[0]+12;
		  		  	$end_time=$end_time2.":".$end_time1[1];
			  	}
		  	}
		  	else
		  	{
	  			$end_time1=explode(':',$end_time[0]);
			  	if($end_time[0]==12)
		  		{		  		 
	  		 		$end_time="00:".$end_time1[1];
		  		}
		  		else
		  		{
		  			$end_time=$end_time[0];
		  		}
			}

		  	$start_time=explode(' ',$start_time);
		  	if($start_time[1]==pm)
		  	{
				$start_time1=explode(':',$start_time[0]);						  	
			  	if($start_time1[0]==12)
			  	{
				  	$start_time2=$start_time1[0];
		  		  	$start_time=$start_time2.":".$start_time1[1];
			  	}
			  	else
			  	{
				  	$start_time2=$start_time1[0]+12;
		  		  	$start_time=$start_time2.":".$start_time1[1];
			  	}
		  	}
		  	else
		  	{
		  		$start_time1=explode(':',$start_time[0]);
			  	if($start_time[0]==12)
			  	{			  		 
		  		 	$start_time="00:".$start_time1[1];
			  	}
		  		else
		  		{
		  			$start_time=$start_time[0];
		  		}
			}

			$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$addSma=explode(',',$_REQUEST['sma_id']);

			$service_typeObject=explode(',',$_REQUEST['service_typeObject']);
			$query ="insert into conditional_date(amount,start_time,end_time,user_id) value('".$_REQUEST['amount']."','".$start_time."','".$end_time."','".$userId."')";
            $cd_id= operations($query);

		 	for($j=0;$j<count($service_typeObject);$j++)
	  		{
				$Smaquery="insert into con_date_time_service(parent_id,service_type,user_id) value('".$cd_id."','".$service_typeObject[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}

			for($i=0;$i<count($VehicleCode);$i++)
	  		{
		  		$Vehquery="insert into cd_vehicle(cd_id,vehicle_code,user_id) value('".$cd_id."','".$VehicleCode[$i]."','".$userId."')";	
		  		$resource1 = operations($Vehquery);
	 	 	}
		  	for($j=0;$j<count($addSma);$j++)
	  		{
				$Smaquery="insert into cd_sma(cd_id,sma_id,user_id) value('".$cd_id."','".$addSma[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}
		   
		   	$result=global_message(200,1008,$cd_id);		   
	   	}
   		else
	   	{
	    	$result=global_message(201,1003);
   		}	
		return $result;	
	}

	/*****************************************************************
	Method:             getRateMatrixList()
	InputParameter:     user_id
	Return:             get Rate Matrix List
	*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  		{
			$query="Select * from conditional_date where user_id='".$_REQUEST['user_id']."' order by amount asc";
				$resource= operations($query);
				$contents = array();
			    if(count($resource)>0 && gettype($resource)!="boolean")
			 	{
					for($i=0; $i<count($resource); $i++)
					{
						$vehicle_code=''; 
						$sma_name='';
						$sma_id='';
						$value='';
						$Vehquery="Select vehicle_code from cd_vehicle where cd_id=".$resource[$i]['id'];
						$resource1= operations($Vehquery);
						for($j=0; $j<count($resource1); $j++)
							{
								$vehicle_code .=$resource1[$j]['vehicle_code'].',';
							}

							$driver_gratuity_query="Select service_type ,b.service_name from con_date_time_service a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";
							$driver_gratuity_query_result= operations($driver_gratuity_query);

							$service_typeArrayDublicate=[];
							$service_type='';
							$service_name='';

							for($j=0; $j<count($driver_gratuity_query_result); $j++)
							{
								if(!in_array($driver_gratuity_query_result[$j]['service_type'],$service_typeArrayDublicate))
								{
									array_push($service_typeArrayDublicate,$driver_gratuity_query_result[$j]['service_type']);
									$service_type .=$driver_gratuity_query_result[$j]['service_type'].',';
									$service_name .=$driver_gratuity_query_result[$j]['service_name'].',';
								}
							}

						$Smaquery="Select sma_id,sma_name from cd_sma,sma where sma.id=cd_sma.sma_id AND cd_sma.cd_id=".$resource[$i]['id'];
						$resource2= operations($Smaquery);
						for($k=0; $k<count($resource2); $k++)
						{
							$sma_name .=$resource2[$k]['sma_name'].',';
							$sma_id .=$resource2[$k]['sma_id'].',';
						}			
						$contents[$i]['id']=$resource[$i]['id'];
						$contents[$i]['amount']=$resource[$i]['amount'];
						$contents[$i]['sma_id'] = $sma_id;
						$contents[$i]['sma_name'] = $sma_name;
						$contents[$i]['vehicle_code']=$vehicle_code;
						$contents[$i]['service_type']=$service_type;
						$contents[$i]['service_name']=$service_name;
						$contents[$i]['start_time'] = $resource[$i]['start_time'];
						$contents[$i]['end_time'] = $resource[$i]['end_time'];
					}

				}
				if(count($contents)>0 && gettype($contents)!="boolean")
				   {
					   $result=global_message(200,1007,$contents);
					   
				   }
				   else
				   {
					   $result=global_message(200,1006);
				   }		  
		  
	  
		}
	 	else
	  	{
		  	$result=global_message(201,1003);
	  	}
	  	return  $result;
	}


	/*****************************************************************
	Method:             deleteCondDate()
	InputParameter:     cd_id
	Return:             deleteCondDate
	*****************************************************************/
	function deleteCondDate()
	{		
 		if((isset($_REQUEST['cd_id']) && !empty($_REQUEST['cd_id'])))
	   {
		  	$rowId=$_REQUEST['cd_id'];
 			$query="delete from conditional_date where id='".$rowId."'";
	    	$resource = operations($query);
			$queryDelete1="delete  from cd_sma where cd_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from cd_vehicle where cd_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$queryDelete2="delete  from con_date_time_service where parent_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$result=global_message(200,1010);   
		}
	  	else
	  	{
	   		$result=global_message(201,1003);
	  	}
		return $result;
	}

