<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
 * Method:             GetUsers()
 * InputParameter:        query
 * Return:             Get Users
 *****************************************************************/

function GetUsers()
{
    $query = "select ac_number,f_name,l_name from vrc_reg_cust_tbl";
    $resource = operations($query);
    if (count($resource) >= 1 && gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1003);
    }
    return $result;
}


/*****************************************************************
 * Method:             GetUsers()
 * InputParameter:        query
 * Return:             Get Users
 *****************************************************************/
function setDiscount()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $start_date = $_REQUEST['start_date'];
        $start_date = explode('/', $start_date);
        $start_date = $start_date[2] . "-" . $start_date[0] . "-" . $start_date[1];
        $end_date = $_REQUEST['endCalender'];
        $end_date = explode('/', $end_date);
        $end_date = $end_date[2] . "-" . $end_date[0] . "-" . $end_date[1];
        $start_time = $_REQUEST['start_time'];
        $start_time = explode('/', $start_time);
        $start_time = $start_time[2] . "-" . $start_time[0] . "-" . $start_time[1];
        $end_time = $_REQUEST['end_time'];
        $end_time = explode('/', $end_time);
        $end_time = $end_time[2] . "-" . $end_time[0] . "-" . $end_time[1];
        $VehicleCode = explode(',', $_REQUEST['apply_vehicle_type']);
        $addSma = explode(',', $_REQUEST['apply_sma']);
        $rateCode = explode(',', $_REQUEST['rate_type']);
        $serviceType = explode(',', $_REQUEST['service_type']);


        $query12 = "select count(*) totalnumber from vehicle_discount_table where (code='" . $_REQUEST['cupon_code'] . "' or name='" . $_REQUEST['cupon_name'] . "' ) and  user_id='" . $_REQUEST['user_id'] . "'";
        $query12Result = operations($query12);

        if ($query12Result[0]['totalnumber'] == 0) {
            $applyAutoPromo = isset($_REQUEST['applyAutoPromo']) ? $_REQUEST['applyAutoPromo'] : 0;
            $promo_code = isset($_REQUEST['promo_code']) ? $_REQUEST['promo_code'] : 0;
            $is_combine_discount = isset($_REQUEST['is_combine_discount']) ? $_REQUEST['is_combine_discount'] : 0;
            $query = "insert into vehicle_discount_table(name,code,discount_value,discount_type,start_date,end_date,start_time,end_time,promo_pref,is_combine_discount,user_id,is_all_user,apply_on,apply_service) value('" . $_REQUEST['cupon_name'] . "','" . $_REQUEST['cupon_code'] . "','" . $_REQUEST['cupon_value'] . "','" . $_REQUEST['cupon_discount_type'] . "','" . $start_date . "','" . $end_date . "','" . $start_time . "','" . $end_time . "','" . $promo_code . "','" . $is_combine_discount . "','" . $_REQUEST['user_id'] . "','" . $applyAutoPromo . "','','')";
            $discount_id = operations($query);
            $allUserArray = explode(',', $_REQUEST['allUser']);
            for ($i = 0; $i < count($allUserArray); $i++) {
                $allUserQuery = "insert into discount_user_associate(parent_id,passenger_id) value('" . (int)$discount_id . "','" . (int)$allUserArray[$i] . "')";
                $allUserQueryResult = operations($allUserQuery);
            }

            for ($i = 0; $i < count($VehicleCode); $i++) {
                $Vehquery = "insert into vehicle_dicount_extra_info(parent_id,vehicle_code, sma_id) value('" . (int)$discount_id . "','" . $VehicleCode[$i] . "', 0)";
                $resource1 = operations($Vehquery);
            }
            for ($j = 0; $j < count($addSma); $j++) {
                $Smaquery = "insert into discount_extra_service_table(parent_id,sma_id) value('" . (int)$discount_id . "','" . $addSma[$j] . "')";
                $resource2 = operations($Smaquery);
            }
            for ($k = 0; $k < count($rateCode); $k++) {
                $applyRatequery = "insert into vehicle_discount_apply_rate_table(parent_id,apply_rate) value('" . (int)$discount_id . "','" . $rateCode[$k] . "')";
                $resource3 = operations($applyRatequery);
            }

            for ($l = 0; $l < count($serviceType); $l++) {
                if($serviceType[$l] != ''){
                    $serviceTypequery = "insert into vehicle_discount_apply_vehicle_type_table(id,parent_id,apply_service_type) value(NULL, '" . (int)$discount_id . "','" . $serviceType[$l] . "')";
                    $resource4 = operations($serviceTypequery);
                }
            }

            $result = global_message(200, 1007, []);
        } else {
            $result = global_message(200, 1006);
        }

    } else {
        $result = global_message(200, 1005);
    }
    return $result;
}

/*****************************************************************
 * Method:             getDiscountCuponList()
 * InputParameter:        user_id
 * Return:             get Discount CuponList
 *****************************************************************/
function getDiscountCuponList()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "Select * from vehicle_discount_table where user_id='" . $_REQUEST['user_id'] . "' order by name asc";
        $resource = operations($query);
        $contents = array();
        for ($i = 0; $i < count($resource); $i++) {
            $vehicle_code = '';
            $sma_id = '';
            $value = '';
            $Vehquery = "Select vehicle_code from vehicle_dicount_extra_info where parent_id='" . $resource[$i]['id'] . "'";
            $resource1 = operations($Vehquery);
            for ($j = 0; $j < count($resource1); $j++) {
                $vehicle_code .= $resource1[$j]['vehicle_code'] . ',';
            }
            $Smaquery = "Select b.sma_name sma_id from discount_extra_service_table a inner join sma b on a.sma_id=b.id where parent_id='" . $resource[$i]['id'] . "'";
            $resource2 = operations($Smaquery);
            for ($k = 0; $k < count($resource2); $k++) {
                $sma_id .= $resource2[$k]['sma_id'] . ',';
            }

            $resource[$i]['start_date'] = explode("-", $resource[$i]['start_date']);
            $resource[$i]['start_date'] = $resource[$i]['start_date'][1] . "-" . $resource[$i]['start_date'][2] . "-" . $resource[$i]['start_date'][0];

            $resource[$i]['end_date'] = explode("-", $resource[$i]['end_date']);
            $resource[$i]['end_date'] = $resource[$i]['end_date'][1] . "-" . $resource[$i]['end_date'][2] . "-" . $resource[$i]['end_date'][0];

            $contents[$i]['id'] = $resource[$i]['id'];
            $contents[$i]['name'] = $resource[$i]['name'];
            $contents[$i]['code'] = $resource[$i]['code'];
            $contents[$i]['discount_value'] = $resource[$i]['discount_value'];
            $contents[$i]['discount_type'] = $resource[$i]['discount_type'];
            $contents[$i]['start_date'] = $resource[$i]['start_date'];
            $contents[$i]['end_date'] = $resource[$i]['end_date'];
            $contents[$i]['start_time'] = $resource[$i]['start_time'];
            $contents[$i]['end_time'] = $resource[$i]['end_time'];
            $contents[$i]['apply_on'] = $resource[$i]['apply_on'];
            $contents[$i]['apply_service'] = $resource[$i]['apply_service'];
            $contents[$i]['promo_pref'] = $resource[$i]['promo_pref'];
            $contents[$i]['is_all_user'] = $resource[$i]['is_all_user'];
            $contents[$i]['is_combine_discount'] = $resource[$i]['is_combine_discount'];
            $contents[$i]['sma_id'] = $sma_id;
            $contents[$i]['vehicle_code'] = $vehicle_code;
        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             viewDiscountCuppon()
 * InputParameter:        view_Id
 * Return:             view Discount Cuppon
 *****************************************************************/
function viewDiscountCuppon()
{
    if ((isset($_REQUEST['view_Id']) && !empty($_REQUEST['view_Id']))) {
        $query = "Select * from vehicle_discount_table where id='" . $_REQUEST['view_Id'] . "'";
        $resource = operations($query);
        $contents = array();

        for ($i = 0; $i < count($resource); $i++) {
            $vehicle_code = '';
            $sma_id = '';
            $value = '';
            $apply_rate = '';
            $service_type = '';
            $Vehquery = "Select vehicle_code from vehicle_dicount_extra_info where parent_id='" . $_REQUEST['view_Id'] . "'";
            $resource1 = operations($Vehquery);
            for ($j = 0; $j < count($resource1); $j++) {
                $vehicle_code .= $resource1[$j]['vehicle_code'] . ',';
            }
            $Smaquery = "Select sma_id from discount_extra_service_table where parent_id='" . $_REQUEST['view_Id'] . "'";
            $resource2 = operations($Smaquery);
            for ($k = 0; $k < count($resource2); $k++) {
                $sma_id .= $resource2[$k]['sma_id'] . ',';
            }

            $Applyrate = "Select apply_rate from vehicle_discount_apply_rate_table where parent_id='" . $_REQUEST['view_Id'] . "'";
            $resource3 = operations($Applyrate);
            for ($m = 0; $m < count($resource3); $m++) {
                $apply_rate .= $resource3[$m]['apply_rate'] . ',';
            }

            $Servicetype = "Select apply_service_type from vehicle_discount_apply_vehicle_type_table where parent_id='" . $_REQUEST['view_Id'] . "'";
            $resource4 = operations($Servicetype);
            for ($m = 0; $m < count($resource4); $m++) {
                $service_type .= $resource4[$m]['apply_service_type'] . ',';
            }

            $allUserQuery = "Select passenger_id from discount_user_associate  where parent_id='" . $_REQUEST['view_Id'] . "'";

            $allUserQueryResult = operations($allUserQuery);
            $allUserQueryResultArray = [];
            for ($ik = 0; $ik < count($allUserQueryResult); $ik++) {
                $querySelceUser = "select f_name,l_name,ac_number from vrc_reg_cust_tbl where ac_number= '" . $allUserQueryResult[$ik]['passenger_id'] . "'";
                $querySelceUserResult = operations($querySelceUser);
                array_push($allUserQueryResultArray, $querySelceUserResult);
            }

            for ($m = 0; $m < count($resource4); $m++) {
                $service_type .= $resource4[$m]['apply_service_type'] . ',';
            }

            $contents[$i]['allUserSelectUser'] = $allUserQueryResultArray;
            $contents[$i]['id'] = $resource[$i]['id'];
            $contents[$i]['name'] = $resource[$i]['name'];
            $contents[$i]['code'] = $resource[$i]['code'];
            $contents[$i]['discount_value'] = $resource[$i]['discount_value'];
            $contents[$i]['discount_type'] = $resource[$i]['discount_type'];
            $contents[$i]['start_date'] = $resource[$i]['start_date'];
            $contents[$i]['end_date'] = $resource[$i]['end_date'];
            $contents[$i]['start_time'] = $resource[$i]['start_time'];
            $contents[$i]['end_time'] = $resource[$i]['end_time'];
            $contents[$i]['apply_on'] = $apply_rate;
            $contents[$i]['apply_service'] = $service_type;
            $contents[$i]['promo_pref'] = $resource[$i]['promo_pref'];
            $contents[$i]['is_all_user'] = $resource[$i]['is_all_user'];
            $contents[$i]['is_combine_discount'] = $resource[$i]['is_combine_discount'];
            $contents[$i]['sma_id'] = $sma_id;
            $contents[$i]['vehicle_code'] = $vehicle_code;
        }

        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             deleteDiscountCuponList()
 * InputParameter:        row_Id
 * Return:             delete Discount CuponList
 *****************************************************************/
function deleteDiscountCuponList()
{
    if ((isset($_REQUEST['row_Id']) && !empty($_REQUEST['row_Id']))) {
        $rowId = $_REQUEST['row_Id'];

        $query = "delete from vehicle_discount_table where id='" . $rowId . "'";
        $resource = operations($query);
        $queryDelete1 = "delete  from discount_extra_service_table where parent_id='" . $rowId . "'";
        $resource2 = operations($queryDelete1);
        $queryDelete2 = "delete  from vehicle_discount_apply_rate_table where parent_id='" . $rowId . "'";
        $resource3 = operations($queryDelete2);
        $queryDelete3 = "delete  from vehicle_dicount_extra_info where parent_id='" . $rowId . "'";
        $resource4 = operations($queryDelete3);
        $queryDelete4 = "delete  from vehicle_discount_apply_vehicle_type_table where parent_id='" . $rowId . "'";
        $resource5 = operations($queryDelete4);

        $queryDelete4 = "delete  from discount_user_associate where parent_id='" . $rowId . "'";
        $resource5 = operations($queryDelete4);
        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}


/*****************************************************************
 * Method:             deleteDiscountCuponList()
 * InputParameter:        row_Id
 * Return:             delete Discount CuponList
 *****************************************************************/
function updateCupponData()
{
    $start_date = $_REQUEST['start_date'];
    $start_date = explode('/', $start_date);
    $start_date = $start_date[2] . "-" . $start_date[0] . "-" . $start_date[1];
    $end_date = $_REQUEST['endCalender'];
    $end_date = explode('/', $end_date);
    $end_date = $end_date[2] . "-" . $end_date[0] . "-" . $end_date[1];
    $start_time = $_REQUEST['start_time'];
    $start_time = explode('/', $start_time);
    $start_time = $start_time[2] . "-" . $start_time[0] . "-" . $start_time[1];
    $end_time = $_REQUEST['end_time'];
    $end_time = explode('/', $end_time);
    $end_time = $end_time[2] . "-" . $end_time[0] . "-" . $end_time[1];
    $getRowId = $_REQUEST['edit_row_id'];
    $query12 = "select count(*) totalnumber from vehicle_discount_table where (code='" . $_REQUEST['cupon_code'] . "' or name='" . $_REQUEST['cupon_name'] . "' )  and id<>'" . $getRowId . "'";
    $query12Result = operations($query12);
    if ($query12Result[0]['totalnumber'] == 0) {
        $queryforDelete = "delete from discount_extra_service_table where parent_id=" . $getRowId;
        operations($queryforDelete);
        $queryforDelete1 = "delete from vehicle_dicount_extra_info where parent_id=" . $getRowId;
        operations($queryforDelete1);
        $queryforDelete1 = "delete from discount_user_associate  where parent_id=" . $getRowId;
        operations($queryforDelete1);
        $queryforDelete2 = "delete from vehicle_discount_apply_vehicle_type_table where parent_id=" . $getRowId;
        operations($queryforDelete2);
        $queryforDelete3 = "delete from vehicle_discount_apply_rate_table where parent_id=" . $getRowId;
        operations($queryforDelete3);

        $applyAutoPromo = isset($_REQUEST['applyAutoPromo']) ? $_REQUEST['applyAutoPromo'] : 0;
        $promo_code = isset($_REQUEST['promo_code']) ? $_REQUEST['promo_code'] : 0;
        $is_combine_discount = isset($_REQUEST['is_combine_discount']) ? $_REQUEST['is_combine_discount'] : 0;
        $query = "update vehicle_discount_table set `name`='" . $_REQUEST['cupon_name'] . "',`is_all_user`='" . $applyAutoPromo . "', `code`='" . $_REQUEST['cupon_code'] . "', `discount_value`='" . $_REQUEST['cupon_value'] . "', `discount_type`='" . $_REQUEST['cupon_discount_type'] . "', `start_date`='" . $start_date . "', `end_date`='" . $end_date . "', `start_time`='" . $start_time . "', `end_time`='" . $end_time . "', `promo_pref`='" . $promo_code . "', `is_combine_discount`='" . $is_combine_discount . "' where id=" . $getRowId;
        operations($query);
        $allUserArray = explode(',', $_REQUEST['allUser']);
        for ($i = 0; $i < count($allUserArray); $i++) {
            $allUserQuery = "insert into discount_user_associate(parent_id,passenger_id) value('" . (int)$getRowId . "','" . (int)$allUserArray[$i] . "')";
            $allUserQueryResult = operations($allUserQuery);
        }
        $VehicleCode = explode(',', $_REQUEST['apply_vehicle_type']);
        $addSma = explode(',', $_REQUEST['apply_sma']);
        $rateCode = explode(',', $_REQUEST['rate_type']);
        $serviceType = explode(',', $_REQUEST['service_type']);
        for ($i = 0; $i < count($VehicleCode); $i++) {
            $Vehquery = "insert into vehicle_dicount_extra_info(parent_id,vehicle_code,sma_id) value('" . (int)$getRowId . "','" . $VehicleCode[$i] . "',0)";
            $resource1 = operations($Vehquery);
        }
        for ($j = 0; $j < count($addSma); $j++) {
            $Smaquery = "insert into discount_extra_service_table(parent_id,sma_id) value('" . (int)$getRowId . "','" . $addSma[$j] . "')";
            $resource2 = operations($Smaquery);
        }
        for ($k = 0; $k < count($rateCode); $k++) {
            $applyRatequery = "insert into vehicle_discount_apply_rate_table(parent_id,apply_rate) value('" . (int)$getRowId . "','" . $rateCode[$k] . "')";
            $resource3 = operations($applyRatequery);
        }
        for ($l = 0; $l < count($serviceType); $l++) {
            $serviceTypequery = "insert into vehicle_discount_apply_vehicle_type_table(parent_id,apply_service_type) value('" . (int)$getRowId . "','" . $serviceType[$l] . "')";
            $resource4 = operations($serviceTypequery);
        }
        $result = global_message(200, 1006, "successInserted");
    } else {
        $result = global_message(200, 1005);
    }
    return $result;
}

?>