<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
 * Method:             addAccount()
 * InputParameter:     user_id,web_access,account_status, prefix, email_address
 * Return:             add Account
 *****************************************************************/
function addAccount()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $createdAccount = createUserAccount($_REQUEST['prioriy'], $_REQUEST['prefix'], $_REQUEST['account_status'], $_REQUEST['web_access'], $_REQUEST['fax_number'], $_REQUEST['acount_type'], $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['email_address'], $_REQUEST['contact_email'], $_REQUEST['confirm_email'], $_REQUEST['cellular_phone'], $_REQUEST['office_phone'], $_REQUEST['home_phone'], $_REQUEST['user_password'], $_REQUEST['confirm_password'], $_REQUEST['company_name'], $_REQUEST['department_name'], $_REQUEST['job_title'], $_REQUEST['primery_Address'], $_REQUEST['country_name'], $_REQUEST['state_name'], $_REQUEST['city_name'], $_REQUEST['zip_code'], $_REQUEST['apt_state_name']);
        $ac_number = $createdAccount['number'];
        $ac_id = $createdAccount['id'];
        if ($createdAccount->ResponseCode == 0) {
            $query = "insert into vrc_reg_cust_tbl(ac_priority,ac_status,web_access,ofc_coun_code,home_count_code,cerl_count_code,f_name,l_name,c_name,department,job_title,ac_type,primry_address,city,state,zip_code,ac_number,ofc_number,hm_number,cerl_number,fax_number,eml_address,user_name,password,conf_password,prefix,country_name,user_id,account_id) value('" . $_REQUEST['prioriy'] . "','" . $_REQUEST['account_status'] . "','" . $_REQUEST['web_access'] . "','" . $_REQUEST['office_country_code'] . "','" . $_REQUEST['home_country_code'] . "','" . $_REQUEST['cellular_country_code'] . "','" . $_REQUEST['first_name'] . "','" . $_REQUEST['last_name'] . "','" . $_REQUEST['company_name'] . "','" . $_REQUEST['department_name'] . "','" . $_REQUEST['job_title'] . "','" . $_REQUEST['acount_type'] . "','" . $_REQUEST['primery_Address'] . "','" . $_REQUEST['city_name'] . "','" . $_REQUEST['state_name'] . "','" . $_REQUEST['zip_code'] . "','" . $ac_number . "','" . $_REQUEST['office_phone'] . "','" . $_REQUEST['home_phone'] . "','" . $_REQUEST['cellular_phone'] . "','" . $_REQUEST['fax_number'] . "','" . $_REQUEST['contact_email'] . "','" . $_REQUEST['email_address'] . "','" . $_REQUEST['user_password'] . "','" . $_REQUEST['confirm_password'] . "','" . $_REQUEST['prefix'] . "','" . $_REQUEST['country_name'] . "','" . $_REQUEST['user_id'] . "','" . $ac_id . "')";
            $resource = operations($query);
            $result = global_message(201, 1006, $resource);
        } else {
            $result = global_message(200, 1007, $createdAccount);
        }
    } else {
        $result = global_message(200, 1003, 'invalid credential');
    }
    return $result;
}

/*****************************************************************
 * Method:             createAccount()
 * InputParameter:     user_id,prefix,first_name, last_name,email_address
 * Return:             create Account
 *****************************************************************/
function createAccount()
{

    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $createdAccount = createCorporateAccount($_REQUEST['prefix'], $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['email_address'], $_REQUEST['confirm_mail'], $_REQUEST['user_name'], $_REQUEST['cellular_phone'], $REQUEST['office_phone'], $REQUEST['office_extension'], $_REQUEST['user_password'], $_REQUEST['confirm_password'], $_REQUEST['company_name'], $_REQUEST['department_name'], $_REQUEST['job_title'], $_REQUEST['primery_Address'], $_REQUEST['country_name'], $_REQUEST['state_name'], $_REQUEST['city_name'], $_REQUEST['zip_code']);
        $ac_number = $createdAccount->AcctNumber;
        $ac_id = $createdAccount->AcctID;

        if ($createdAccount->ResponseCode == 0) {
            $query = "insert into vrc_reg_cust_tbl(ofc_coun_code,f_name,l_name,c_name,department,job_title,primry_address,city,state,zip_code,ac_number,ofc_number,cerl_number,eml_address,user_name,password,conf_password,prefix,country_name,user_id,account_id) value('" . $_REQUEST['office_extension'] . "','" . $_REQUEST['first_name'] . "','" . $_REQUEST['last_name'] . "','" . $_REQUEST['company_name'] . "','" . $_REQUEST['department_name'] . "','" . $_REQUEST['job_title'] . "','" . $_REQUEST['primery_Address'] . "','" . $_REQUEST['city_name'] . "','" . $_REQUEST['state_name'] . "','" . $_REQUEST['zip_code'] . "','" . $ac_number . "','" . $_REQUEST['office_phone'] . "','" . $_REQUEST['cellular_phone'] . "','" . $_REQUEST['email_address'] . "','" . $_REQUEST['user_name'] . "','" . $_REQUEST['user_password'] . "','" . $_REQUEST['confirm_password'] . "','" . $_REQUEST['prefix'] . "','" . $_REQUEST['country_name'] . "','" . $_REQUEST['user_id'] . "','" . $ac_id . "')";
            $resource = operations($query);
            $result = global_message(201, 1006, $resource);
        } else {
            $result = global_message(200, 1007, $createdAccount);
        }
    } else {
        $result = global_message(200, 1003, 'invalid credential');
    }
    return $result;
}

/*****************************************************************
 * Method:             getuserAccounList()
 * InputParameter:     user_id,
 * Return:            get user Account List
 *****************************************************************/
function getuserAccounList()
{
    if ((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))) {
        $query = "Select * from vrc_reg_cust_tbl where user_id='" . $_REQUEST['user_id'] . "' order by f_name asc ";
        $resource = operations($query);
        $contents = array();
        for ($i = 0; $i < count($resource); $i++) {
            $account_type = '';
            $account_type = $resource[$i]['account_type'];
            $contents[$i]['id'] = $resource[$i]['id'];
            $contents[$i]['f_name'] = $resource[$i]['f_name'];
            $contents[$i]['l_name'] = $resource[$i]['l_name'];
            $contents[$i]['c_name'] = $resource[$i]['c_name'];
            $contents[$i]['department'] = $resource[$i]['department'];
            $contents[$i]['job_title'] = $resource[$i]['job_title'];
            $contents[$i]['ac_type'] = $resource[$i]['ac_type'];
            $contents[$i]['primry_address'] = $resource[$i]['primry_address'];
            $contents[$i]['city'] = $resource[$i]['city'];
            $contents[$i]['state'] = $resource[$i]['state'];
            $contents[$i]['zip_code'] = $resource[$i]['zip_code'];
            $contents[$i]['ac_number'] = $resource[$i]['ac_number'];
            $contents[$i]['vip_number'] = $resource[$i]['vip_number'];
            $contents[$i]['ofc_coun_code'] = $resource[$i]['ofc_coun_code'];
            $contents[$i]['ofc_number'] = $resource[$i]['ofc_number'];
            $contents[$i]['home_count_code'] = $resource[$i]['home_count_code'];
            $contents[$i]['hm_number'] = $resource[$i]['hm_number'];
            $contents[$i]['cerl_count_code'] = $resource[$i]['cerl_count_code'];
            $contents[$i]['cerl_number'] = $resource[$i]['cerl_number'];
            $contents[$i]['fax_number'] = $resource[$i]['fax_number'];
            $contents[$i]['eml_address'] = $resource[$i]['eml_address'];
            $contents[$i]['referal'] = $resource[$i]['referal'];
            $contents[$i]['ac_priority'] = $resource[$i]['ac_priority'];
            $contents[$i]['ac_status'] = $resource[$i]['ac_status'];
            $contents[$i]['web_access'] = $resource[$i]['web_access'];
            $contents[$i]['ac_term'] = $resource[$i]['ac_term'];
            $contents[$i]['ac_term'] = $resource[$i]['ac_term'];
            $contents[$i]['user_name'] = $resource[$i]['user_name'];
            $contents[$i]['prefix'] = $resource[$i]['prefix'];
            $contents[$i]['user_name'] = $resource[$i]['user_name'];
            $contents[$i]['country_name'] = $resource[$i]['country_name'];
            $contents[$i]['account_type'] = $account_type;
            $contents[$i]['account_id'] = $resource[$i]['account_id'];
        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             viewuserInfo()
 * InputParameter:     user_id,
 * Return:            view user Info
 *****************************************************************/
function viewuserInfo()
{
    if ((isset($_REQUEST['view_Id']) && !empty($_REQUEST['view_Id']))) {
        $query = "Select * from vrc_reg_cust_tbl where id='" . $_REQUEST['view_Id'] . "'";
        $resource = operations($query);
        $contents = array();

        for ($i = 0; $i < count($resource); $i++) {
            $account_type = '';
            $account_type = $resource[$i]['account_type'];
            $contents[$i]['id'] = $resource[$i]['id'];
            $contents[$i]['f_name'] = $resource[$i]['f_name'];
            $contents[$i]['l_name'] = $resource[$i]['l_name'];
            $contents[$i]['c_name'] = $resource[$i]['c_name'];
            $contents[$i]['department'] = $resource[$i]['department'];
            $contents[$i]['job_title'] = $resource[$i]['job_title'];
            $contents[$i]['ac_type'] = $resource[$i]['ac_type'];
            $contents[$i]['primry_address'] = $resource[$i]['primry_address'];
            $contents[$i]['city'] = $resource[$i]['city'];
            $contents[$i]['state'] = $resource[$i]['state'];
            $contents[$i]['zip_code'] = $resource[$i]['zip_code'];
            $contents[$i]['ac_number'] = $resource[$i]['ac_number'];
            $contents[$i]['vip_number'] = $resource[$i]['vip_number'];
            $contents[$i]['ofc_coun_code'] = $resource[$i]['ofc_coun_code'];
            $contents[$i]['ofc_number'] = $resource[$i]['ofc_number'];
            $contents[$i]['home_count_code'] = $resource[$i]['home_count_code'];
            $contents[$i]['hm_number'] = $resource[$i]['hm_number'];
            $contents[$i]['cerl_count_code'] = $resource[$i]['cerl_count_code'];
            $contents[$i]['cerl_number'] = $resource[$i]['cerl_number'];
            $contents[$i]['fax_number'] = $resource[$i]['fax_number'];
            $contents[$i]['eml_address'] = $resource[$i]['eml_address'];
            $contents[$i]['user_password'] = $resource[$i]['password'];
            $contents[$i]['confirm_password'] = $resource[$i]['conf_password'];
            $contents[$i]['referal'] = $resource[$i]['referal'];
            $contents[$i]['ac_priority'] = $resource[$i]['ac_priority'];
            $contents[$i]['ac_status'] = $resource[$i]['ac_status'];
            $contents[$i]['web_access'] = $resource[$i]['web_access'];
            $contents[$i]['ac_term'] = $resource[$i]['ac_term'];
            $contents[$i]['ac_term'] = $resource[$i]['ac_term'];
            $contents[$i]['user_name'] = $resource[$i]['user_name'];
            $contents[$i]['prefix'] = $resource[$i]['prefix'];
            $contents[$i]['user_name'] = $resource[$i]['user_name'];
            $contents[$i]['country_name'] = $resource[$i]['country_name'];
            $contents[$i]['account_type'] = $account_type;
            $contents[$i]['account_id'] = $resource[$i]['account_id'];
        }
        if (count($contents) > 0 && gettype($contents) != "boolean") {
            $result = global_message(200, 1007, $contents);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             updateUserAccount()
 * InputParameter:     edit_row_id,prioriy,prefix,account_status
 * Return:             update User Account
 *****************************************************************/
function updateUserAccount()
{
    if ((isset($_REQUEST['edit_row_id']) && !empty($_REQUEST['edit_row_id']))) {
        $accountId = $_REQUEST['account_id'];
        $updateUserAccount = updateUserAccount1($_REQUEST['prioriy'], $_REQUEST['prefix'], $_REQUEST['account_status'], $_REQUEST['web_access'], $_REQUEST['fax_number'], $accountId, $_REQUEST['acount_type'], $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['email_address'], $_REQUEST['cellular_phone'], $_REQUEST['office_phone'], $_REQUEST['home_phone'], $_REQUEST['user_password'], $_REQUEST['confirm_password'], $_REQUEST['company_name'], $_REQUEST['department_name'], $_REQUEST['job_title'], $_REQUEST['primery_Address'], $_REQUEST['country_name'], $_REQUEST['state_name'], $_REQUEST['city_name'], $_REQUEST['zip_code']);
        if ($updateUserAccount->ResponseCode == 0) {
            $getRowId = $_REQUEST['edit_row_id'];
            $query = "update vrc_reg_cust_tbl set `ac_priority`='" . $_REQUEST['prioriy'] . "', `ac_status`='" . $_REQUEST['account_status'] . "', `web_access`='" . $_REQUEST['web_access'] . "', `f_name`='" . $_REQUEST['first_name'] . "', `l_name`='" . $_REQUEST['last_name'] . "', `c_name`='" . $_REQUEST['company_name'] . "', `department`='" . $_REQUEST['department_name'] . "', `job_title`='" . $_REQUEST['job_title'] . "', `ac_type`='" . $_REQUEST['acount_type'] . "', `primry_address`='" . $_REQUEST['primery_Address'] . "', `city`='" . $_REQUEST['city_name'] . "', `state`='" . $_REQUEST['state_name'] . "', `zip_code`='" . $_REQUEST['zip_code'] . "', `ofc_number`='" . $_REQUEST['office_phone'] . "', `hm_number`='" . $_REQUEST['home_phone'] . "', `cerl_number`='" . $_REQUEST['cellular_phone'] . "', `fax_number`='" . $_REQUEST['fax_number'] . "', `eml_address`='" . $_REQUEST['email_address'] . "', `password`='" . $_REQUEST['user_password'] . "', `conf_password`='" . $_REQUEST['confirm_password'] . "', `user_name`='" . $_REQUEST['email_address'] . "', `prefix`='" . $_REQUEST['prefix'] . "', `country_name`='" . $_REQUEST['country_name'] . "' where id=" . $getRowId;
            $resource = operations($query);
            $result = global_message(201, 1006, $resource);
        } else {
            $result = global_message(200, 1007, $updateUserAccount);
        }
        $result = global_message(201, 1006, $resource);
    } else {
        $result = global_message(200, 1003, 'invalid credential');
    }
    return $result;
}

/*****************************************************************
 * Method:             deleteUserInfo()
 * InputParameter:     delete_id
 * Return:             delete User Info
 *****************************************************************/
function deleteUserInfo()
{
    if ((isset($_REQUEST['delete_id']) && !empty($_REQUEST['delete_id']))) {
        $rowId = $_REQUEST['delete_id'];
        $queryDelete = "delete  from vrc_reg_cust_tbl where id='" . $rowId . "'";
        $resource = operations($queryDelete);
        $result = global_message(200, 1010);
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

/*****************************************************************
 * Method:             getuserAddress()
 * InputParameter:    AccountId,ApiId,ApiKey
 * Return:             getUserAddress
 *****************************************************************/

function GetAccountAddresses()
{

    if (isset($_REQUEST['action']) && !empty($_REQUEST['action']) && isset($_REQUEST['limoApiKey']) && !empty($_REQUEST['limoApiKey']) && isset($_REQUEST['limoApiID']) && !empty($_REQUEST['limoApiID']) && isset($_REQUEST['account_id']) && !empty($_REQUEST['account_id'])) {
        $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
        $sh_param = array(
            'apiId' => $_REQUEST['limoApiID'],
            'apiKey' => $_REQUEST['limoApiKey']);
        $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL', 'GetStates', $sh_param);
        $action = $_REQUEST['action'];
        $result = $action . 'Result';
        $trans_vehicle = $soapClient->$action(array('apiId' => $_REQUEST['limoApiID'], 'apiKey' => $_REQUEST['limoApiKey'], 'acctId' => $_REQUEST['account_id']))->$result;
        $final_user_array = [];
        $parrentArray = $trans_vehicle->Addresses->Address;
        $childArray = $trans_vehicle->Addresses->Address;
        /*for($i=0; $i<count($parrentArray); $i++)
          {
            for($j=$i+1; $j<count($parrentArray); $j++)
              {
                if($parrentArray[$i]->PassengerCapacity>$parrentArray[$j]->PassengerCapacity)
                {
                      $finalVehicleResult=$parrentArray[$i];
                      $parrentArray[$i]=$parrentArray[$j];
                      $parrentArray[$j]=$finalVehicleResult;
                }

              }
        }*/
        $trans_vehicle->Addresses->Address = $parrentArray;
        echo json_encode($trans_vehicle);
        exit;
    } else {
        echo 'unauthorized url!';
    }
}

/*****************************************************************
 * Method:             GetAirlines()
 * InputParameter:    AccountId,ApiId,ApiKey
 * Return:             getUserAddress
 *****************************************************************/

function GetAirlines()
{

    $query = "Select * from airlines where user_id='" . $_REQUEST['userId'] . "' order by name asc";
    $resource = operations($query);

    $airline = [];

    foreach ($resource as $item) {
        $airline[] = [
            "AirlineCode" => $item['airline_code'],
            "AirlineName" => $item['name'] . ' - ' . $item['airline_code'],
        ];
    }

    $airlines_list = [
        "ResponseCode" => 0,
        "ResponseText" => "OK",
        "Airlines" =>
            [
                "Airline" => $airline,
            ]
    ];

    return $airlines_list;
}

/*****************************************************************
 * Method:             GetAirlines()
 * InputParameter:    AccountId,ApiId,ApiKey
 * Return:             getUserAddress
 *****************************************************************/

function GetCruiseShipsWithLines()
{

//        return [
//            "ResponseCode" => 0,
//            "ResponseText" => "OK",
//            "CruiseShipsWithLines" => [
//                "CruiseShip" => [
//                    [
//                        "IdCruiseShip" => 59,
//                        "IdCruiseLine" => 39,
//                        "ShipName" => "Freedom",
//                        "LineName" => "Carnival"
//                    ],
//                    [
//                        "IdCruiseShip" => 8129,
//                        "IdCruiseLine" => 39,
//                        "ShipName" => "Fascination",
//                        "LineName" => "Royal Carnival",
//                    ],
//                    [
//                        "IdCruiseShip" => 8161,
//                        "IdCruiseLine" => 38,
//                        "ShipName" => "Symphony of the Seas",
//                        "LineName" => "Aa Caribbean",
//                    ],
//                ],
//            ],
//        ];

    $query = "Select * from cruise where user_id='" . $_REQUEST['userId'] . "' order by name asc";
    $resource = operations($query);

    $cruiseShip = [];

    foreach ($resource as $item) {
        $cruiseShip[] = [
            'IdCruiseShip' => $item['id'],
            'IdCruiseLine' => preg_replace('/[^a-zA-Z0-9_ -]/s', '', $item['name']),
            'ShipName' => preg_replace('/[^a-zA-Z0-9_ -]/s', '', $item['cruise_code']),
            'LineName' => preg_replace('/[^a-zA-Z0-9_ -]/s', '', $item['name']),
        ];
    }
    $cruise_list = [
        "ResponseCode" => 0,
        "ResponseText" => "OK",
        "CruiseShipsWithLines" => [
            "CruiseShip" => $cruiseShip,
        ],
    ];

    return $cruise_list;
}


/*****************************************************************
 * Method:             GetAccountCreditCards()
 * InputParameter:    AccountId,ApiId,ApiKey
 * Return:             getUserAddress
 *****************************************************************/

function GetAccountCreditCards()
{

    if (isset($_REQUEST['action']) && !empty($_REQUEST['action']) && isset($_REQUEST['limoApiKey']) && !empty($_REQUEST['limoApiKey']) && isset($_REQUEST['limoApiID']) && !empty($_REQUEST['limoApiID']) && isset($_REQUEST['account_id']) && !empty($_REQUEST['account_id'])) {
        $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
        $sh_param = array(
            'apiId' => $_REQUEST['limoApiID'],
            'apiKey' => $_REQUEST['limoApiKey']);
        $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL', 'GetAccountCreditCards', $sh_param);
        $action = $_REQUEST['action'];
        $result = $action . 'Result';
        $trans_vehicle = $soapClient->$action(array('apiId' => $_REQUEST['limoApiID'], 'apiKey' => $_REQUEST['limoApiKey'], 'acctId' => $_REQUEST['account_id']))->$result;
        $final_user_array = [];
        $parrentArray = $trans_vehicle->CreditCards->CreditCard;
        $childArray = $trans_vehicle->CreditCards->CreditCard;
        /*for($i=0; $i<count($parrentArray); $i++)
          {
            for($j=$i+1; $j<count($parrentArray); $j++)
              {
                if($parrentArray[$i]->PassengerCapacity>$parrentArray[$j]->PassengerCapacity)
                {
                      $finalVehicleResult=$parrentArray[$i];
                      $parrentArray[$i]=$parrentArray[$j];
                      $parrentArray[$j]=$finalVehicleResult;
                }

              }
        }*/
        $trans_vehicle->CreditCards->CreditCard = $parrentArray;
        echo json_encode($trans_vehicle);
        exit;
    } else {
        echo 'unauthorized url!';
    }
}

/*****************************************************************
 * Method:             createUserAccount()
 * InputParameter:    ContPrLevel,$prefix,ContStatus,ContWebAccess,ContFax,accountType,first_name,last_name,email,contact_email,confirm_email,cellular_phone,office_phone,home_phone,password,confirm_password,company_name,user_department,user_job,prim_address,country,state,city,zip_code,app_state
 * Return:             create User Account
 *****************************************************************/

function createUserAccount($ContPrLevel, $prefix, $ContStatus, $ContWebAccess, $ContFax, $accountType, $first_name, $last_name, $email, $contact_email, $confirm_email, $cellular_phone, $office_phone, $home_phone, $password, $confirm_password, $company_name, $user_department, $user_job, $prim_address, $country, $state, $city, $zip_code, $app_state)
{

    $limoAPi = operations('select * from limoanywhereapikey where user_id=' . $_REQUEST['user_id']);
    $resource = $limoAPi[0];
    $getTockent = array(
        "grant_type" => "client_credentials",
        "client_id" => $resource['client_id'],//"ca_customer_123corp",
        "client_secret" => $resource['client_secret'] //"K2UnJNviQWhq5Wf7yLtIjD6Yyo0Bo7sC3OgfPFKX33PdRoAIQV"
    );
    $getTockent = json_encode($getTockent);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.mylimobiz.com/oauth2/token");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $getTockent);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json"
    ));
    $getTockenResponse = curl_exec($ch);
    curl_close($ch);
    $getTockenResponse = json_decode($getTockenResponse);
//    $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
//    $sh_param = array(
//        'apiId' => $limoAPi[0]['limo_any_where_api_key'],
//        'apiKey' => $limoAPi[0]['limo_any_where_api_id']
//    );
//    $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL', 'GetStates', $sh_param);


    $accountType1 = explode(',', $accountType);
    for ($j = 0; $j < count($accountType1); $j++) {
        if ($accountType1[$j] == '(B)') {
            $bill = $accountType[$j];
            $contTerm = 'BILL';
        } else if ($accountType1[$j] == '(P)') {
            $passenger = $accountType[$j];
            $contTerm = 'BILL';
        } else {
            $ContType3 = $accountType[$j];
        }
    }

    $action = 'CreateAccount';
    $fname = $first_name;
    $lname = $last_name;
    $email = $email;
    $contact_email = $contact_email;
    $confirm_email = $confirm_email;
    $pwd = $password;
    $verify_pwd = $confirm_password;
    $ContPhone1 = $office_phone;
    $ContPhone2 = $home_phone;
    $phone = $cellular_phone;
    $address = $prim_address;
    $city = $city;
    $state = $state;
    $country = $country;
    $zip = $zip_code;
    $prefix = $prefix;
    $company_name = $company_name;
    $idCont = '4';
    $idComp = '24';
    $idParent = '23';
    $contGrat = '24.5';
    $ContDiscount = '12';
    $ContVoucherFee = '34';
    $ContOtherSurchPerc = '25';
    $JobTitle = $user_job;
    $ContPrLevel = $ContPrLevel;
    $Department = $user_department;
    $AptSte = '';
    $Passenger = $passenger;
    $billing_Contact = $bill;
    $DateCreated = date("Y-m-d");
    $DateUpdated = date("Y-m-d");
    $ContExtraFldFL1 = '24';
    $ContExtraFldFL2 = '23';
    $IdAgent = '45';
    $ContAgentRate = 0;
    $IdAgrTmpl = 0;
    $IdWebLogin = 0;
    $IsCustomContAcctNumber = 0;
    $faxnumber = $ContFax;
    $ContStatus = $ContStatus;
    $ContWebAccess = $ContWebAccess;
    $ContFax = $ContFax;
    $ContTerms = $contTerm;
    $AccountEmail = array('IdAccountEmail' => '', 'IdCont' => '', 'Email' => $contact_email, 'Type' => 'Confirmation',
        'Notification' => 1, 'EmailProperties' => 'yes');
    $email_list = array('AccountEmail' => $AccountEmail);
    $account_param = array('ContPrLevel' => $ContPrLevel, 'ContStatus' => $ContStatus, 'ContWebAccess' => $ContWebAccess,
        'ContPrefix' => $prefix, 'Prefix' => $prefix, 'IdCont' => $idCont, 'IdComp' => $idComp, 'ContGrat' => $contGrat,
        'ContDiscount' => $ContDiscount, 'ContOtherSurchPerc' => $ContOtherSurchPerc, 'ContVoucherFee' => $ContVoucherFee,
        'IdParent' => $idParent, 'CompanyName' => $company_name, 'PrimaryAddress' => $address, 'StateProv' => $state,
        'FirstName' => $fname, 'LastName' => $lname, 'JobTitle' => $JobTitle, 'CityTown' => $city,
        'Country' => $country, 'Department' => $Department, 'AptSte' => $app_state, 'ZipPost' => $zip,
        'BillingContact' => $billing_Contact, 'DateCreated' => $DateCreated, 'DateUpdated' => $DateUpdated,
        'ContExtraFldFL1' => $ContExtraFldFL1, 'ContExtraFldFL2' => $ContExtraFldFL2,
        'ContExtraFldFL3' => $ContExtraFldFL2, 'IdAgent' => $IdAgent, 'ContAgentRate' => $ContAgentRate,
        'IdAgent2' => $IdAgent, 'ContAgent2Rate' => $ContAgentRate, 'IdAgrTmpl' => $IdAgrTmpl,
        'IdWebLogin' => $IdWebLogin, 'IsCustomContAcctNumber' => $IsCustomContAcctNumber,
        'Passenger' => $Passenger, 'EmailList' => $email_list, 'ContType3' => $ContType3, 'MobilePhone' => $phone,
        'ContPhone1' => $office_phone, 'ContPhone2' => $home_phone, 'ContEmail' => $contact_email,
        'ContEmail1' => $contact_email, 'ContConfirm1' => $confirm_email, 'ContConfirm2' => $confirm_email,
        'UserName' => $email, 'Fax' => $faxnumber, 'Password' => $pwd, 'VerifyPassword' => $verify_pwd);

    //print_r($account_param);
    $result = $action . 'Result';
//    $mainarray = array('apiId' => $limoAPi[0]['limo_any_where_api_id'], 'apiKey' => $limoAPi[0]['limo_any_where_api_key'], 'accountInfo' => $account_param);
//    $trans = $soapClient->$action($mainarray)->$result;

    $dataInfo = array(
        "email" => $email,
        "password" => $pwd,
        "prefix" => $prefix,
        "first_name" => $fname,
        "last_name" => $lname,
        "position" => $JobTitle,
        "department" => "",
        "company" => $company_name,
        "address" => array(
            "name" => $address,
            "phone" => $phone,
            "country_code" => "",
            "state_code" => "",
            "postal_code" => "",
            "county" => $country,
            "city" => $city,
            "address_line1" => "",
            "address_line2" => "",
            "latitude" => 0,
            "longitude" => 0
        ),
        "home_phone" => $home_phone,
        "office_phone" => $office_phone,
        "cellular_phone1" => $phone,
        "cellular_phone2" => "",
        "cellular_phone3" => "",
        "fax1" => $faxnumber,
        "fax2" => "",
        "emails" => [['email' => $email]],
//        "default_payment_type_id" => 1,
//        "default_credit_card_id" => 1
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.mylimobiz.com/companies/123corp/customers/sign_up");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dataInfo, true));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: bearer " . $getTockenResponse->access_token
    ));
    $response = curl_exec($ch);
    curl_close($ch);

    return json_decode($response, true);
}


/*****************************************************************
 * Method:             createCorporateAccount()
 * InputParameter:     prefix,first_name,last_name,email,confirm_email,user_name,cellular_phone,office_phone,office_extenstion,password,confirm_password,company_name,user_department,user_job,prim_address,country,state,city,zip_code
 * Return:             create Corporate Accountt
 *****************************************************************/
function createCorporateAccount($prefix, $first_name, $last_name, $email, $confirm_email, $user_name, $cellular_phone, $office_phone, $office_extenstion, $password, $confirm_password, $company_name, $user_department, $user_job, $prim_address, $country, $state, $city, $zip_code)
{
    $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
    $sh_param = array(
        'apiId' => 'UySGaySkGxFjA28',
        'apiKey' => 'Ypk882t5QfWEgqrorQRJ');
    $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL', 'GetStates', $sh_param);
    $action = 'CreateAccount';
    $fname = $first_name;
    $lname = $last_name;
    $email = $email;
    $confirm_mail = $confirm_email;
    $user_name = $user_name;
    $pwd = $password;
    $verify_pwd = $confirm_password;
    $ContPhone1 = $office_phone;
    $ContPhone2 = '';
    $phone = $cellular_phone;
    $address = $prim_address;
    $city = $city;
    $state = $state;
    $country = $country;
    $zip = $zip_code;
    $prefix = $prefix;
    $company_name = $company_name;
    $idCont = '4';
    $idComp = '24';
    $idParent = '23';
    $contGrat = '24.5';
    $ContDiscount = '12';
    $ContVoucherFee = '34';
    $ContOtherSurchPerc = '25';
    $JobTitle = $user_job;
    $ContPrLevel = '';
    $Department = $user_department;
    $AptSte = '';
    $Passenger = '';
    $billing_Contact = '';
    $DateCreated = date("Y-m-d");
    $DateUpdated = date("Y-m-d");
    $ContExtraFldFL1 = '24';
    $ContExtraFldFL2 = '23';
    $IdAgent = '45';
    $ContAgentRate = 0;
    $IdAgrTmpl = 0;
    $IdWebLogin = 0;
    $IsCustomContAcctNumber = 0;
    $faxnumber = 0;
    $ContStatus = 0;
    $ContWebAccess = 0;
    $ContFax = 0;
    $ContTerms = '';

    $account_param = array('ContPrefix' => $prefix, 'Prefix' => $prefix, 'IdCont' => $idCont, 'IdComp' => $idComp, 'ContGrat' => $contGrat, 'ContDiscount' => $ContDiscount, 'ContOtherSurchPerc' => $ContOtherSurchPerc, 'ContVoucherFee' => $ContVoucherFee, 'IdParent' => $idParent, 'CompanyName' => $company_name, 'PrimaryAddress' => $address, 'StateProv' => $state, 'FirstName' => $fname, 'LastName' => $lname, 'JobTitle' => $JobTitle, 'CityTown' => $city, 'Country' => $country, 'Department' => $Department, 'AptSte' => $email, 'ZipPost' => $zip, 'BillingContact' => $billing_Contact, 'DateCreated' => $DateCreated, 'DateUpdated' => $DateUpdated, 'ContExtraFldFL1' => $ContExtraFldFL1, 'ContExtraFldFL2' => $ContExtraFldFL2, 'ContExtraFldFL3' => $ContExtraFldFL2, 'IdAgent' => $IdAgent, 'ContAgentRate' => $ContAgentRate, 'IdAgent2' => $IdAgent, 'ContAgent2Rate' => $ContAgentRate, 'IdAgrTmpl' => $IdAgrTmpl, 'IdWebLogin' => $IdWebLogin, 'IsCustomContAcctNumber' => $IsCustomContAcctNumber,
        'Passenger' => $Passenger, 'EmailList' => $email_list, 'ContType3' => $ContType3, 'MobilePhone' => $phone, 'ContPhone1' => $office_phone, 'ContPhone2' => $home_phone, 'ContEmail' => $email, 'ContConfirm1' => $confirm_mail, 'UserName' => $user_name, 'Fax' => $faxnumber, 'Password' => $pwd, 'VerifyPassword' => $verify_pwd);
    $AccountEmail = array('IdAccountEmail' => $idCont, 'IdCont' => $idCont, 'Email' => $email, 'Type' => 'Confirmation', 'Notification' => 1, 'EmailProperties' => 'yes');
    $email_list = array('AccountEmail' => $AccountEmail);
    //print_r($account_param);
    $result = $action . 'Result';
    $mainarray = array('apiId' => 'UySGaySkGxFjA28', 'apiKey' => 'Ypk882t5QfWEgqrorQRJ', 'accountInfo' => $account_param);
    $trans = $soapClient->$action($mainarray)->$result;
    return $trans;
}

/*****************************************************************
 * Method:             updateUserAccount1()
 * InputParameter:     ContPrLevel,prefix,ContStatus,ContWebAccess,ContFax,accountId,accountType,first_name,last_name,email,cellular_phone,office_phone,home_phone,password,confirm_password,company_name,user_department,user_job,prim_address,country,state,city,zip_code
 * Return:             update User Account1
 *****************************************************************/
function updateUserAccount1($ContPrLevel, $prefix, $ContStatus, $ContWebAccess, $ContFax, $accountId, $accountType, $first_name, $last_name, $email, $cellular_phone, $office_phone, $home_phone, $password, $confirm_password, $company_name, $user_department, $user_job, $prim_address, $country, $state, $city, $zip_code)
{
    $soapClient = new SoapClient("https://book.mylimobiz.com/api/apiservice.asmx?WSDL");
    $sh_param = array(
        'apiId' => 'UySGaySkGxFjA28',
        'apiKey' => 'Ypk882t5QfWEgqrorQRJ',
        'idCont' => $accountId);
    $headers = new SoapHeader('https://book.mylimobiz.com/api/apiservice.asmx?WSDL', 'GetStates', $sh_param);

    $accountType1 = explode(',', $accountType);

    for ($j = 0; $j < count($accountType1); $j++) {
        if ($accountType1[$j] == '(B)') {
            $bill = $accountType[$j];
        } else if ($accountType1[$j] == '(P)') {
            $passenger = $accountType[$j];
        }
    }

    $action = 'UpdateAccount';
    $fname = $first_name;
    $lname = $last_name;
    $accountId = $accountId;
    $email = $email;
    $pwd = $password;
    $verify_pwd = $confirm_password;
    $ContPhone1 = $office_phone;
    $ContPhone2 = $home_phone;
    $phone = $cellular_phone;
    $address = $prim_address;
    $city = $city;
    $state = $state;
    $country = $country;
    $zip = $zip_code;
    $prefix = $prefix;
    $company_name = $company_name;
    $idCont = '4';
    $idComp = '24';
    $idParent = '23';
    $contGrat = '24.5';
    $ContDiscount = '12';
    $ContVoucherFee = '34';
    $ContOtherSurchPerc = '25';
    $JobTitle = $user_job;
    $ContPrLevel = $ContPrLevel;
    $Department = $user_department;
    $AptSte = '';
    $Passenger = $passenger;
    $billing_Contact = $bill;
    $DateCreated = date("Y-m-d");
    $DateUpdated = date("Y-m-d");
    $ContExtraFldFL1 = '24';
    $ContExtraFldFL2 = '23';
    $IdAgent = '45';
    $ContAgentRate = 0;
    $IdAgrTmpl = 0;
    $IdWebLogin = 0;
    $IsCustomContAcctNumber = 0;
    $faxnumber = $ContFax;
    $ContStatus = $ContStatus;
    $ContWebAccess = $ContWebAccess;
    $ContFax = $ContFax;
    $ContTerms = $contTerm;

    $account_param = array(
        'ContPrLevel' => $ContPrLevel,
        'ContStatus' => $ContStatus,
        'ContWebAccess' => $ContWebAccess,
        'ContPrefix' => $prefix,
        'Prefix' => $prefix,
        'IdCont' => $idCont,
        'IdComp' => $idComp,
        'ContGrat' => $contGrat,
        'ContDiscount' => $ContDiscount,
        'ContOtherSurchPerc' => $ContOtherSurchPerc,
        'ContVoucherFee' => $ContVoucherFee,
        'IdParent' => $idParent,
        'CompanyName' => $company_name,
        'PrimaryAddress' => $address,
        'StateProv' => $state,
        'FirstName' => $fname,
        'LastName' => $lname,
        'JobTitle' => $JobTitle,
        'CityTown' => $city,
        'Country' => $country,
        'Department' => $Department,
        'AptSte' => $email,
        'ZipPost' => $zip,
        'BillingContact' => $billing_Contact,
        'DateCreated' => $DateCreated,
        'DateUpdated' => $DateUpdated,
        'ContExtraFldFL1' => $ContExtraFldFL1,
        'ContExtraFldFL2' => $ContExtraFldFL2,
        'ContExtraFldFL3' => $ContExtraFldFL2,
        'IdAgent' => $IdAgent,
        'ContAgentRate' => $ContAgentRate,
        'IdAgent2' => $IdAgent,
        'ContAgent2Rate' => $ContAgentRate,
        'IdAgrTmpl' => $IdAgrTmpl,
        'IdWebLogin' => $IdWebLogin, 'IsCustomContAcctNumber' => $IsCustomContAcctNumber,
        'Passenger' => $Passenger, 'ContType3' => $ContType3, 'MobilePhone' => $phone, 'ContPhone1' => $office_phone,
        'ContPhone2' => $home_phone, 'ContEmail' => $email, 'UserName' => $email, 'Fax' => $faxnumber,
        'Password' => $pwd, 'VerifyPassword' => $verify_pwd);
    $result = $action . 'Result';
    $mainarray = array('apiId' => 'UySGaySkGxFjA28', 'apiKey' => 'Ypk882t5QfWEgqrorQRJ', 'idCont' => $accountId, 'accountInfo' => $account_param);
    $trans = $soapClient->$action($mainarray)->$result;
    return $trans;
}

?>