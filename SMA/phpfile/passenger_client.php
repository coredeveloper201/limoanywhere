<?php

include_once 'passenger_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
	case "getRateMatrix":
	$response=getRateMatrix();
	echo json_encode($response);
	break;
	
	case "setPassengerRate":		
	$response=setPassengerRate();
	echo json_encode($response);
	break;	
	
	case "getRateMatrixList":	
	$response=getRateMatrixList();
	echo json_encode($response);
	break;
		
	case "editRateMatrix":		
	$response=editRateMatrix();
	echo json_encode($response);
	break;
		
	case "deletePassengerRate":
	$response=deletePassengerRate();
	echo json_encode($response);
	break;
	
	case "checkUniqueMatrix":		
	$response=checkUniqueMatrix();
	echo json_encode($response);
	break;
}

