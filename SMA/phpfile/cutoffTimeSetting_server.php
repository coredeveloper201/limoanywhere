<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

	/*****************************************************************
	Method:             deleteCutoff()
	InputParameter:     rowId
	Return:             deleteCutoff
	*****************************************************************/
	function deleteCutoff()
	{
		$row_id=$_REQUEST['rowId'];
		$query="delete from cutoff_time_table where id='".$row_id."'";
		$resource = operations($query);
		$query2="delete from cutoff_service_type where parent_id='".$row_id."'";
		$resource2 = operations($query2);
		$result=global_message(200,1007);
	   	return  $result;
	}

	/*****************************************************************
	Method:             getCutoffSettingRow()
	InputParameter:     rowId, user_id
	Return:             getCutoffSettingRow
	*****************************************************************/
	function getCutoffSettingRow()
	{
		$user_id=$_REQUEST['user_id'];
		$row_id=$_REQUEST['rowId'];
		$query="select * from cutoff_time_table where id='".$row_id."' order by cutoff_save asc";
		$resource = operations($query);
		$result=[];
		for($i=0; $i<count($resource); $i++)
		{
			$cutOfferQuery="Select service_type ,b.service_name from cutoff_service_type a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";
			$cutOfferQueryResult = operations($cutOfferQuery);
			$resource[$i]['service_type_array']=$cutOfferQueryResult;
		}

		if(count($resource)>=1 && gettype($resource)!="boolean")
		{
			$result=global_message(200,1006,$resource);
		}	
		else
		{
			$result=global_message(200,1007);
		}
		
		return  $result;
	}

	/*****************************************************************
	Method:             getCutoffSettingRow()
	InputParameter:    	user_id
	Return:             getCutoffSettingRow
	*****************************************************************/
	function getCutoffSetting()
	{
		$user_id=$_REQUEST['user_id'];
		$query="select * from cutoff_time_table where user_id='".$user_id."'";
		$resource = operations($query);
		$result=[];
		for($i=0; $i<count($resource); $i++)
		{
			$cutOfferQuery="Select service_type ,b.service_name from cutoff_service_type a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";
			$cutOfferQueryResult = operations($cutOfferQuery);
			$resource[$i]['service_type_array']=$cutOfferQueryResult;			
		}
		if(count($resource)>=1 && gettype($resource)!="boolean")
		{
			$result=global_message(200,1006,$resource);
		}	
		else
		{
			$result=global_message(200,1007);
		}
		return  $result;
	}


	/*****************************************************************
	Method:             updateCutoffTime()
	InputParameter:    	user_id
	Return:             update Cut off Time
	*****************************************************************/
	function updateCutoffTime()
	{
		$serviceType=json_decode($_REQUEST['service_type']);
		$cutoffTime=$_REQUEST['hhTime'].':'.$_REQUEST['mmTime'];
		$save_as=$_REQUEST['save_as'];
		$cutoffMessage=$_REQUEST['cutoffMessage'];
		$networkCheckBox=$_REQUEST['networkCheckBox'];
		$user_id=$_REQUEST['user_id'];
		$row_id=$_REQUEST['rowId'];
		$hhTime=$_REQUEST['hhTime'];
		$mmTime=$_REQUEST['mmTime'];
		$deleteClidTable="delete from cutoff_service_type where parent_id='".$row_id."'";
		$deleteClidTableResult = operations($deleteClidTable);
	  	$query="select * from cutoff_time_table where cutoff_save='".$save_as."' and id<>'".$row_id."'";
	  	$resource = operations($query);
		if(count($resource) < 1)
		{
			$cutoffTimeQuery="update cutoff_time_table set cutoff_save='".$save_as."',hh_time='".$hhTime."',mm_time='".$mmTime."',cutoff_message='".$cutoffMessage."',network_status='".$networkCheckBox."' where id='".$row_id."'";		
			$cutoffTimeQueryresult = operations($cutoffTimeQuery);
			for($i=0; $i<count($serviceType); $i++)
			{
				$queryParentTable="insert into cutoff_service_type(parent_id,service_type) values('".$row_id."','".$serviceType[$i]."')";
				$queryParentTableResult = operations($queryParentTable);
			}	
			$result=global_message(200,1006);
			return  $result;
		}
		else
		{
			$result=global_message(200,1007);
		   	return  $result;
		}
	}


	/*****************************************************************
	Method:             setCutoffTime()
	InputParameter:    	service_type
	Return:             set Cut off Time
	*****************************************************************/

	function setCutoffTime()
	{
		$serviceType=json_decode($_REQUEST['service_type']);
		$cutoffTime= $_REQUEST['hhTime'].':'.$_REQUEST['mmTime'];
		$save_as=$_REQUEST['save_as'];
		$cutoffMessage=$_REQUEST['cutoffMessage'];
		$networkCheckBox=$_REQUEST['networkCheckBox'];
		$user_id=$_REQUEST['user_id'];
		$hhTime=$_REQUEST['hhTime'];
		$mmTime=$_REQUEST['mmTime'];
		$cutoffTime=explode(' ',$cutoffTime);

	  	if(isset($cutoffTime[1]) && $cutoffTime[1]=="pm")
	  	{
		  	$cutoffTime1=explode(':',$cutoffTime[0]);
			if($cutoffTime1[0]==12)
		  	{
				$cutoffTime2=$cutoffTime1[0];
		  		$cutoffTime=$cutoffTime2.":".$cutoffTime1[1];
		  	}
		  	else
		  	{
				$cutoffTime2=$cutoffTime1[0]+12;
		  		$cutoffTime=$cutoffTime2.":".$cutoffTime1[1];
		  	}
		}
		else
		{
			$cutoffTime1=explode(':',$cutoffTime[0]);
		  	if($cutoffTime1[0]==12)
		  	{
		  		$cutoffTime="00:".$cutoffTime1[1];
		  	}
		  	else
		  	{
		  		$cutoffTime=$cutoffTime[0];
		  	}
		}

		$query="select * from cutoff_time_table where cutoff_save='".$save_as."'";
		$resource = operations($query);
		if(count($resource) < 1)
		{
			$cutoffTimeQuery="insert into cutoff_time_table(cutoff_save,cutoff_time,cutoff_message,network_status,user_id,hh_time,mm_time) values('".$save_as."','".$cutoffTime."','".$cutoffMessage."','".$networkCheckBox."','".$user_id."','".$hhTime."','".$mmTime."')";
            $last_insertid = operations($cutoffTimeQuery);
			for($i=0; $i<count($serviceType); $i++)
			{
				$queryParentTable="insert into cutoff_service_type(parent_id,service_type) values('".$last_insertid."','".$serviceType[$i]."')";
				$queryParentTableResult = operations($queryParentTable);
			}	
			$result=global_message(200,1006);
		   	return  $result;
		}
		else
		{
			$result=global_message(200,1007);
		   	return  $result;
		}

	}

?>