<?php

include_once 'hourly_server.php';
	$fullresult=json_decode($_REQUEST['FullResult']);
	$action = $fullresult[0]->action;

$response=array();
switch ($action) {
	case "updateHourlyRate":		
	$response=updateHourlyRate($fullresult[0]);
	echo json_encode($response);
	break;

	case "viewSpecialHourly":
	$response=viewSpecialHourly($fullresult[0]);
	echo json_encode($response);
	break;

	case "deleteHourlyRate":
	$response=deleteHourlyRate($fullresult[0]);
	echo json_encode($response);
	break;

	case "getHourlyRate":
	$response=getHourlyRate($fullresult[0]);
	echo json_encode($response);
	break;

	case "setHourlyRate":
	$response=setHourlyRate($fullresult[0]);
	echo json_encode($response);
	break;
}