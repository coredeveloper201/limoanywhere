<?php

include_once 'discount_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
	case "setDiscount":
	$response=setDiscount();
	echo json_encode($response);
	break;

	case "GetUsers":
	$response=GetUsers();
	echo json_encode($response);
	break;

	case "getDiscountCuponList":
	$response=getDiscountCuponList();
	echo json_encode($response);
	break;

	case "deleteDiscountCuponList":		
	$response=deleteDiscountCuponList();
	echo json_encode($response);
	break;

	case "viewDiscountCuppon":	
	$response=viewDiscountCuppon();
	echo json_encode($response);
	break;

	case "updateCupponData":
	$response=updateCupponData();
 	echo json_encode($response);
	break;
}
?>