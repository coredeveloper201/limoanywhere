/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: SMA Seaport
    Author: Mylimoproject
---------------------------------------------*/
/*create a class name for the sma seaport*/
$(function () {
    $('#refresh_overlay').show();
});
var getServiceVehicleType = {
    /*set a web service path for webservice*/
    _SERVICEPATH: "phpfile/service.php",
    /*---------------------------------------------
        Function Name: getVehicle()
        Input Parameter: user_id
        return:json data
     ---------------------------------------------*/
    getVehicle: function () {
        var getLocalStoragevalue = window.localStorage.getItem("apiInformation");
        if (typeof(getLocalStoragevalue) == "string") {
            getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
        }
        var getuserId = window.localStorage.getItem("companyInfo");
        if (typeof(getuserId) == "string") {
            getuserId = JSON.parse(getuserId);
        }
        $.ajax({
            url: getServiceVehicleType._SERVICEPATH,
            type: 'POST',
            data: "action=GetVehicleTypes&limoApiID=" + getLocalStoragevalue.limo_any_where_api_id + "&user_id=" + getuserId[0].id + "&limoApiKey=" + getLocalStoragevalue.limo_any_where_api_key,
            success: function (response) {
                $('#refresh_overlay').hide();
                var k = 1;
                var responseHTML = '';
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                var getFinal = [];
                for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {
                    getFinal[i] = responseObj.VehicleTypes.VehicleType[i].PassengerCapacity + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '@' + responseObj.VehicleTypes.VehicleType[i].VehTypeId;

                }
                getFinal.sort();
                var d = 0;
                for (var j = 0; j < getFinal.length; j++) {
                    var carsplitResult = getFinal[j].split('@');
                    var k = 0;
                    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {
                        if (carsplitResult[1] == responseObj.VehicleTypes.VehicleType[i].VehTypeId) {
                            if (typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1) != "undefined" && typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1) != undefined) {
                                if (d == 0) {
                                    responseHTML += '<tr><td><input type="checkbox" class="cmnclass" seq="' + d + '" name="vehicle" value="checked"></td><td class="vehicle_' + d + '" >' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '</td><td><input type="text" style="width:50%" class="phpfeesCommand phpfess' + d + '" disabled><input type="checkbox" name="vehicle"   class="phpfesscheck' + d + '" value="Bike"></td><td><input type="text" style="width:50%" disabled class="domaticCLass' + d + '"><input type="checkbox" name="vehicle"  class="domaticCLasscheck' + d + '" value="Bike"></td><td><input type="text" disabled class="fhvclass' + d + '" style="width:50%"><input type="checkbox"  class="fhvclasscheck' + d + '" name="vehicle" value="Bike"></td></tr>';
                                }
                                else {
                                    responseHTML += '<tr><td><input type="checkbox" class="cmnclass" seq="' + d + '" name="vehicle" value="checked"></td><td class="vehicle_' + d + '">' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '</td><td><input type="text" style="width:50%" class=" phpfeesCommand phpfess' + d + '" disabled></td><td><input type="text" style="width:50%" disabled class="domaticCLass' + d + '"></td><td><input type="text" disabled class="fhvclass' + d + '" style="width:50%"></tr>';
                                }
                                d++;
                            }

                        }
                    }
                }

                $('.airportVehicleInfo').html(responseHTML + '<br><br>');
                /*click on the button to get the data*/
                $('.cmnclass').on("click", function () {

                    var getSeq = $(this).attr('seq');
                    if ($(this).is(":checked")) {

                        $('.phpfess' + getSeq).removeAttr("disabled").prop("required", true).val('');
                        $('.domaticCLass' + getSeq).removeAttr("disabled").prop("required", true).val('');
                        $('.intrfclass' + getSeq).removeAttr("disabled").prop("required", true).val('');
                        $('.fhvclass' + getSeq).removeAttr("disabled").prop("required", true).val('');

                    }
                    else {

                        $('.phpfess' + getSeq).prop("disabled", true).removeAttr("required").val('');
                        $('.domaticCLass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                        $('.intrfclass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                        $('.fhvclass' + getSeq).prop("disabled", true).removeAttr("required").val('');

                    }
                });


                /* FHV code start here */
                $('.fhvclasscheck0').on("change", function () {
                    var getUndesablevalue = 0;
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        if ($(this).is(":checked")) {
                            if (!$('.fhvclass' + getSeq).attr('disabled')) {
                                if (getUndesablevalue == 0) {
                                    getUndesablevalue = $('.fhvclass0').val();
                                    $('.fhvclass' + getSeq).val(getUndesablevalue);
                                }
                                else {
                                    $('.fhvclass' + getSeq).val(getUndesablevalue);

                                }

                            }

                        }

                    });
                });

                /* international code start here  */
                $('.intrfclasscheck0').on("change", function () {
                    var getUndesablevalue = 0;
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        if ($(this).is(":checked")) {
                            if (!$('.intrfclass' + getSeq).attr('disabled')) {
                                if (getUndesablevalue == 0) {
                                    getUndesablevalue = $('.intrfclass' + getSeq).val();

                                }
                                else {

                                    $('.intrfclass' + getSeq).val(getUndesablevalue);
                                }

                            }
                        }
                    });

                });

                /* international code end here*/

                /* domestic code start here */
                $('.domaticCLasscheck0').on("change", function () {
                    var getUndesablevalue = 0;
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        if ($(this).is(":checked")) {
                            if (!$('.domaticCLass' + getSeq).attr('disabled')) {
                                if (getUndesablevalue == 0) {
                                    getUndesablevalue = $('.domaticCLass0').val();
                                    $('.domaticCLass' + getSeq).val(getUndesablevalue);
                                }
                                else {
                                    $('.domaticCLass' + getSeq).val(getUndesablevalue);
                                }

                            }
                        }
                    });
                });

                /* code end here */
                $('.phpfesscheck0').on("change", function () {
                    var getUndesablevalue = 0;
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        if ($(this).is(":checked")) {
                            if (!$('.phpfess' + getSeq).attr('disabled')) {
                                if (getUndesablevalue == 0) {
                                    getUndesablevalue = $('.phpfess0').val();
                                    $('.phpfess' + getSeq).val(getUndesablevalue);
                                }
                                else {
                                    $('.phpfess' + getSeq).val(getUndesablevalue);
                                }
                            }

                        }

                    });

                });
            }
        });
    }
}


/* new client service start here */
getServiceVehicleType.getVehicle();
/* new client service end here */

/*cretate the city class */
var getCityInfomation = {

    /*set the server path for the php*/
    _Serverpath: "phpfile/sma_client.php",

    /*---------------------------------------------
        Function Name: getSmaCityInformation()
        Input Parameter: smaId
        return:json data
    ---------------------------------------------*/
    getSmaCityInformation: function (smaId) {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
    },

    /*---------------------------------------------
        Function Name: delseSmaSeaportInformation()
        Input Parameter: getRowId
        return:json data
    ---------------------------------------------*/
    delseSmaSeaportInformation: function (getRowId) {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=delseSmaSeaportInformation&rowId=" + getRowId + "&type=seaport&user_id=" + getUserId[0].id,
            success: function (response) {
                $('#refresh_overlay').hide();
                getCityInfomation.getSmaAirportInformation();
            }
        });

    },

    /*---------------------------------------------
        Function Name: showEditItem()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    showEditItem: function (getid) {
        $('#refresh_overlay').css("display", "block");
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditSeaPortItem&type=seaport&rowId=" + getid + "&user_id=" + getUserId[0].id,
            success: function (response) {
                $('#refresh_overlay').hide();
                var responseJson = response;
                if (typeof(response) == "string") {
                    responseJson = JSON.parse(response);
                }
                $('#add_seaport_sma').val(responseJson.data[0][0].sma_id);
                $('#comment').val(responseJson.data[1][0].qnr_contant);
                $('.fhv_fee').val(responseJson.data[1][0].seaport_fee);
                $('.fhv_diclaimer').val(responseJson.data[1][0].seaport_contant);
                if (responseJson.data[1][0].is_inside_meet_check == 1) {
                    $('.inside_meet_greet_check').prop("checked", true);
                    $('.inside_meet_greet').removeAttr("disabled").prop("required", true).val(responseJson.data[1][0].inside_meet_text);
                }
                if (responseJson.data[1][0].is_curbside_check == 1) {
                    $('.curbside_check').prop("checked", true);
                    $('.curbside_text').removeAttr("disabled").prop("required", true).val(responseJson.data[1][0].curbside_text);
                }
                $('.seaportDeclairContent').val(responseJson.data[1][0].seaport_pickup_fee);
                $('.cmnclass').each(function () {
                    var getSeq = $(this).attr('seq');
                    $(this).removeAttr("checked");
                    $('.phpfess' + getSeq).prop("disabled", true).removeAttr("required").val('');
                    $('.domaticCLass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                    $('.fhvclass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                });
                for (var x = 0; x < responseJson.data[2].length; x++) {
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        var vehicleName = $('.vehicle_' + getSeq).html();
                        if (vehicleName == responseJson.data[2][x].vehicle_code) {
                            $(this).prop("checked", true);
                            $('.phpfess' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].perhour_fees);
                            $('.domaticCLass' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].sea_port_schg);
                            $('.fhvclass' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].fhv_access_fee);
                        }
                    });
                }

            }
        });
    },

    /*---------------------------------------------
        Function Name: showEditItemSecond()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    showEditItemSecond: function (getid) {
        $('#refresh_overlay').css("display", "block");
        var getAirportDbLocalStorage2 = window.localStorage.getItem("seaport_db_table_id");
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditSeaPortItem&type=seaport&rowId=" + getid + "&user_id=" + getUserId[0].id,
            success: function (response) {
                var responseJson = response;
                if (typeof(response) == "string") {
                    responseJson = JSON.parse(response);
                }
                getAirportDbLocalStorage = JSON.parse(getAirportDbLocalStorage2);
                // $('#add_seaport_sma').val(responseJson.data[0][0].sma_id);
                $('#seaport_zipcode').val(responseJson.data[0][0].type_zip);
                $('#seaport_name').val(responseJson.data[0][0].type_name).prop("disabled", true);
                $('#seaport_code').val(getAirportDbLocalStorage.seaport_db_code).prop("disabled");
                $('#seaport_name').val(getAirportDbLocalStorage.seaport_db_name).prop("disabled");
                $('#comment').val(responseJson.data[1][0].qnr_contant);
                $('.fhv_fee').val(responseJson.data[1][0].seaport_fee);
                $('.fhv_diclaimer').val(responseJson.data[1][0].seaport_contant);
                if (responseJson.data[1][0].is_inside_meet_check == 1) {
                    $('.inside_meet_greet_check').prop("checked", true);
                    $('.inside_meet_greet').removeAttr("disabled").prop("required", true).val(responseJson.data[1][0].inside_meet_text);
                }

                if (responseJson.data[1][0].is_curbside_check == 1) {
                    $('.curbside_check').prop("checked", true);
                    $('.curbside_text').removeAttr("disabled").prop("required", true).val(responseJson.data[1][0].curbside_text);
                }

                $('.seaportDeclairContent').val(responseJson.data[1][0].seaport_pickup_fee);
                $('.cmnclass').each(function () {
                    var getSeq = $(this).attr('seq');
                    $(this).removeAttr("checked");
                    $('.phpfess' + getSeq).prop("disabled", true).removeAttr("required").val('');
                    $('.domaticCLass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                    $('.fhvclass' + getSeq).prop("disabled", true).removeAttr("required").val('');
                });

                for (var x = 0; x < responseJson.data[2].length; x++) {
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        var vehicleName = $('.vehicle_' + getSeq).html();
                        if (vehicleName == responseJson.data[2][x].vehicle_code) {
                            $(this).prop("checked", true);
                            $('.phpfess' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].perhour_fees);
                            $('.domaticCLass' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].sea_port_schg);
                            $('.fhvclass' + getSeq).removeAttr("disabled").prop("required", true).val(responseJson.data[2][x].fhv_access_fee);
                        }
                    });

                }
                $('#add_seaport_location').html("Update");
                $('#add_seaport_location').attr("seq", getid);
                $('#refresh_overlay').hide();
            }

        });

    },

    /*---------------------------------------------
        Function Name: showViewItem()
        Input Parameter: getid
        return:json data
    ---------------------------------------------*/
    showViewItem: function (getid) {
        $('#refresh_overlay').css("display", "block");
        var getAirportDbLocalStorage2 = window.localStorage.getItem("seaport_db_table_id");
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=showEditSeaPortItem&type=seaport&rowId=" + getid + "&user_id=" + getUserId[0].id,
            success: function (response) {
                var responseJson = response;
                if (typeof(response) == "string") {
                    responseJson = JSON.parse(response);
                }
                getAirportDbLocalStorage = JSON.parse(getAirportDbLocalStorage2);
                $('#add_seaport_sma').val(responseJson.data[0][0].sma_id);
                $('#seaport_zipcode').val(responseJson.data[0][0].type_zip);
                $('#seaport_code').val(getAirportDbLocalStorage.seaport_db_code).prop("disabled");
                $('#seaport_name').val(getAirportDbLocalStorage.seaport_db_name).prop("disabled");
                $('#comment').val(responseJson.data[1][0].qnr_contant);
                $('.fhv_fee').val(responseJson.data[1][0].seaport_fee);
                $('.fhv_diclaimer').val(responseJson.data[1][0].seaport_contant);
                if (responseJson.data[1][0].is_inside_meet_check == 1) {
                    $('.inside_meet_greet_check').prop("checked", true);
                    $('.inside_meet_greet').val(responseJson.data[1][0].inside_meet_text);
                }
                if (responseJson.data[1][0].is_curbside_check == 1) {
                    $('.curbside_check').prop("checked", true);
                    $('.curbside_text').val(responseJson.data[1][0].curbside_text);
                }

                $('.seaportDeclairContent').val(responseJson.data[1][0].seaport_pickup_fee);

                for (var x = 0; x < responseJson.data[2].length; x++) {
                    $('.cmnclass').each(function () {
                        var getSeq = $(this).attr('seq');
                        var vehicleName = $('.vehicle_' + getSeq).html();
                        if (vehicleName == responseJson.data[2][x].vehicle_code) {
                            $(this).prop("checked", true);
                            $('.phpfess' + getSeq).val(responseJson.data[2][x].perhour_fees);
                            $('.domaticCLass' + getSeq).val(responseJson.data[2][x].sea_port_schg);
                            //$('.intrfclass'+x).val(responseJson.data[2][x].int_flt_schg);
                            $('.fhvclass' + getSeq).val(responseJson.data[2][x].fhv_access_fee);
                        }
                    });
                }

                $('#add_seaport_location').hide();
                $('#add_seaport_location').attr("seq", getid);
                $('#refresh_overlay').hide();
            }
        });
    },


    /*---------------------------------------------
        Function Name: getSmaAirportInformation()
        return:json data
    ---------------------------------------------*/
    getSmaAirportInformation: function () {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }

        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=getSmaAirportInformation&type=seaport&user_id=" + getUserId[0].id,
            success: function (response) {
                var responseJson = response;
                var finalSelectBox = '';
                var selectBoxMatrix = '';
                if (typeof(response) == "string") {
                    responseJson = JSON.parse(response);

                }
                selectBoxMatrix += '<option>Select</option>';
                $.each(responseJson.data, function (index, result) {
                    if (result.id != null) {
                        finalSelectBox += '<tr><td>' + result.sma_name + '</td><td>' + result.type_name + '</td><td>' + result.save_as + '</td><td>' + result.type_zip + '</td><td><button class="btn btn-primary viewSma" seq="' + result.id + '" > View</button></td><td><a class="btn btn-xs editAirport" seq="' + result.id + '" >Edit</a><a class="btn btn-xs deleteSeaport" seq="' + result.id + '" >Delete</a></td></tr>';
                        selectBoxMatrix += "<option value=" + result.id + ">" + result.type_name + "</option>";
                    }
                    else {
                        finalSelectBox = '<tr><td colspan="4">Data not Found.</td></tr>';
                        selectBoxMatrix += "<option value='noairport'>No Airport</option>";
                    }
                });
                $('#sma_seaport_list').html(finalSelectBox);
                $('#select_seaport_matrix_db').html(selectBoxMatrix);
                $('.viewSma').on("click", function () {
                    var getseq = $(this).attr("seq");
                    getCityInfomation.showViewItem(getseq);
                })

                $('.editAirport').on("click", function () {
                    var getseq = $(this).attr("seq");
                    getCityInfomation.showEditItem(getseq);
                })

                $('.deleteSeaport').on("click", function () {
                    var getseq = $(this).attr("seq");
                    getCityInfomation.delseSmaSeaportInformation(getseq);
                });

                $('#copy_rate_matrix_seaport').on("click", function () {
                    var getseq = $('#select_seaport_matrix_db').val();
                    $('#seaport_name').removeAttr("disabled");
                    $('#seaport_code').removeAttr("disabled");
                    getCityInfomation.showEditItem(getseq);
                })
            }
        });
    },

    /*---------------------------------------------
        Function Name: getSmaInformation()
        return:json data
    ---------------------------------------------*/
    getSmaInformation: function () {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        var seaportDB = JSON.parse(localStorage.seaport_db_table_id);
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            data: "action=getSMA&user_id=" + getUserId[0].id + "&zip_code=" + seaportDB.seaport_db_zipcode,
            success: function (response) {
                var responseJson = response;
                var finalSelectBox = '';
                if (typeof(response) == "string") {
                    responseJson = JSON.parse(response);
                }
                finalSelectBox = '<option value="">select SMA</option>';
                var exist = 0;
                $.each(responseJson.data, function (index, result) {
                    if (result.selected === 1) {
                        exist = 1;
                        finalSelectBox += '<option selected="selected" value="' + result.id + '">' + result.sma_name + '</option>';
                    } else {
                        finalSelectBox += '<option value="' + result.id + '">' + result.sma_name + '</option>';
                    }
                });

                $('#add_seaport_sma').html(finalSelectBox);
                if (exist === 0) {
                    $('#add_seaport_sma').parent().append('<p class="zipVerify" style="color: red;">Verify that zipcode/city exist within existing database or edit and add it or create new SMA and include zipcode</p>');
                }
                $('#add_seaport_sma').on("change", function () {
                    var getValue = $(this).val();
                    getCityInfomation.getSmaCityInformation(getValue);
                    $('.zipVerify').remove();
                });
            }
        });

    }
}

getCityInfomation.getSmaInformation();
getCityInfomation.getSmaAirportInformation();

$('#seaportZoneForm').on("submit", function (e) {
    e.preventDefault();
    if ($('#add_seaport_location').html() == "Submit") {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }

        var getForm = new FormData($('#seaportZoneForm')[0]);
        var commanArray = [];
        $('.cmnclass').each(function () {
            var getSeq = $(this).attr('seq');
            if ($(this).is(':checked')) {
                var vehiclCode = $('.vehicle_' + getSeq).html().trim();
                var domaticCLass = $('.domaticCLass' + getSeq).val().trim();
                var phpfess = $('.phpfess' + getSeq).val();
                var fhvclass = $('.fhvclass' + getSeq).val();
                commanArray.push({
                    "vehiclCode": vehiclCode,
                    "phpfess": phpfess,
                    "fhvclass": fhvclass,
                    "domaticCLass": domaticCLass
                });
            }
        });

        commanArray = JSON.stringify(commanArray);
        var getQandRMsg = $('#comment').val();
        var fhv_fee = $('.fhv_fee').val();
        var fhv_diclaimer = $('.fhv_diclaimer').val();
        var seaportDeclairContent = $('.seaportDeclairContent').val();
        var inside_meet_greet = '';
        if ($('.inside_meet_greet_check').is(":checked")) {
            inside_meet_greet = $('.inside_meet_greet').val();
            getForm.append("inside_meet_greet", inside_meet_greet);
            getForm.append("inside_meet_greet_check", '1');
        }
        else {
            inside_meet_greet = '';
            getForm.append("inside_meet_greet", inside_meet_greet);
            getForm.append("inside_meet_greet_check", '0');
        }

        var curbside_check = '';
        if ($('.curbside_check').is(":checked")) {
            curbside_check_text = $('.curbside_text').val();
            getForm.append("curbside_check_text", curbside_check_text);
            getForm.append("curbside_check", '1');
        }
        else {
            getForm.append("curbside_check", '0');
        }

        var getseaportDeclairContent = $('.seaportDeclairContent').val()
        getForm.append("getseaportDeclairContent", getseaportDeclairContent);
        getForm.append("getQandRMsg", getQandRMsg);
        getForm.append("fhv_fee", fhv_fee);
        getForm.append("fhv_diclaimer", fhv_diclaimer);
        getForm.append("vehicle_code", commanArray);
        getForm.append("airport_code", airport_code);
        var smaId = $('#add_seaport_sma').val();
        var airport_zipcode = $('#seaport_zipcode').val();
        var seaport_name = $('#seaport_name').val();
        var airport_code = $('#seaport_code').val();
        getForm.append("action", "setSmaDataSeaportZone");
        var getAirportDbLocalStorage2 = window.localStorage.getItem("seaport_db_table_id");
        if (getAirportDbLocalStorage2 != "string12") {
            getAirportDbLocalStorage2 = JSON.parse(getAirportDbLocalStorage2);
            getForm.append("seaport_db_id", getAirportDbLocalStorage2.airport_db_id);
        }

        getForm.append("smaId", smaId);
        getForm.append("airport_zipcode", airport_zipcode);
        getForm.append("airport_name", seaport_name);
        getForm.append("airport_code", airport_code);
        getForm.append("type", "seaport");
        getForm.append("user_id", getUserId[0].id);
        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: getForm,
            success: function (response) {
                var responseJson = response;
                if (typeof(responseJson) == "string") {
                    responseJson = JSON.parse(responseJson);
                }
                if (responseJson.code == 1006) {
                    console.log()
                    if (responseJson.data == 'airportCode') {
                        alert("Seaport code should be unique.");
                    }
                    else if (responseJson.data == 'airportName') {
                        alert("Seaport name should be unique.");
                    }
                    else if (responseJson.data == 'zipCode') {
                        alert("Seaport Zip code should be unique.");
                    }
                }
                else {
                    alert("Suceessfully inserted");
                    window.location.href = "seaport_db.html"
                }
            }
        });

    }
    else {
        var getId = $('#add_seaport_location').attr("seq");
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }
        var getForm = new FormData($('#seaportZoneForm')[0]);
        var commanArray = [];
        $('.cmnclass').each(function () {
            var getSeq = $(this).attr('seq');
            if ($(this).is(':checked')) {
                var vehiclCode = $('.vehicle_' + getSeq).html().trim();
                var domaticCLass = $('.domaticCLass' + getSeq).val();
                var phpfess = $('.phpfess' + getSeq).val();
                var fhvclass = $('.fhvclass' + getSeq).val();
                commanArray.push({
                    "vehiclCode": vehiclCode,
                    "phpfess": phpfess,
                    "fhvclass": fhvclass,
                    "domaticCLass": domaticCLass
                });
            }
        });

        commanArray = JSON.stringify(commanArray);

        var getQandRMsg = $('#comment').val();
        var fhv_fee = $('.fhv_fee').val();
        var fhv_diclaimer = $('.fhv_diclaimer').val();
        var seaportDeclairContent = $('.seaportDeclairContent').val();
        var inside_meet_greet = '';
        if ($('.inside_meet_greet_check').is(":checked")) {
            inside_meet_greet = $('.inside_meet_greet').val();
            getForm.append("inside_meet_greet", inside_meet_greet);
            getForm.append("inside_meet_greet_check", '1');
        }
        else {
            inside_meet_greet = '';
            getForm.append("inside_meet_greet", inside_meet_greet);
            getForm.append("inside_meet_greet_check", '0');
        }
        var curbside_check = '';
        if ($('.curbside_check').is(":checked")) {
            curbside_check_text = $('.curbside_text').val();
            getForm.append("curbside_check_text", curbside_check_text);
            getForm.append("curbside_check", '1');
        }
        else {
            getForm.append("curbside_check", '0');
        }

        var getseaportDeclairContent = $('.seaportDeclairContent').val()
        getForm.append("getseaportDeclairContent", getseaportDeclairContent);
        getForm.append("getQandRMsg", getQandRMsg);
        getForm.append("fhv_fee", fhv_fee);
        getForm.append("fhv_diclaimer", fhv_diclaimer);
        getForm.append("vehicle_code", commanArray);

        var smaId = $('#add_seaport_sma').val();
        var airport_zipcode = $('#seaport_zipcode').val();
        var seaport_name = $('#seaport_name').val();
        var airport_code = $('#seaport_code').val();

        getForm.append("action", "updateSmaDataSeaportZone");
        getForm.append("smaId", smaId);
        getForm.append("airport_zipcode", airport_zipcode);
        getForm.append("airport_name", seaport_name);
        getForm.append("airport_code", airport_code);
        getForm.append("type", "seaport");
        getForm.append("user_id", getUserId[0].id);
        getForm.append("rowId", getId);


        $.ajax({
            url: getCityInfomation._Serverpath,
            type: 'POST',
            processData: false,
            contentType: false,
            data: getForm,
            success: function (response) {
                var responseJson = response;
                if (typeof(responseJson) == "string") {
                    responseJson = JSON.parse(responseJson);
                }
                if (responseJson.code == 1006) {
                    if (responseJson.data == 'airportCode') {
                        alert("Seaport code should be unique.");
                    }
                    else if (responseJson.data == 'airportName') {
                        alert("Seaport name should be unique.");
                    }
                    else if (responseJson.data == 'zipCode') {
                        alert("Seaport Zip code should be unique.");
                    }
                }
                else {
                    alert("Successfully updated.");
                    //window.location.href="seaport_db.html";
                    location.reload();
                }
            }
        });
    }

});


var getAirportDbLocalStorage = window.localStorage.getItem("seaport_db_table_id");

var show_vehicle_count = 0;
var show_vehicle_interval;

function show_vehicle_value() {

    show_vehicle_interval = setInterval(function () {

        show_vehicle_count = show_vehicle_count + 1;
        if (show_vehicle_count == 20) { // it works like 20 seconds
            clearInterval(show_vehicle_interval);
        }

        if ($.trim($('.airportVehicleInfo').text()) != '') {

            clearInterval(show_vehicle_interval);

            if (getAirportDbLocalStorage.type == "edit") {
                getCityInfomation.showEditItemSecond(getAirportDbLocalStorage.mainIdLocal);
            }
            else if (getAirportDbLocalStorage.type == "view") {
                getCityInfomation.showViewItem(getAirportDbLocalStorage.mainIdLocal);
            }
            else {
                getCityInfomation.delseSmaAirportInformation(getAirportDbLocalStorage.mainIdLocal);
            }
        }


    }, 1000);

}

if (typeof(getAirportDbLocalStorage) != "object") {
    if (getAirportDbLocalStorage == "string12") {
        $('#seaport_name').removeAttr("disabled");
        $('#seaport_code').removeAttr("disabled");
    }
    else {
        getAirportDbLocalStorage = JSON.parse(getAirportDbLocalStorage);
        if (getAirportDbLocalStorage.mainIdLocal == "NoId") {
            $('#seaport_zipcode').val(getAirportDbLocalStorage.seaport_db_zipcode).prop("disabled");
            $('#seaport_code').val(getAirportDbLocalStorage.seaport_db_code).prop("disabled");
            $('#seaport_name').val(getAirportDbLocalStorage.seaport_db_name).prop("disabled");
        }
        else {

            show_vehicle_value();

            /*
		setTimeout(function(){
			if(getAirportDbLocalStorage.type=="edit"){
				getCityInfomation.showEditItemSecond(getAirportDbLocalStorage.mainIdLocal);
			}
			else if(getAirportDbLocalStorage.type=="view"){
				getCityInfomation.showViewItem(getAirportDbLocalStorage.mainIdLocal);
			}
			else{
				getCityInfomation.delseSmaAirportInformation(getAirportDbLocalStorage.mainIdLocal);	
			}

		},3000);
			*/
        }

    }

}
else {
    //location.reload();
    //window.location.href='seaport_db.html';
}



