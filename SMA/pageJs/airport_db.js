/*---------------------------------------------
Template Name: Mylimoproject
Page Name: Airport
Author: Mylimoproject
---------------------------------------------*/

/*click on the input on change function*/
$('#input-24').on("change", function () {

    airportDbClass.setImportDatabaseFIle();
});

/*add airport manual function start*/
$('#add_airport_manual').on("submit", function (event1) {
    event1.preventDefault();
    airportDbClass.setManualAirport();

});
/*airport db class */
var airportDbClass = {
    _Serverpath: "phpfile/airport_db_client.php",

    /*---------------------------------------------
       Function Name: getAiportCityAndCode()
       Input Parameter: rowId,categoryId
       return:json data
     ---------------------------------------------*/
    getAiportCityAndCode: function (rowId, categoryId) {
        var getUserId = window.localStorage.getItem('companyInfo');
        getUserId = JSON.parse(getUserId);

        $.ajax({
            url: this._Serverpath,
            type: 'POST',
            data: "action=getAiportCityAndCode&user_id=" + getUserId[0].id + "&stateName=" + rowId + "&categoryId=" + categoryId,
        }).done(function (response) {

            var responseJSON = response;
            if (typeof(response) == "string") {
                responseJSON = JSON.parse(response);

            }

            var responseHTML = '';
            if (responseJSON.code == 1007) {

                $.each(responseJSON.data, function (index, value) {
                    if (value.city_code != '') {
                        responseHTML += '<option value="' + value.city_name + "@" + (value.city_code) + '">' + value.city_name + "-" + (value.city_code) + '</option>';
                    }
                    else {
                        responseHTML += '<option value="' + value.city_name + '">' + value.city_name + '</option>';
                    }
                });

            } else {

                responseHTML += '<option>No city </option>';
            }
            $('#airport_db_city').html(responseHTML);
            $("#airport_db_city").val('');
            $("#airport_db_city").multiselect('destroy');
            $('#airport_db_city').multiselect({
                maxHeight: 184,
                buttonWidth: '155px',
                includeSelectAllOption: true,
                id: 'airport_db_city'
            });

            $('#airport_db_city_check').on("click", function () {
                var airport_db_cityValue = $('#airport_db_city').val();

                $.ajax({
                    url: airportDbClass._Serverpath,
                    type: 'POST',
                    data: "action=isAirportCheck&user_id=" + getUserId[0].id + "&airport_db_cityValue=" + airport_db_cityValue,
                }).done(function (response) {

                    var responseJSON = response;
                    if (typeof(response) == "string") {
                        responseJSON = JSON.parse(response);
                    }
                    var responseHTML = '';
                    if (responseJSON.code == 1007) {

                        $.each(responseJSON.data, function (index, value) {
                            if (value.airport_code != '') {
                                responseHTML += '<option value=' + value.id + '>' + value.airport_name + "-(" + value.airport_code + ")" + '</option>';
                            }
                            else {
                                responseHTML += '<option value=' + value.id + '>' + value.airport_name + '</option>';
                            }
                        });

                        $('#airport_db_city_check img').attr("src", "images/green_check.png");
                        $('#airport_db_airport_code').html(responseHTML);
                        $("#airport_db_airport_code").val('');
                        $("#airport_db_airport_code").multiselect('destroy');
                        $('#airport_db_airport_code').multiselect({
                            maxHeight: 184,
                            buttonWidth: '155px',
                            includeSelectAllOption: true,
                            id: 'airport_db_city'
                        });

                    } else {

                        alert("Airport Not Found");
                        $('#airport_db_city_check img').attr("src", "images/ok_checkmark_red_T.png");
                        $('#airport_db_airport_code').html(" ");
                        $("#airport_db_airport_code").val('');
                        $("#airport_db_airport_code").multiselect('destroy');
                        $('#airport_db_airport_code').multiselect({
                            maxHeight: 184,
                            buttonWidth: '155px',
                            includeSelectAllOption: true,
                            id: 'airport_db_city'
                        });

                    }

                });


            });

        });

    },


    /*---------------------------------------------
       Function Name: getCountryAirportDb()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
    getCountryAirportDb: function () {
        var getUserId = window.localStorage.getItem('companyInfo');
        getUserId = JSON.parse(getUserId);

        $.ajax({
            url: this._Serverpath,
            type: 'POST',
            data: "action=getCountryAirportDb&user_id=" + getUserId[0].id,
        }).done(function (response) {

            var responseJson = response;
            if (typeof(response) == "string") {

                responseJson = JSON.parse(response);

            }
            var responseHTML = '';
            responseHTML += '<option value="noState">Select Country</option>';
            if (responseJson.code == 1007) {
                $.each(responseJson.data, function (index, value) {
                    responseHTML += '<option value="' + value.airport_country + '">' + value.airport_country + '</option>';
                });
            }
            else {
                responseHTML += '<option>No Country</option>';

            }
            $('#airport_db_country').html(responseHTML);
            $('#airport_db_country').on("change", function () {
                var getValue = $(this).val();
                airportDbClass.getStateAirportDb(getValue);

            });

        });

    },

    /*---------------------------------------------
      Function Name: getStateAirportDb()
      Input Parameter:user_id,rowId
      return:json data
    ---------------------------------------------*/

    getStateAirportDb: function (rowId) {

        var getUserId = window.localStorage.getItem('companyInfo');
        getUserId = JSON.parse(getUserId);

        $.ajax({
            url: this._Serverpath,
            type: 'POST',
            data: "action=getStateAirportDb&user_id=" + getUserId[0].id + "&rowId=" + rowId,
        }).done(function (response) {

            var responseJson = response;
            if (typeof(response) == "string") {
                responseJson = JSON.parse(response);
            }

            var responseHTML = '';
            responseHTML += '<option value="noState">Select State</option>';

            if (responseJson.code == 1007) {
                $.each(responseJson.data, function (index, value) {
                    responseHTML += '<option value="' + value.state_name + '">' + value.state_name + '</option>';
                });
            }
            else {
                responseHTML += '<option> No State</option>';

            }

            $('#airport_db_state').html(responseHTML);
            $('#airport_db_state').on("change", function () {
                var getValue = $(this).val();
                $('#category_db_state').removeAttr("disabled");
                $('#category_db_state').val('');

                /* Reset all data from table start here */
                $('#airport_db_city').html(' ');
                $("#airport_db_city").multiselect('destroy');
                $('#airport_db_city').multiselect({
                    maxHeight: 184,
                    buttonWidth: '155px',
                    includeSelectAllOption: true,
                    id: 'airport_db_city'
                });

                /* Reset all data from table End here */

                $('#category_db_state').on("change", function () {
                    var getCategoryValue = $(this).val();
                    airportDbClass.getAiportCityAndCode(getValue, getCategoryValue);

                });

            });

        }).fail(function () {

            alert("Network Problem");
        });

    },

    /*---------------------------------------------
       Function Name: delseSmaAirportInformation()
       Input Parameter: getRowId,rowId2
       return:json data
     ---------------------------------------------*/
    delseSmaAirportInformation: function (getRowId, rowId2) {
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }
        $.ajax({

            url: "phpfile/sma_client.php",
            type: 'POST',
            data: "action=delseSmaAirportInformation&rowId=" + getRowId + "&type=airport&user_id=" + getUserId[0].id + "&rowId2=" + rowId2,
            success: function (response) {
                window.location.href = "";
            }
        });
    },

    /*---------------------------------------------
      Function Name: airport_activeFormSubmit()
      Input Parameter: getRowId,rowId2
      return:json data
    ---------------------------------------------*/
    airport_activeFormSubmit: function (rowId) {
        $('#refresh_overlay').css("display", "block");
        var getUserId = window.localStorage.getItem('companyInfo');

        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);
        }

        $.ajax({
            type: "POST",
            url: airportDbClass._Serverpath,
            data: "action=airport_activeFormSubmit&user_id=" + getUserId[0].id + "&aiportId=" + rowId,
        }).done(function (resultData) {

            $('#refresh_overlay').css("display", "none");
            location.reload();
            airportDbClass.getImportDatabaseFIle();

        });

    },

    /*---------------------------------------------
      Function Name: delseSmaAirportInformation()
      Input Parameter: user_id,
      return:json data
    ---------------------------------------------*/
    getImportDatabaseFIle: function () {
        $('#refresh_overlay').css("display", "block");

        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }
        $.ajax({
            type: "POST",
            url: airportDbClass._Serverpath,
            data: "action=getImportDatabaseFIle&user_id=" + getUserId[0].id,
            success: function (result) {
                var getJson = result;

                if (typeof(result) == "string") {
                    getJson = JSON.parse(result);
                }

                var resultHTML = '';

                if (getJson.code != 1006) {
                    for (var i = 0; i < getJson.data.length; i++) {

                        if (getJson.data[i].status == 1) {
                            resultHTML += '<tr><td>' + (i + 1) + '</td><td class="airport_db_code_' + getJson.data[i].id + '">' + getJson.data[i].airport_code + '</td><td class="airport_db_name_' + getJson.data[i].id + '">' + getJson.data[i].airport_name + '</td><td class="airport_db_zipcode_' + getJson.data[i].id + '">' + getJson.data[i].airport_zip_code + '</td><td><button class="btn btn-warning not_view_btn">View</button></td><td><a class="btn btn-xs editAirportDb" seq="' + getJson.data[i].id + '" mainId="NoId">Edit</a><a class="btn btn-xs deactivateAirportDb" seq="' + getJson.data[i].id + '">Deactivate</a></td></tr>';
                        }
                        else {

                            resultHTML += '<tr><td>' + (i + 1) + '</td><td class="airport_db_code_' + getJson.data[i].id + '">' + getJson.data[i].airport_code + '</td><td class="airport_db_name_' + getJson.data[i].id + '">' + getJson.data[i].airport_name + '</td><td class="airport_db_zipcode_' + getJson.data[i].id + '">' + getJson.data[i].airport_zip_code + '</td><td><button class="btn btn-primary viewAirportDb" seq="' + getJson.data[i].id + '" mainId="' + getJson.data[i].airport_id + '">View</td></td><td><a class="btn btn-xs editAirportDb" seq="' + getJson.data[i].id + '" mainId="' + getJson.data[i].airport_id + '">Edit</a><a class="btn btn-xs deactivateAirportDb" seq="' + getJson.data[i].id + '" mainId="' + getJson.data[i].airport_id + '">Deactivate</a></td></tr>';

                        }

                    }

                }
                else {

                    resultHTML += '<tr><td colspan="6">Data Not Found</td></tr>';

                }


                $('#airport_db_table').html(resultHTML);
                $('#refresh_overlay').css("display", "none");
                $('.not_view_btn').on("click", function () {

                    alert("Aiport you are trying to view is not activated yet. Please click on Edit button and fill details to activate ariport");

                });

                $('.deactivateAirportDb').on("click", function (e) {
                    e.preventDefault();
                    var getSeq = $(this).attr("seq");
                    var mainIdLocal = $(this).attr("mainId");
                    var confirmValue = confirm("Warning!  Are you sure, you wants to deactivate zone airport. Deactivating will erase all saved data for zone airport from system.");

                    if (confirmValue) {
                        airportDbClass.delseSmaAirportInformation(mainIdLocal, getSeq);

                    }

                });

                $('.viewAirportDb').on("click", function () {

                    var getSeq = $(this).attr("seq");
                    var mainIdLocal = $(this).attr("mainId");
                    var airport_db_code = $('.airport_db_code_' + getSeq).html().trim();
                    var airport_db_name = $('.airport_db_name_' + getSeq).html().trim();
                    var airport_db_zipcode = $('.airport_db_zipcode_' + getSeq).html().trim();
                    var getJson = {
                        "airport_db_id": getSeq,
                        "mainIdLocal": mainIdLocal,
                        "airport_db_code": airport_db_code,
                        "airport_db_name": airport_db_name,
                        "airport_db_zipcode": airport_db_zipcode,
                        "type": "view"
                    };
                    window.localStorage.setItem("airport_db_table_id", JSON.stringify(getJson));

                    window.location.href = 'sma_airport.html';

                });

                $('.editAirportDb').on("click", function () {

                    var getSeq = $(this).attr("seq");
                    var mainIdLocal = $(this).attr("mainId");
                    var airport_db_code = $('.airport_db_code_' + getSeq).html().trim();
                    var airport_db_name = $('.airport_db_name_' + getSeq).html().trim();
                    var airport_db_zipcode = $('.airport_db_zipcode_' + getSeq).html().trim();
                    var getJson = {
                        "airport_db_id": getSeq,
                        "mainIdLocal": mainIdLocal,
                        "airport_db_code": airport_db_code,
                        "airport_db_name": airport_db_name,
                        "airport_db_zipcode": airport_db_zipcode,
                        "type": "edit"
                    };
                    window.localStorage.setItem("airport_db_table_id", JSON.stringify(getJson));

                    window.location.href = 'sma_airport.html';

                });

            }
        });

    },

    /*---------------------------------------------
        Function Name: setImportDatabaseFIle()
        Input Parameter: user_id,
        return:json data
     ---------------------------------------------*/

    setImportDatabaseFIle: function () {

        $("#refresh_overlay").css('display', 'block');
        var getUserId = window.localStorage.getItem('companyInfo');

        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }

        var newformdata = new FormData($('#getformdata')[0]);
        newformdata.append("action", "setImportDatabaseFIle");
        newformdata.append("user_id", getUserId[0].id);

        $.ajax({
            type: "POST",
            url: this._Serverpath,
            data: newformdata,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json',

            success: function (response) {

                $("#refresh_overlay").css('display', 'none');

                if (response.code == 1006) {

                    alert("Data Imported successfully");

                }
                else {
                    $("#refresh_overlay").css('display', 'none');
                    alert("Some Error.");
                }
            }

        });

    },

    /*---------------------------------------------
      Function Name: setManualAirport()
      Input Parameter: user_id,
      return:json data
     ---------------------------------------------*/
    setManualAirport: function () {

        $("#refresh_overlay").css('display', 'block');
        var getUserId = window.localStorage.getItem('companyInfo');
        if (typeof(getUserId) == "string") {
            getUserId = JSON.parse(getUserId);

        }
        var newformdata = new FormData($('#add_airport_manual')[0]);
        newformdata.append("action", "setManualAirport");
        newformdata.append("user_id", getUserId[0].id);

        $.ajax({
            type: "POST",
            url: this._Serverpath,
            processData: false,
            contentType: false,
            data: newformdata,
            dataType: 'json',
            success: function (response) {
                $("#refresh_overlay").css('display', 'none');
                alert("Data Inserted successfully");
                location.reload();

            }

        });

    }
    /*END OF THE FUNCTION*/
}
/*END OF THE CLASS*/

/*call the function on data base load*/
airportDbClass.getImportDatabaseFIle();
airportDbClass.getCountryAirportDb();
/*call the function on form submit*/
$('#airportDbFormSubmit').on("submit", function (event1) {
    event1.preventDefault();
    var get_airport_db_airport_code_value = $('#airport_db_airport_code').val();
    airportDbClass.airport_activeFormSubmit(get_airport_db_airport_code_value);
});

/* form submit code End here */




