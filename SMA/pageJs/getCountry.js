/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: get country page
    Author: Mylimoproject
---------------------------------------------*/
/*set the path for the webservice*/
var _SERVICEPATH="phpfile/sma_client.php"; 

/*call GetCountries function on pageload*/
GetCountries();
/*---------------------------------------------
   Function Name: GetCountriesInPage()
   Input Parameter:user_id
   return:json data
---------------------------------------------*/
function GetCountries()
{ 
	$('#refresh_overlay').css("display","block");
    var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
		
    if(typeof(getLocalStoragevalueUserInformation)=="string")
	{
	   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
	}

    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
	}
	
    $.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaCountry&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {
			var responseHTML='';
			var responseObj=response;
			if(typeof(response)=="string")
			{
				responseObj=JSON.parse(response);					
			}
			$.each(responseObj.data,function(index,dataUse){
				if(dataUse.id!=null){
					responseHTML+='<tr ><td>'+(parseInt(index)+1)+'</td> <td>'+dataUse.country_name+'</td><td>'+dataUse.country_abbr+'</td><td><div><a class="btn btn-xs editCountry" seq="edit_'+dataUse.id+'"> Edit</a> <a class="btn btn-xs deleteCountry" seq="'+dataUse.id+'" >Delete</a> </div></td></tr>';
	            }else{

	                responseHTML+='<tr><td colspan="4">Data not Found.</td></tr>';	
	            }
	        });

			$('#CountryDetails').html(responseHTML);
			$('#refresh_overlay').css("display","none");
			$('.editCountry').on("click",function(){
				var getSeq=$(this).attr("seq");
				$('#add_edit_country').find('button').attr("seq",getSeq);
				newCountryFunction.getSmaCountrySpecific(getSeq);
			});
		    $('.deleteCountry').on("click",function(){
			    var getSeq=$(this).attr("seq");
				newCountryFunction.deleteSmaCountrySpecific(getSeq);

		    })
			
		}
	
	});	
   
}


/*create a class for the country function*/
var newCountryFunction= {
	/*set the service url using php path*/
	 _SERVICEPATHSecond:"phpfile/sma_client.php",
/*---------------------------------------------
    Function Name: deleteSmaCountrySpecific()
    Input Parameter:countryid
    return:json data
---------------------------------------------*/	 
	deleteSmaCountrySpecific:function(countryid)
	{
		$('#refresh_overlay').css("display","block");
	 	var getCountryId={
	 		"country_id":countryid,
	 		"action":"deleteSmaCountrySpecific"

	 	};
	 	$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			data:getCountryId,
			dataType:'json',
			success: function(response) {	
			
				GetCountries();
			},

			error: function(response){

				$('#refresh_overlay').css("display","none");
			}

	    });
     
    },

/*---------------------------------------------
    Function Name: deleteSmaCountrySpecific()
    Input Parameter:countryid
    return:json data
---------------------------------------------*/	
	updateCountryName:function(newFormData)
	{
		$('#refresh_overlay').css("display","block");
	 	newFormData.append("action","updateCountryName");
	 	$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			processData: false,
	  		contentType: false,
			data:newFormData,
			success: function(response) {	
				GetCountries();
				$('#view_rate_vehicle_modal').hide();
			},
            error: function(response){

				$('#refresh_overlay').css("display","none");
			}

	    });
     
	 },
/*---------------------------------------------
    Function Name: getSmaCountrySpecific()
    Input Parameter:getCountryID
    return:json data
---------------------------------------------*/	
	getSmaCountrySpecific:function(getCountryID){
		$('#refresh_overlay').css("display","block");
	 	var getCountryIdJson={"country_id":getCountryID,
	 							"action":"getSmaCountrySpecific"};
	 	var getResult=JSON.stringify(getCountryIdJson);

	 	$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			data:getCountryIdJson,
			dataType:'json',
			success: function(response) {	
			    $('#add_edit_country').find('button').html("Update");
			    $('#country_name').val(response.data[0]['country_name']);
			    $('#country_abbr').val(response.data[0]['country_abbr']);
			    $('#view_rate_vehicle_modal').show();
			    $('#refresh_overlay').css("display","none");

		    },
		    error: function(response){

				$('#refresh_overlay').css("display","none");
			}

	    });
	    
	 },
/*---------------------------------------------
    Function Name: getSmaCountrySpecific()
    Input Parameter:getCountryID
    return:json data
---------------------------------------------*/	
	setCountryName:function(formDataone)
	{
		$('#refresh_overlay').css("display","block");
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string")
		{
		   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
	    formDataone.append("user_id",getLocalStoragevalue[0].id);
	    formDataone.append("action","setSmaCountry");
		$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			processData: false,
	  		contentType: false,
			data:formDataone,
			success: function(response) {		
			    $('#view_rate_vehicle_modal').hide();
			    GetCountries();
        	},
        	error: function(response){

				$('#refresh_overlay').css("display","none");
			}

	    });
      $('#refresh_overlay').css("display","none");
	}
}
	

/*---------------------------------------------
    Function Name: click on edit button
    Input Parameter:getCountryID
    return:json data
---------------------------------------------*/	
    $('#add_edit_country').submit(function(event){
    	$('#refresh_overlay').css("display","block");
    	event.preventDefault(); 
    	var getFormSubmitSeq=$(this).find('button').attr("seq");
    	var geacountryAbr=$('#country_abbr').val();
    	var geacountryName=$('#country_name').val();
	 	var newFormData=new FormData();
    	    newFormData.append("country_abbr",geacountryAbr);
    	    newFormData.append("country_name",geacountryName);
    	if(getFormSubmitSeq=='default')
    	{
    		newCountryFunction.setCountryName(newFormData)
    	}
    	else
    	{
    		var getId=getFormSubmitSeq.split("_");
    		newFormData.append("country_name_id",getId[1]);
    		newCountryFunction.updateCountryName(newFormData)
    	}
     
    });