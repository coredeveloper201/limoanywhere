/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: SMA Train
    Author: Mylimoproject
---------------------------------------------*/
/*create a class name for the sma train*/
$(function () {
    $('#refresh_overlay').show();
});
var getCityInfomation={
	/*set a web service path for webservice*/
	_Serverpath:"phpfile/sma_client.php",

	/*---------------------------------------------
       Function Name: getSmaCityInformation()
       Input Parameter: smaId
       return:json data
    ---------------------------------------------*/

	getSmaCityInformation:function(smaId){
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}		
	},

	/*---------------------------------------------
       Function Name: delseSmaAirportInformation()
       Input Parameter: getRowId
       return:json data
    ---------------------------------------------*/
	delseSmaAirportInformation:function(getRowId){

		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}

		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			data: "action=delseSmaTrainInformation&rowId="+getRowId+"&type=train&user_id="+getUserId[0].id,
			success: function(response) {
                $('#refresh_overlay').hide();
				getCityInfomation.getSmaAirportInformation();
			}
		});
	},

	/*---------------------------------------------
       Function Name: showEditItemSecond()
       Input Parameter: getid
       return:json data
    ---------------------------------------------*/
	showEditItemSecond:function(getid){
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}

        $('#refresh_overlay').show();
		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			data: "action=showEditTrainItem&type=train&rowId="+getid+"&user_id="+getUserId[0].id,
			success: function(response) {
                $('#refresh_overlay').hide();
				var responseJson=response;
				if(typeof(response)=="string"){
					responseJson=JSON.parse(response);
				}
				$('#sma_in_db').val(responseJson.data[0][0].sma_id);
				$('#train_zipcode').val(responseJson.data[0][0].type_zip);
				$('#train_name').val(responseJson.data[0][0].type_name);
				$('#train_code').val(responseJson.data[0][0].save_as);
				if(typeof(responseJson.data[1])!= 'boolean'){
					$('#train_schg').val(responseJson.data[1][0].train_pickup_schg);
					$('#qr_disclaimer').val(responseJson.data[1][0].train_qr_disclaimer);
				}
				$('#refresh_overlay').css("display","none");
				$('#add_train_location').html("Update");

				$('#add_train_location').attr("seq",getid);
			}
		});

	},

	/*---------------------------------------------
       Function Name: showEditItem()
       Input Parameter: getid
       return:json data
    ---------------------------------------------*/
	showEditItem:function(getid){
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}

        $('#refresh_overlay').show();
		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			data: "action=showEditTrainItem&type=train&rowId="+getid+"&user_id="+getUserId[0].id,
			success: function(response) {
                $('#refresh_overlay').hide();
				var responseJson=response;
				if(typeof(response)=="string"){
					responseJson=JSON.parse(response);
				}
				// $('#sma_in_db').val(responseJson.data[0][0].sma_id);
				if(typeof(responseJson.data[1])!= 'boolean'){
					$('#train_schg').val(responseJson.data[1][0].train_pickup_schg);
					$('#qr_disclaimer').val(responseJson.data[1][0].train_qr_disclaimer);
				}
			}
		});

	},

	/*---------------------------------------------
       Function Name: showViewItem()
       Input Parameter: getid
       return:json data
    ---------------------------------------------*/
	showViewItem:function(getid){

		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string")
		{
			getUserId=JSON.parse(getUserId);
		}

        $('#refresh_overlay').show();
		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			data: "action=showEditTrainItem&type=train&rowId="+getid+"&user_id="+getUserId[0].id,
			success: function(response) {
                $('#refresh_overlay').hide();
				var responseJson=response;
				if(typeof(response)=="string"){
					responseJson=JSON.parse(response);
				}
				$('#sma_in_db').val(responseJson.data[0][0].sma_id);
				$('#train_zipcode').val(responseJson.data[0][0].type_zip);
				$('#train_name').val(responseJson.data[0][0].type_name);
				$('#train_code').val(responseJson.data[0][0].save_as);
				$('#train_schg').val(responseJson.data[1][0].train_pickup_schg);
				$('#qr_disclaimer').val(responseJson.data[1][0].train_qr_disclaimer);
				$('#refresh_overlay').css("display","none");
				$('#add_train_location').hide();
				$('#add_train_location').attr("seq",getid);
			}
		});
	},

	/*---------------------------------------------
       Function Name: getSmaAirportInformation()
       Input Parameter: getid
       return:json data
    ---------------------------------------------*/
	getSmaAirportInformation:function(){
		$('#refresh_overlay').css("display","block");
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}

		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			data: "action=getSmaAirportInformation&type=train&user_id="+getUserId[0].id,
			success: function(response) {
				var responseJson=response;
	  			var finalSelectBox='';
				if(typeof(response)=="string"){
					responseJson=JSON.parse(response);
				}	
				var selectBoxMatrix='<option>select</option>';

				$.each(responseJson.data,function(index,result){
					if(result.id!=null){
						finalSelectBox+='<tr><td>'+result.sma_name+'</td><td>'+result.type_name+'</td><td>'+result.save_as+'</td><td>'+result.type_zip+'</td><td><button class="btn btn-primary viewSma" seq="'+result.id+'" > View</button></td><td><a class="btn btn-xs editAirport" seq="'+result.id+'" >Edit</a><a class="btn btn-xs deleteAirport" seq="'+result.id+'" >Delete</a></td></tr>';
						selectBoxMatrix+="<option value="+result.id+">"+result.type_name+"</option>";
					}
					else{
						finalSelectBox='<tr><td colspan="4">Data not Found.</td></tr>';
						selectBoxMatrix+="<option value='noairport'>No Airport</option>";
					}								
				});

                $('#refresh_overlay').hide();
				$('#sma_train_list').html(finalSelectBox);
				$('#select_matrix_db_train').html(selectBoxMatrix);		
				$('.viewSma').on("click",function(){				
					var getseq=$(this).attr("seq");
					getCityInfomation.showViewItem(getseq);									
				})

				$('.editAirport').on("click",function(){
					$('#refresh_overlay').css("display","block");
					var getseq=$(this).attr("seq");
					getCityInfomation.showEditItem(getseq);
				})


				$('#copy_rate_matrix_train').on("click",function(){
					var getseq=$('#select_matrix_db_train').val();
					$('#train_name').removeAttr("disabled");
					$('#train_code').removeAttr("disabled");
					getCityInfomation.showEditItem(getseq);
				})

				$('.deleteAirport').on("click",function(){
					$('#refresh_overlay').css("display","block");
					var getseq=$(this).attr("seq");
					getCityInfomation.delseSmaAirportInformation(getseq);
				})
			}
		});
	},

	/*---------------------------------------------
       Function Name: getSmaInformation()
       Input Parameter: getid
       return:json data
    ---------------------------------------------*/
	getSmaInformation:function(){
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}
        var trainDB = JSON.parse(localStorage.train_db_table_id);
		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
            data: "action=getSMA&user_id="+getUserId[0].id+"&zip_code="+trainDB.train_db_zipcode,
			success: function(response){
                $('#refresh_overlay').hide();
				var responseJson=response;
	  			var finalSelectBox='';
				if(typeof(response)=="string"){
					responseJson=JSON.parse(response);
				}
                finalSelectBox='<option value="">select SMA</option>';
                var exist = 0;
                $.each(responseJson.data,function(index,result){
                    if(result.selected === 1){
                        exist = 1;
                        finalSelectBox+='<option selected="selected" value="'+result.id+'">'+result.sma_name+'</option>';
                    } else {
                        finalSelectBox+='<option value="'+result.id+'">'+result.sma_name+'</option>';
                    }
                });

				$('#sma_in_db').html(finalSelectBox);
                if(exist === 0){
                    $('#sma_in_db').parent().append('<p class="zipVerify" style="color: red;">Verify that zipcode/city exist within existing database or edit and add it or create new SMA and include zipcode</p>');
                }
				$('#sma_in_db').on("change",function(){
					var getValue=$(this).val();					
					getCityInfomation.getSmaCityInformation(getValue);
                    $('.zipVerify').remove();
				});

			}
		});
	}

}

getCityInfomation.getSmaInformation();
getCityInfomation.getSmaAirportInformation();

/*submit the form*/
$('#trainZoneForm').on("submit",function(e){
	 e.preventDefault();
	 if($('#add_train_location').html()=="Submit"){
	 	var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}

		var getForm=new FormData($('#trainZoneForm')[0]);
		var smaId=$('#sma_in_db').val();
		var airport_zipcode=$('#train_zipcode').val();
		var airport_name=$('#train_name').val();
		var airport_code=$('#train_code').val();
		var train_pickup_schg = $('#train_schg').val();
		var train_qr_disclaimer = $('#qr_disclaimer').val();

		getForm.append("action","setSmaDataTrainZone");
		getForm.append("smaId",smaId);
		getForm.append("airport_zipcode",airport_zipcode);
		getForm.append("airport_name",airport_name);
		getForm.append("airport_code",airport_code);
		getForm.append("train_pickup_schg",train_pickup_schg);
		getForm.append("train_qr_disclaimer",train_qr_disclaimer);

		var getAirportDbLocalStorage2=window.localStorage.getItem("train_db_table_id");
		if(getAirportDbLocalStorage2!="string12"){
			getAirportDbLocalStorage2=JSON.parse(getAirportDbLocalStorage2);
			getForm.append("train_db_id",getAirportDbLocalStorage2.train_db_id);
		}

		getForm.append("type","train");
		getForm.append("user_id",getUserId[0].id);

		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: getForm,
			success: function(response) {
                $('#refresh_overlay').hide();
				var responseJson=response;
				if(typeof(responseJson)=="string"){
					responseJson=JSON.parse(responseJson);
				}
				if(responseJson.code==1006){				
					if(responseJson.data=='airportCode'){
						alert("Station code should be unique.");
					} 
					else if(responseJson.data=='airportName'){
						alert("Station name should be unique.");
					}
					else if(responseJson.data=='zipCode'){
						alert("Station Zip code should be unique.");					
					}			
				}	
				else{
					alert("Suceessfully inserted");
					window.location.href="train_db.html";
				}
			}	
		});

	}
	else{
		var getId=$('#add_train_location').attr("seq");
	 	var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){
			getUserId=JSON.parse(getUserId);
		}
		var getForm=new FormData($('#trainZoneForm')[0]);
		var smaId=$('#sma_in_db').val();
		var airport_zipcode=$('#train_zipcode').val();
		var airport_name=$('#train_name').val();
		var airport_code=$('#train_code').val();
		var train_pickup_schg = $('#train_schg').val();
		var train_qr_disclaimer = $('#qr_disclaimer').val();

		getForm.append("action","updateSmaDataTrainZone");
		getForm.append("smaId",smaId);
		getForm.append("airport_zipcode",airport_zipcode);
		getForm.append("airport_name",airport_name);
		getForm.append("airport_code",airport_code);
		getForm.append("train_pickup_schg",train_pickup_schg);
		getForm.append("train_qr_disclaimer",train_qr_disclaimer);
		getForm.append("type","train");
		getForm.append("user_id",getUserId[0].id);
		getForm.append("rowId",getId);

		$.ajax({
			url: getCityInfomation._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: getForm,
			success: function(response) {
                $('#refresh_overlay').hide();
				var responseJson=response;
				if(typeof(responseJson)=="string"){
					responseJson=JSON.parse(responseJson);
				}
				if(responseJson.code==1006){
					if(responseJson.data=='airportCode'){
						alert("Station code should be unique.");
					} 
					else if(responseJson.data=='airportName'){
						alert("Station name should be unique.");
					}
					else if(responseJson.data=='zipCode'){
						alert("Station Zip code should be unique.");	
					}
				}	
				else{
					alert("Suceessfully updated.");
                    location.reload();
					//window.location.href="train_db.html";
				}	
			}	
		});
	} 	
});

/*   localstrorage pickup code start here  */		
var getAirportDbLocalStorage=window.localStorage.getItem("train_db_table_id");
		
if(typeof(getAirportDbLocalStorage)!="object"){	
	if(getAirportDbLocalStorage=="string12"){
		$('#train_zipcode').removeAttr("disabled");
		$('#train_name').removeAttr("disabled");
		$('#train_code').removeAttr("disabled");
	}
	else{
		getAirportDbLocalStorage=JSON.parse(getAirportDbLocalStorage);
		console.log(getAirportDbLocalStorage);
		if(getAirportDbLocalStorage.mainIdLocal=="NoId"){	
			$('#train_zipcode').val(getAirportDbLocalStorage.train_db_zipcode).prop("disabled");
			$('#train_name').val(getAirportDbLocalStorage.train_db_name).prop("disabled");
			$('#train_code').val(getAirportDbLocalStorage.train_db_code).prop("disabled");
		}
		else{
			setTimeout(function(){
				if(getAirportDbLocalStorage.type=="edit"){
				getCityInfomation.showEditItemSecond(getAirportDbLocalStorage.mainIdLocal);
			}
				else if(getAirportDbLocalStorage.type=="view"){				
					getCityInfomation.showViewItem(getAirportDbLocalStorage.mainIdLocal);
				}
				else{
					getCityInfomation.delseSmaAirportInformation(getAirportDbLocalStorage.mainIdLocal);	
				}
			},3000);		
		}
	}
}
