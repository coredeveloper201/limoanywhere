/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Airport
  Author: Mylimoproject
---------------------------------------------*/
/*set the web service php path for the web service call*/
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHPass="phpfile/passenger_client.php";
var ADDpassenger = 0;

/*onload finction call on page load*/
$(function(){
	/*---------------------------------------------
       Function Name: click on add button of passenger rate
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	$('#add_passenger_rate').on("click",function(){		
		new_passenger_rate();
	});	
	
	/*---------------------------------------------
       Function Name: click on copy button of ratematrix
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/	
	$('#copy_rate_matrix').on("click",function(){
	    if($('#select_matrix').val()=='')
	    {
			alert("Please Select Rate Matrix");
		}
		else
		{
			editRateMatrix($('#select_matrix').val(),0);
		}

	});
	
	/*---------------------------------------------
       Function Name: click on back button
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	$('#back_button').on("click",function(){
		location.reload(true);

	}); 

    /*---------------------------------------------
       Function Name: click on copy button
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	$("input[name='copy_existing']").click(function(){
		if($("input[name='copy_existing']:checked").val()=="yes")
		{
			$('#select_matrix_div').css("visibility","visible");
			$('#copy_rate_matrix').css("visibility","visible");
		}
		else
		{
			$('#select_matrix_div').css("visibility","hidden");
			$('#copy_rate_matrix').css("visibility","hidden");
		}
	});

    /*load all the required data on page load*/ 
	getSMA();
	getRateMatrix();
	getVehicleType();	
	getRateMatrixList();

    /*---------------------------------------------
       Function Name: click on edit button of ratematrix
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	$('.rate_matrix_onedit').on("click",function(){
		getSequence=$(this).attr("seq");
		editRateMatrix(getSequence);
	});
});

   /*---------------------------------------------
       Function Name: isUserApiKey()
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/
	function isUserApiKey()
	{	
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;	
		if(typeof(userInfo)=="string")
		{
			userInfoObj=JSON.parse(userInfo);
		}	
		$.ajax({
			url: _SERVICEPATHServer,
			type: 'POST',
			data: "action=getApiKey&user_id="+userInfoObj[0].id,
			success: function(response) {
			$('#refresh_overlay').css("display","none");
				var responseObj=response;
				if(typeof(response)=="string")
				{
				    responseObj=JSON.parse(response);
				}
				if(responseObj.code=="1017")
				{
				    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));
	  	            var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
				    if(typeof(getLocalStoragevalueUserInformation)=="string")
			        {
			   	        getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
			        }

				    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
				}
				else
				{			
					window.location.href="connectwithlimo.html";			
				}			
			}

		});

	}
    
    /*---------------------------------------------
        Function Name: new_passenger_rate()
        Input Parameter:
        return:set the passenger data
    ---------------------------------------------*/
	function new_passenger_rate(){
		ADDpassenger++;
		$("#additionl_passenger_rates").append('<div class="row" id="passenger_row_'+ADDpassenger+'"><div class="col-xs-2 col-sm-2 col-md-2" style="text-align:right;width:19%; padding-right:0px;" ><h5>Next</h5></div><div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;width:16%"><input type="text" class="form-control" placeholder="passenger(s)" id="apply_to_passengers_'+ADDpassenger+'"></div><div class="col-xs-2 col-sm-2 col-md-2" style="text-align:right;width:14%; padding-right:0px;"><h5>Per Pax Fare</h5></div><div class="col-xs-1" style="text-align:left;width:11%"><input type="text" class="form-control" id="rate_passenger_'+ADDpassenger+'" placeholder="$"></div><div class="col-xs-2" style="text-align:left;width:14%"><div class="row"><div class="col-xs-9" style="text-align:left;width:63%;margin:0px;padding:0px;" ><b></b></div><div class="col-xs-3" style="text-align:left;width:37%;margin:0px;padding:0px;"></div></div></div><div class="col-xs-3" style="text-align:right;width:23.5%;" ><b></b><button type="button" id="edit_remaining_rate" class="btn btn-primary" style="text-align:right;" onclick="del_passenger('+ADDpassenger+')">Delete</button></div><br><br><br></div>');
				
	}

    /*---------------------------------------------
        Function Name: getSMA()
        Input Parameter:user_id
        return:set the passenger data
    ---------------------------------------------*/ 
	function getSMA()
    {
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
	    {
		  userInfoObj=JSON.parse(userInfo);
	    }

		$.ajax({
		   url: _SERVICEPATHSma,
 		   type: 'POST',
		   data: "action=getSMA&user_id="+userInfoObj[0].id,
		   success: function(response) {
		        var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
				   responseObj=JSON.parse(response);
				}	
				$('#refresh_overlay').css("display","none");
				var responseHTML='';
				for(var i=0; i<responseObj.data.length; i++)
				{	
                    if(responseObj.data[i].id!=null){
					    responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
                    }
				}
				$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
				    $("#apply_sma").multiselect('destroy');
 					$('#apply_sma').multiselect({
					   maxHeight: 200,
					   buttonWidth: '155px',
					   includeSelectAllOption: true
					});

				},400);
		    },
		    error:function(){
		       alert("Some Error");
		    }

	    });

    }

	/*---------------------------------------------
        Function Name: del_passenger()
        Input Parameter:passenger_row_id
        return:remove from the passenger
    ---------------------------------------------*/
	function del_passenger(passenger_row_id){

		$('#passenger_row_'+passenger_row_id).remove();

    }
	/*---------------------------------------------
        Function Name: getRateMatrix()
        Input Parameter:user_id,
        return:json data
    ---------------------------------------------*/
	function getRateMatrix()
	{
	    var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
	    {
		  userInfoObj=JSON.parse(userInfo);
	    }
		$.ajax({
		    url: _SERVICEPATHPass,
		    type: 'POST',
		    data: "action=getRateMatrix&user_id="+userInfoObj[0].id,
		    success: function(response) {
		        var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string")
				{
				   responseObj=JSON.parse(response);
				}	
				$('#refresh_overlay').css("display","none");
				var responseHTML='<option value="">--Select--</option>';
				for(var i=0; i<responseObj.data.length; i++)
				{	
					if(responseObj.data[i].name!=null)
					{
						responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].name+'</option>';		
					}
					
     			}
			    $('#select_matrix').html(responseHTML);
	
		    },
		    error:function(){
			   alert("Some Error");
		    }
		});

	}

    /*---------------------------------------------
        Function Name: getVehicleType()
        Input Parameter:user_id,
        return:json data
    ---------------------------------------------*/
	function getVehicleType()
	{
	    var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	    if(typeof(getLocalStoragevalue)=="string")
      	{
	   	    getLocalStoragevalue=JSON.parse(getLocalStoragevalue);

	    }
	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

	    $.ajax({
		    url: _SERVICEPATH,
		    type: 'POST',
		    data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
 		    success: function(response) {
			    var responseHTML='';
			    var responseObj=response;
			    if(typeof(response)=="string")
       			{
				    responseObj=JSON.parse(response);					
			    }
			    responseHTML='';
			    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
			    {
			        responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
			    }
				$('#refresh_overlay').css("display","none");
			    $('#apply_vehicle_type').html(responseHTML);
			    setTimeout(function(){
				    $("#apply_vehicle_type").multiselect('destroy');
            	    $('#apply_vehicle_type').multiselect({
                        maxHeight: 200,
                        buttonWidth: '155px',
                        includeSelectAllOption: true
                    });
				},400);

		    }

	    });
	}

    /*---------------------------------------------
        Function Name: click on save button
        Input Parameter:user_id
        return:insert the form value
    ---------------------------------------------*/
	$('#save_perpass_rate').on("click",function(){
		
		$('#refresh_overlay').css("display","block");
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
	    if(typeof(userInfo)=="string")
	    {
		   userInfoObj=JSON.parse(userInfo);
	    }
		var addPassengers=[];
		var addPassengersRate=[];
		var baseRate=[];	
		var passenger1=$('#apply_to_passenger_0').val();
		addPassengers .push(passenger1);
		addPassengersRate.push($('#min_base_rate').val());
		var matrix_name=$('#passenger_matrix_name').val();
		matrix_name=matrix_name.trim();
		var selectedVehicle = $('#apply_vehicle_type option:selected');
        var selectedVehicleObject = [];
        /*insert the applied vehicle type*/
        $(selectedVehicle).each(function(index, selectedVehicle){
            selectedVehicleObject .push($(this).val());
        });

        var currencyType=$('#currencyType').val();
        if(currencyType=="%")
        {
            currencyType="per";
        }
        else
        {
        	currencyType="dollar";
        }
        var pick_zone_name=$('#pick_zone_name').val();
        var drop_zone_name=$('#drop_zone_name').val();
		var selectedSma = $('#apply_sma option:selected');
        var selectedSmaObject = [];
        /*insert the selected SMA*/
        $(selectedSma).each(function(index, selectedSma){
            selectedSmaObject .push($(this).val());
        });
         
		var percent_increase= $('#percent_increase').val();
		var k=1;
		for(k=1;k<=ADDpassenger;k++)
		{
			if($('#apply_to_passengers_'+k).val()!='' && $('#apply_to_passengers_'+k).val()!=undefined && $('#rate_passenger_'+k).val()!='' && $('#rate_passenger_'+k).val()!=undefined )
			{
				addPassengers .push($('#apply_to_passengers_'+k).val());
				addPassengersRate.push($('#rate_passenger_'+k).val());
				if($('#base_rate_'+k).val()!=null && $('#base_rate_'+k).val()!='' && $('#base_rate_'+k).val()!=undefined)
				{
					baseRate.push($('#base_rate_'+k).val());
				}
				else
				{
					baseRate.push(0);
				}
			}
		}
		var remaining_rate=$('#remaining_rate').val();
		addPassengers .push(1000000);
		addPassengersRate.push(remaining_rate);
		if($('#remaining_fixed_amnt').val()!=null && $('#remaining_fixed_amnt').val()!='' && $('#remaining_fixed_amnt').val()!=undefined)
			{
			    baseRate.push($('#remaining_fixed_amnt').val());
		    }
		    else
			{
				baseRate.push(0);
			}
		var getUserInfo=window.localStorage.getItem("companyInfo");
		var getUserInfoObj=getUserInfo;
		if($('#min_base_rate').val()=='')
		{
			alert("Please Enter Minimum Base Rate");
			$('#refresh_overlay').css("display","none");
		}
		else if($('#apply_to_passenger_0').val()=='')
		{
			alert("Please Enter Apply To First Passenger(s)");
			$('#refresh_overlay').css("display","none");
		}
		else if($('#remaining_rate').val()=='')
		{
			alert("Please Enter Remaining Passengers Rate");
			$('#refresh_overlay').css("display","none");
		}
		else if(matrix_name=='')
		{
			alert("Please Enter Rate Matrix Name");
			$('#refresh_overlay').css("display","none");
		}
		else if(selectedVehicleObject=='')
		{
			alert("Please Select At Least One Vehicle Code");
			$('#refresh_overlay').css("display","none");
		}
		else if(selectedSmaObject=='')
		{
			alert("Please Select At Least One SMA ");
			$('#refresh_overlay').css("display","none");
		}
		else
		{

			if($('#save_perpass_rate').html()=='Update')
			{
            	var rate_matrix_id=$('#save_perpass_rate').attr('seq')
				$.ajax({
					url: _SERVICEPATHPass,
					type: 'POST',
				    data: "action=deletePassengerRate&rate_matrix_id="+rate_matrix_id,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
						$.ajax({
		                    url:_SERVICEPATHPass,
                		    type: 'POST',
		                    data: "action=checkUniqueMatrix&matrix_name="+matrix_name+"&user_id="+userInfoObj[0].id,
		                    success: function(response) {
			                    var responseObj=response;
								if(typeof(response)=="string")
								{
									responseObj=JSON.parse(response);
								}
							    if(responseObj.data[0].id!="null" && responseObj.data[0].id!=null && responseObj.data[0].id!=undefined)
							    {
								    alert("Matrix Name already exists");
							    }
							    else
							    {

								    $('#save_perpass_rate').html('Processing...');
								    $('#refresh_overlay').css("display","none");
							        $('#save_perpass_rate').attr("disable","true");
							        var pickHrsDatabase=$('#pickHrsDatabase').val();
								    $.ajax({
										url: _SERVICEPATHPass,
										type: 'POST',
										data: "action=setPassengerRate&user_id="+userInfoObj[0].id+"&matrix_name="+matrix_name+"&vehicle_code="+selectedVehicleObject+"&add_passengers="+addPassengers+"&add_passengers_rate="+addPassengersRate+"&sma_id="+selectedSmaObject+"&percent_increase="+percent_increase+"&pickHrsDatabase="+pickHrsDatabase+"&currencyType="+currencyType+"&pick_zone_name="+pick_zone_name+"&drop_zone_name="+drop_zone_name,
										success: function(response) {
											alert("Rate Matrix Updated Successfully");
											location.reload(true);
										}
									});
							    }
  
						    }

						});

					}

			    });
			}
			else
			{
				$.ajax({
		            url:_SERVICEPATHPass,
		            type: 'POST',
		            data: "action=checkUniqueMatrix&matrix_name="+matrix_name+"&user_id="+userInfoObj[0].id,
		            success: function(response) {
			            var responseObj=response;
				        if(typeof(response)=="string")
			         	{
					        responseObj=JSON.parse(response);
				        }
			           if(responseObj.data[0].id!="null" && responseObj.data[0].id!=null && responseObj.data[0].id!=undefined)
			           {
				            alert("Matrix Name already exists");
			           }
			           else
			           {
				            $('#save_perpass_rate').html('Processing...');
			                $('#save_perpass_rate').attr("disable","true");
			                var pickHrsDatabase=$('#pickHrsDatabase').val();
					        $.ajax({
					            url: _SERVICEPATHPass,
					            type: 'POST',
				                data: "action=setPassengerRate&user_id="+userInfoObj[0].id+"&matrix_name="+matrix_name+"&vehicle_code="+selectedVehicleObject+"&add_passengers="+addPassengers+"&add_passengers_rate="+addPassengersRate+"&sma_id="+selectedSmaObject+"&percent_increase="+percent_increase+"&pickHrsDatabase="+pickHrsDatabase+"&currencyType="+currencyType+"&pick_zone_name="+pick_zone_name+"&drop_zone_name="+drop_zone_name,
					            success: function(response) {
					                alert("Rate Matrix Added Successfully");
					                location.reload(true);
						        }
					        });
		                }
		            }
		        });
			}
	
		}

	});

	/*---------------------------------------------
        Function Name: getRateMatrixList()
        Input Parameter:user_id
        return:json data
     ---------------------------------------------*/

	function getRateMatrixList()
	{
	    $("#refresh_overlay").css('display','block');
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string")
		{
		    userInfoObj=JSON.parse(userInfo);
		}
			$.ajax({
				url:_SERVICEPATHPass,
				type:'POST',
				data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
				success:function(response)
				{
					var responseObj=response;
				    var responseHTML='';
			       	var responseVeh='';
					var responseSma='';
					var vehicle_code='';
					var sma_name='';
					var vehicle_code_array='';
					var sma_name_array='';
					var sma_id='';
					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);
				    }
					$("#refresh_overlay").css('display','none');
					if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
					 responseObj.data!="")
					{
					    for(var i=0; i<responseObj.data.length; i++)
						{
							 responseVeh=0;
						 	 responseSma=0;
							 vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
							 vehicle_code_array=vehicle_code.split(',');
							 sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
							 sma_name_array=sma_name.split(',');
							 var s=0;
							 for(s=0;s<vehicle_code_array.length;s++)
							 {
								responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
							 }
							 var t=0;
	 						 for(t=0;t<sma_name_array.length;t++)
	 						 {
								responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
							 }
							 sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
						 	 responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="rate_matrix_'+responseObj.data[i].id+'">'+responseObj.data[i].matrix_name+'</td><td style="text-align:center" id="min_fare_'+responseObj.data[i].id+'">'+responseObj.data[i].min_fare+'</td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" seq='+responseObj.data[i]['id']+' id=edit_rate_'+responseObj.data[i]['id']+' currencyType="'+responseObj.data[i].currency_type+'" pickupLocation="'+responseObj.data[i].pickup_location+'" dropoffLocation="'+responseObj.data[i].drop_off_location+'" inc_percent="'+responseObj.data[i].miles_increase_percent+'" onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit<input type="hidden" id="hiddenPeakHour_'+responseObj.data[i]['id']+'" value='+responseObj.data[i].peak_hour_id+'></a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteRateMatrix('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';
						} 

			        }else{

					 	responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
					}
					$('#rate_matrix_list').html(responseHTML);
					$("#refresh_overlay").css('display','none');
					setTimeout(function(){
					    $(".refresh_multi_select").multiselect('destroy');
						$('.refresh_multi_select').multiselect({
						    maxHeight: 200,
						    buttonWidth: '155px',
						    includeSelectAllOption: true
						});

					},800);
				},
				error:function(){
					alert("Some Error");					
				}
		    });

    }


    /*---------------------------------------------
        Function Name: getCountryAirportDb()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/
	function editRateMatrix(getSequence,status)
	{
		$('#refresh_overlay').css("display","block");
		var currencytype=$('#edit_rate_'+getSequence).attr("currencytype");
		if(currencytype=="dollar")
		{
			currencytype="$";
		}
		else
		{
			currencytype="%";
		}
		$('#currencyType').val(currencytype);
		var pickuplocation=$('#edit_rate_'+getSequence).attr("pickuplocation");
		$('#pick_zone_name').val(pickuplocation);
		var dropofflocation=$('#edit_rate_'+getSequence).attr("dropofflocation");
		$('#drop_zone_name').val(dropofflocation);
		var inc_percent=$('#edit_rate_'+getSequence).attr("inc_percent");
		$("#percent_increase").val(inc_percent);
		$('.isInputBoxFill').css("visibility","visible");
		$('#pickHrsDatabase').val($('#hiddenPeakHour_'+getSequence).val());
		var rate_matrix_name_edit=$('#rate_matrix_'+getSequence).html();
		var min_base_rate_edit=$('#min_fare_'+getSequence).html();
		var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");;
			rate_matrix_veh=rate_matrix_veh.split(',');
		$("#apply_vehicle_type").val(rate_matrix_veh);
		$("#apply_vehicle_type").multiselect('refresh');
		var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
			rate_matrix_sma=rate_matrix_sma.split(',');
		$("#apply_sma").val(rate_matrix_sma);
		$("#apply_sma").multiselect('refresh');
		if(status==1)
		{
			$("#passenger_matrix_name").val(rate_matrix_name_edit);
		}
		$("#min_base_rate").val(min_base_rate_edit);
		$.ajax({
		    url:_SERVICEPATHPass,
		    type:'POST',
		    data:"action=editRateMatrix&rate_matrix_id="+getSequence,
		    success:function(response)
		    {
			    var responseObj=response;
				var responseHTML='';
				var vehicle_code='';
				var sma_name='';
				var sma_id='';
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
					 responseObj.data!="")
				{
				    while(ADDpassenger>0)
					{
						del_passenger(ADDpassenger);
						ADDpassenger--;
					}

					if(status==1)
					{
					  	$('#save_perpass_rate').html("Update");
						$('#save_perpass_rate').attr("seq",getSequence);
						$('#back_button').css("display","inline-block");
					}	
					var q=0;
				    for( q=0; q<responseObj.data.length; q++)
					{	
					    if(q==0)
					    {
						    $('#apply_to_passenger_0').val(responseObj.data[q]['passengers']);
						}
						else if(q==(responseObj.data.length-1))
						{
						    $('#remaining_rate').val(responseObj.data[q]['rate']);
						}
						else
						{
							new_passenger_rate();
							$('#apply_to_passengers_'+q).val(responseObj.data[q]['passengers']);
							$('#rate_passenger_'+q).val(responseObj.data[q]['rate']);
						}
						setTimeout(function(){
							$("html, body").animate({ scrollTop: 0 }, "slow");	
							$('#refresh_overlay').css("display","none");
						},200);
					}

			    }
		    },
		    error:function()
		    {
			    alert("Some Error.");
			    $('#refresh_overlay').css("display","none");
		    }
		});
    }

    /*call the get location function on page load*/
      getLocationName();
     /*---------------------------------------------
        Function Name: getCountryAirportDb()
        Input Parameter:user_id
        return:json data
      ---------------------------------------------*/
		function getLocationName()
		{
			var getUserId=window.localStorage.getItem('companyInfo');
			if(typeof(getUserId)=="string")
			{
				getUserId=JSON.parse(getUserId);
			}
			var availableTags = [];
	  		$.ajax({
			    url:"phpfile/rate_point_client.php",
			    type: 'POST',
			    data: "action=getPointToPointZone&user_id="+getUserId[0].id,
 			    success: function(response) {
			 	    var responseOBJ='';
	     			if(typeof(response)=="string")
					{
						responseOBJ=JSON.parse(response);
	     			}
					var getHtml='<option value="">Select Zone</option>';
					if(responseOBJ.code==1007)
					{
						$.each(responseOBJ.data,function(index,result){
					   	    availableTags.push(result.type_name)
						    getHtml+='<option value="'+result.type_name+'">'+result.type_name+'</option>';
						});
						$('#drop_zone_name').html(getHtml);
						$('#pick_zone_name').html(getHtml);
								    
					}
			    }

		    });
		}

       
       /*call the getcomparehourRate on page load*/ 
        getCommanpeakHourRate();
        
      /*---------------------------------------------
        Function Name: getCommanpeakHourRate()
        Input Parameter:user_id
        return:json data
       ---------------------------------------------*/
		function getCommanpeakHourRate()
		{	
			var _Serverpath="phpfile/rate_point_client.php";
		    var getCompanyInfo=localStorage.getItem("companyInfo");
			if(typeof(getCompanyInfo)=="string")
			{
				getCompanyInfo=JSON.parse(getCompanyInfo);
			}
		    var getJson={"action":"getCommanpeakHourRate","user_id":getCompanyInfo[0].id};

			$.ajax({
				url:_Serverpath,
				type: 'POST',
				dataType:'json',
				data: getJson,
				success: function(response) {
					var responseHTML='<option value="">Select</option>';
					if(response.code==1007)
					{
						$.each(response.data,function(index,result){
							responseHTML+='<option value='+result.id+'>'+result.peak_hour_name+'</option>';	
						});
					}
					else
					{
						responseHTML+='<option value="">Data Not Found</option>';	
					}
					$('#pickHrsDatabase').html(responseHTML);
				}

			});
		}

    /*---------------------------------------------
        Function Name: getCountryAirportDb()
        Input Parameter:user_id
        return:json data
    ---------------------------------------------*/
	function deleteRateMatrix(getSequence){

		var res = confirm("Do You Really Want To Delete this Matrix?");
	    if (res == true) {
			$('#refresh_overlay').css("display","block");
		    $.ajax({
				url: _SERVICEPATHPass,
				type: 'POST',
			    data: "action=deletePassengerRate&rate_matrix_id="+getSequence,
				success: function(response) {
				    $('#refresh_overlay').css("display","none");
				    getRateMatrixList();
					getRateMatrix();
				},
				error:function(){
					alert("Some Error");
					$('#refresh_overlay').css("display","none");
				}

			});
		}else
		{

		}
    }

    /*---------------------------------------------
        Function Name: checknegative()
        Input Parameter:increase value
        return:result of value is negative or not
     ---------------------------------------------*/

	function checknegative() {

        if ($('#percent_increase').val() < 0) {
	        $('#percent_increase').val(0) ;
	        $('#percent_increase').focus();
	        alert('Negative Values Not allowed');
	        return false;
	    }

	 }