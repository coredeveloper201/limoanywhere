/*---------------------------------------------
Template Name: Mylimoproject
Page Name: seaport db
Author: Mylimoproject
---------------------------------------------*/

/*click on the import on change function*/
$('#importSeaport').on("change",function(){
  airportDbClass.setImportDatabaseFIleSeaport();
});

/*add seaport manual function start*/
$('#add_seaport_manual').on("submit",function(event1){
 event1.preventDefault();
  airportDbClass.setManualSeaport();
});

/*airport db class */
var airportDbClass={
	_Serverpath:"phpfile/airport_db_client.php",

  /*---------------------------------------------
    Function Name:  delseSmaAirportInformation()
    Input Parameter: rowId,rowId2
    return:json data
  ---------------------------------------------*/
  delseSmaAirportInformation:function(getRowId,rowId2){
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
      getUserId=JSON.parse(getUserId);
    }

    $.ajax({
      url: "phpfile/sma_client.php",
      type: 'POST',
      data: "action=delseSmaSeaportInformation&rowId="+getRowId+"&type=seaport&user_id="+getUserId[0].id+"&rowId2="+rowId2,
      success: function(response) {
        window.location.href="";
      }
    });
  },

  /*---------------------------------------------
    Function Name:  airport_activeFormSubmit()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  airport_activeFormSubmit:function(rowId){
    $('#refresh_overlay').css("display","block");
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
       getUserId=JSON.parse(getUserId);
    } 
    $.ajax({
      type: "POST",
      url:airportDbClass._Serverpath,
      data: "action=seaport_activeFormSubmit&user_id="+getUserId[0].id+"&aiportId="+rowId,
       }).done(function(resultData){
            $('#refresh_overlay').css("display","none");
            location.reload();    
        });
  },

  /*---------------------------------------------
    Function Name:  getImportDatabaseFIleSeaPort()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
	getImportDatabaseFIleSeaPort:function(){		
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
      getUserId=JSON.parse(getUserId);
    }
		
    $.ajax({
      type: "POST",
      url:airportDbClass._Serverpath,
      data: "action=getImportDatabaseFIleSeaPort&user_id="+getUserId[0].id,
      success: function(result){
      	var getJson=result;
      	if(typeof(result)=="string"){
      		getJson=JSON.parse(result);
      	}

        if(getJson.code!=1006){
        	var resultHTML='';
        	for(var i=0; i<getJson.data.length; i++){
        		if(getJson.data[i].status==1){
        			resultHTML+='<tr><td>'+(i+1)+'</td><td class="seaport_db_code_'+getJson.data[i].id+'">'+getJson.data[i].seaport_code+'</td><td class="seaport_db_name_'+getJson.data[i].id+'">'+getJson.data[i].seaport_name+'</td><td class="seaport_db_zipcode_'+getJson.data[i].id+'">'+getJson.data[i].seaport_zip_code+'</td><td><button class="btn btn-warning not_view_btn">View</button></td><td><a class="btn btn-xs editSeaportDb" seq="'+getJson.data[i].id+'" mainId="NoId">Edit</a><a class="btn btn-xs deactivateSeaportDb" seq="'+getJson.data[i].id+'">Deactivate</a></td></tr>';	
        		}
      		  else{
      			 resultHTML+='<tr><td>'+(i+1)+'</td><td class="seaport_db_code_'+getJson.data[i].id+'">'+getJson.data[i].seaport_code+'</td><td class="seaport_db_name_'+getJson.data[i].id+'">'+getJson.data[i].seaport_name+'</td><td class="seaport_db_zipcode_'+getJson.data[i].id+'">'+getJson.data[i].seaport_zip_code+'</td><td><button class="btn btn-primary viewSeaportDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].seaport_id+'">View</td></td><td><a class="btn btn-xs editSeaportDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].seaport_id+'">Edit</a><a class="btn btn-xs deactivateSeaportDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].seaport_id+'">Deactivate</a></td></tr>';	
      		  }
      		}
        }
        else{
          resultHTML+='<tr><td colspan="6">Data Not Found</td></tr>';  
        }

      	$('#seaport_db_table').html(resultHTML);
        $('.not_view_btn').on("click",function(){
          alert("Seaport you are trying to view is not activated yet. Please click on Edit button and fill details to activate seaport");
        });

      	$('.deactivateSeaportDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var confirmValue=confirm("Warning!  Are you sure, you wants to deactivate zone seaport. Deactivating will erase all saved data for zone seaport from system.");
      		if(confirmValue){
             airportDbClass.delseSmaAirportInformation(mainIdLocal,getSeq);
          }
        })

      	$('.viewSeaportDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var seaport_db_code=$('.seaport_db_code_'+getSeq).html().trim();
      		var seaport_db_name=$('.seaport_db_name_'+getSeq).html().trim();
      		var seaport_db_zipcode=$('.seaport_db_zipcode_'+getSeq).html().trim();	
      		var getJson={"airport_db_id":getSeq,"mainIdLocal":mainIdLocal,"seaport_db_code":seaport_db_code,"seaport_db_name":seaport_db_name,"seaport_db_zipcode":seaport_db_zipcode,"type":"view"};
  			  window.localStorage.setItem("seaport_db_table_id",JSON.stringify(getJson));    		
  				window.location.href='sma_seaport.html';
  			})
      	$('.editSeaportDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var seaport_db_code=$('.seaport_db_code_'+getSeq).html().trim();
      		var seaport_db_name=$('.seaport_db_name_'+getSeq).html().trim();
      		var seaport_db_zipcode=$('.seaport_db_zipcode_'+getSeq).html().trim();	
      		var getJson={"airport_db_id":getSeq,"mainIdLocal":mainIdLocal,"seaport_db_code":seaport_db_code,"seaport_db_name":seaport_db_name,"seaport_db_zipcode":seaport_db_zipcode,"type":"edit"};
  			  window.localStorage.setItem("seaport_db_table_id",JSON.stringify(getJson));   		
  			  window.location.href='sma_seaport.html';
        })
      }
	 });
},

  /*---------------------------------------------
    Function Name:  getSeaportCityAndCode()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getSeaportCityAndCode:function(rowId){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);
    $.ajax({
      url:this._Serverpath,
      type: 'POST',
      data: "action=getSeaportCityAndCode&user_id="+getUserId[0].id+"&stateName="+rowId,
      }).done(function(response){
          var responseJSON=response;
          if(typeof(response)=="string"){
            responseJSON=JSON.parse(response);
          }                
          var responseHTML='';

          if(responseJSON.code==1007)
          {
            $.each(responseJSON.data,function(index,value){
              if(value.city_code!='')
              {
                responseHTML+='<option value="'+value.city_name+"@"+(value.city_code)+'">'+value.city_name+"-"+(value.city_code)+'</option>';
              }
              else
              {
                responseHTML+='<option value="'+value.city_name+'">'+value.city_name+'</option>';
              }
            });
          }
          else
          {
              responseHTML+='<option>No city </option>';
          }
          $('#seaport_db_city').html(responseHTML);
          $("#seaport_db_city").val('');     
          $("#seaport_db_city").multiselect('destroy');
          $('#seaport_db_city').multiselect({
            maxHeight: 184,
            buttonWidth: '155px',
            includeSelectAllOption: true,
            id:'seaport_db_city'
          });
          $('#seaport_db_city_check').on("click",function(){
          var airport_db_cityValue=$('#seaport_db_city').val(); 
          $.ajax({
            url:airportDbClass._Serverpath,
            type: 'POST',
            data: "action=isSeaportCheck&user_id="+getUserId[0].id+"&airport_db_cityValue="+airport_db_cityValue,
          }).done(function(response){
              var responseJSON=response;
          if(typeof(response)=="string"){
            responseJSON=JSON.parse(response);
          }
          var responseHTML='';
          if(responseJSON.code==1007){
            $.each(responseJSON.data,function(index,value){
              if(value.airport_code!=''){
               responseHTML+='<option value='+value.id+'>'+value.seaport_name+"-("+value.seaport_code+")"+'</option>';
              }
              else{
               responseHTML+='<option value='+value.id+'>'+value.seaport_name+'</option>';
              }
            });
            $('#seaport_db_city_check img').attr("src","images/green_check.png");
            $('#seaport_db_seaport_code').html(responseHTML);
            $("#seaport_db_seaport_code").val('');     
            $("#seaport_db_seaport_code").multiselect('destroy');
            $('#seaport_db_seaport_code').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'seaport_db_city'
            });
          }

          else{
            alert("Seaport Not Found");
            $('#seaport_db_city_check img').attr("src","images/ok_checkmark_red_T.png");
            $('#seaport_db_seaport_code').html(" ");
            $("#seaport_db_seaport_code").val('');     
            $("#seaport_db_seaport_code").multiselect('destroy');
            $('#seaport_db_seaport_code').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'seaport_db_city'
            });
          }
        });
      });
    
    }).fail(function(){
        alert("Internet Problem")
      })
  },

  /*---------------------------------------------
    Function Name: getCountryAirportDb()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getCountryAirportDb:function(){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);
    $.ajax({
      url:this._Serverpath,
      type: 'POST',
      data: "action=getCountrySeaportDb&user_id="+getUserId[0].id,
      }).done(function(response){
          var responseJson=response;
          if(typeof(response)=="string"){
            responseJson=JSON.parse(response);
          }
          var responseHTML='';
          responseHTML+='<option value="noState">Select Country</option>';
          if(responseJson.code==1007){
            $.each(responseJson.data,function(index,value){
           
              responseHTML+='<option value="'+value.seaport_country+'">'+value.seaport_country+'</option>';
            });
          }
          else{
              responseHTML+='<option> No country</option>';
          }
          $('#seaport_db_country').html(responseHTML);
          $('#seaport_db_country').on("change",function(){
            var getValue=$(this).val();
            $('#seaport_category').val('select');
            $('#seaport_db_seaport_code').html(' ');
            $("#seaport_db_seaport_code").multiselect('destroy');
            $('#seaport_db_seaport_code').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'seaport_db_seaport_code'
            });
            $('#seaport_db_city').html(' ');
            $("#seaport_db_city").multiselect('destroy');
            $('#seaport_db_city').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'seaport_db_city'
            });

           var getValue=$(this).val();
            airportDbClass.getStateAirportDb(getValue);
          });

        }).fail(function(){
            alert("Network Problem");
          });
  },


  /*---------------------------------------------
    Function Name:  getStateAirportDb()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getStateAirportDb:function(rowId){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);
      $.ajax({
      url:this._Serverpath,
      type: 'POST',
      data: "action=getStateSeaportDb&user_id="+getUserId[0].id+"&rowId="+rowId,
      }).done(function(response){
          var responseJson=response;
          if(typeof(response)=="string"){
            responseJson=JSON.parse(response);
          }
          var responseHTML='';
          responseHTML+='<option value="noState">Select State</option>';
          if(responseJson.code==1007){
            $.each(responseJson.data,function(index,value){
              responseHTML+='<option value="'+value.state_name+'">'+value.state_name+'</option>';
            });
          }
          else{
              responseHTML+='<option> No State</option>';
          }
          $('#seaport_db_state').html(responseHTML);
          $('#seaport_db_state').on("change",function(){
            var getValue=$(this).val();
            airportDbClass.getSeaportCityAndCode(getValue);
            $('#seaport_db_city').html(' ');
            $("#seaport_db_city").multiselect('destroy');
            $('#seaport_db_city').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'seaport_db_city'
            });
          });
        }).fail(function(){
            alert("Network Problem");
          });
  },


  /*---------------------------------------------
    Function Name:  setImportDatabaseFIleSeaport()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
	setImportDatabaseFIleSeaport:function(){
  	$("#refresh_overlay").css('display','block');
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
        getUserId=JSON.parse(getUserId);
      }
    
    var newformdata=new FormData($('#getformdata')[0]);
  	newformdata.append("action","setImportDatabaseFIleSeaport");  
    newformdata.append("user_id",getUserId[0].id);
    $.ajax({
      type: "POST",
      url:this._Serverpath,
      data:newformdata,
      enctype: 'multipart/form-data',
      processData: false,  
      contentType: false ,
      dataType:'json',
      success: function(response) {
        $("#refresh_overlay").css('display','none');
        alert("Data Imported successfully");
        location.reload();
      }
  	});
},

  /*---------------------------------------------
    Function Name:  setManualSeaport()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  setManualSeaport:function(){
    $("#refresh_overlay").css('display','block');
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
      getUserId=JSON.parse(getUserId);
    }
    
    var newformdata=new FormData($('#add_seaport_manual')[0]);
    newformdata.append("action","setManualSeaport");  
    newformdata.append("user_id",getUserId[0].id);
    $.ajax({
      type: "POST",
      url:this._Serverpath,
      data:newformdata,
      processData: false,  
      contentType: false ,
      dataType:'json',
      success: function(response){
        $("#refresh_overlay").css('display','none');
        alert("Data Inserted successfully");
        location.reload();
      }
    });
  }
}

// airportDbClass.getStateAirportDb();
airportDbClass.getImportDatabaseFIleSeaPort();
airportDbClass.getCountryAirportDb();
$('#seaportDbFormSubmit').on("submit",function(event1){
  event1.preventDefault();
  var get_airport_db_airport_code_value=$('#seaport_db_seaport_code').val();
  airportDbClass.airport_activeFormSubmit(get_airport_db_airport_code_value);
});


