/*---------------------------------------------
Template Name: Mylimoproject
Page Name: train db
Author: Mylimoproject
---------------------------------------------*/

/*click on the import on change function*/
$('#importTrain').on("change",function(){
  airportDbClass.setImportDatabaseFIleSeaport();
});

/*add train manual function start*/
$('#add_train_manual').on("submit",function(event1){
  event1.preventDefault();   
  airportDbClass.setManualTrain();
});

/*airport db class */
var airportDbClass={
	_Serverpath:"phpfile/airport_db_client.php",

  /*---------------------------------------------
    Function Name:  delseSmaAirportInformation()
    Input Parameter: rowId,rowId2
    return:json data
  ---------------------------------------------*/
  delseSmaAirportInformation:function(getRowId,rowId2){
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
      getUserId=JSON.parse(getUserId);
    }

    $.ajax({
      url: "phpfile/sma_client.php",
      type: 'POST',
      data: "action=delseSmaTrainInformation&rowId="+getRowId+"&type=train&user_id="+getUserId[0].id+"&rowId2="+rowId2,
      success: function(response){
          window.location.href="";
      }
    });
  },

  /*---------------------------------------------
    Function Name:  getAiportCityAndCode()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getAiportCityAndCode:function(rowId){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);
    $.ajax({
      url:this._Serverpath,
      type: 'POST',
      data:"action=getTrainCityAndCode&user_id="+getUserId[0].id+"&stateName="+rowId,
      }).done(function(response){
        var responseJSON=response;
        if(typeof(response)=="string"){
          responseJSON=JSON.parse(response);
        }      
        var responseHTML='';
        if(responseJSON.code==1007){
          $.each(responseJSON.data,function(index,value){
            if(value.city_code!=''){
              responseHTML+='<option value="'+value.city_name+"@"+(value.city_code)+'">'+value.city_name+"-"+(value.city_code)+'</option>';
            }
            else{
              responseHTML+='<option value="'+value.city_name+'">'+value.city_name+'</option>';
            }
          });
        }
        else{
          responseHTML+='<option>No city </option>';
        }
        $('#train_db_city').html(responseHTML);
        $("#train_db_city").val('');     
        $("#train_db_city").multiselect('destroy');
        $('#train_db_city').multiselect({
          maxHeight: 184,
          buttonWidth: '155px',
          includeSelectAllOption: true,
          id:'train_db_city'
        });

        $('#train_db_city_check').on("click",function(){
        var airport_db_cityValue=$('#train_db_city').val(); 
        
        $.ajax({
          url:airportDbClass._Serverpath,
          type: 'POST',
          data: "action=isTrainCheck&user_id="+getUserId[0].id+"&airport_db_cityValue="+airport_db_cityValue,
          }).done(function(response){
              var responseJSON=response;
              if(typeof(response)=="string"){
                responseJSON=JSON.parse(response);
          }
            var responseHTML='';
            if(responseJSON.code==1007){
              $.each(responseJSON.data,function(index,value){
              if(value.airport_code!=''){
                responseHTML+='<option value='+value.id+'>'+value.train_name+"-("+value.train_code+")"+'</option>';
              }
              else{
                 responseHTML+='<option value='+value.id+'>'+value.train_name+'</option>';
              }
            });

            $('#train_db_city_check img').attr("src","images/green_check.png");
            $('#train_db_train_code').html(responseHTML);
            $("#train_db_train_code").val('');     
            $("#train_db_train_code").multiselect('destroy');
            $('#train_db_train_code').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'train_db_city'
            });
          }

          else{
            alert("Train Not Found");
            $('#train_db_city_check img').attr("src","images/ok_checkmark_red_T.png");
            $('#train_db_train_code').html(responseHTML);
            $("#train_db_train_code").val('');     
            $("#train_db_train_code").multiselect('destroy');
            $('#train_db_train_code').multiselect({
              maxHeight: 184,
              buttonWidth: '155px',
              includeSelectAllOption: true,
              id:'train_db_city'
            });
          }
        });

      });

    });
  },


  /*---------------------------------------------
    Function Name:  getCountryAirportDb()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getCountryAirportDb:function(){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);

    $.ajax({
    url:this._Serverpath,
    type: 'POST',
    data: "action=getCountryTrainDb&user_id="+getUserId[0].id,
    }).done(function(response){
        var responseJson=response;
        if(typeof(response)=="string"){
          responseJson=JSON.parse(response);
        }
        var responseHTML='';
        responseHTML+='<option value="noState">Select Country</option>';
        if(responseJson.code==1007){
          $.each(responseJson.data,function(index,value){
            responseHTML+='<option value="'+value.train_country+'">'+value.train_country+'</option>';
          });
        }
        else{
            responseHTML+='<option> No Country</option>';
        }
        $('#train_db_country').html(responseHTML);
        $('#train_db_country').on("change",function(){
          var getValue=$(this).val();
          $('#train_category').val("select");
          $('#train_db_city').html(' ');
          $("#train_db_city").multiselect('destroy');
          $('#train_db_city').multiselect({
            maxHeight: 184,
            buttonWidth: '155px',
            includeSelectAllOption: true,
            id:'train_db_city'
          });

          $('#train_db_train_code').html(' ');
          $("#train_db_train_code").multiselect('destroy');
          $('#train_db_train_code').multiselect({
            maxHeight: 184,
            buttonWidth: '155px',
            includeSelectAllOption: true,
            id:'train_db_train_code'
          });

            airportDbClass.getStateAirportDb(getValue);
        });

        }).fail(function(){
          alert("Network Problem");
        });
},

  /*---------------------------------------------
    Function Name:  getStateAirportDb()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  getStateAirportDb:function(rowId){
    var getUserId=window.localStorage.getItem('companyInfo');
    getUserId=JSON.parse(getUserId);
    $.ajax({
      url:this._Serverpath,
      type: 'POST',
      data: "action=getStateTrainDb&user_id="+getUserId[0].id+"&rowId="+rowId,
      }).done(function(response){
        var responseJson=response;
        if(typeof(response)=="string"){
          responseJson=JSON.parse(response);
        }
        var responseHTML='';
        responseHTML+='<option value="noState">Select State</option>';
        if(responseJson.code==1007){
          $.each(responseJson.data,function(index,value){
            responseHTML+='<option value="'+value.state_name+'">'+value.state_name+'</option>';
          });
        }
        else{
          responseHTML+='<option> No State</option>';

        }
        $('#train_db_state').html(responseHTML);
        $('#train_db_state').on("change",function(){
          var getValue=$(this).val();
          airportDbClass.getAiportCityAndCode(getValue);
        });

        }).fail(function(){
          alert("Network Problem");
        });
  },

  /*---------------------------------------------
    Function Name:  getImportDatabaseFIleSeaPort()
    Input Parameter: 
    return:json data
  ---------------------------------------------*/
	getImportDatabaseFIleSeaPort:function(){
		var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
      getUserId=JSON.parse(getUserId);
    }
    
    $.ajax({
      type: "POST",
      url:airportDbClass._Serverpath,
      data: "action=getImportDatabaseFIleTrain&user_id="+getUserId[0].id,
      success: function(result){
      	var getJson=result;
      	if(typeof(result)=="string"){
      		getJson=JSON.parse(result);
      	}
        if(getJson.code!=1006){
    	  var resultHTML='';
    	  for(var i=0; i<getJson.data.length; i++){
    		  if(getJson.data[i].status==1){
    			  resultHTML+='<tr><td>'+(i+1)+'</td><td class="train_db_code_'+getJson.data[i].id+'">'+getJson.data[i].train_code+'</td><td class="train_db_name_'+getJson.data[i].id+'">'+getJson.data[i].train_name+'</td><td class="train_db_zipcode_'+getJson.data[i].id+'">'+getJson.data[i].train_zip_code+'</td><td><button class="btn btn-warning not_view_btn">View</button></td><td><a class="btn btn-xs editTrainDb" seq="'+getJson.data[i].id+'" mainId="NoId">Edit</a><a class="btn btn-xs deactivateTrainDb" seq="'+getJson.data[i].id+'">Deactivate</a></td></tr>';	
    		  }
    		  else{
            resultHTML+='<tr><td>'+(i+1)+'</td><td class="train_db_code_'+getJson.data[i].id+'">'+getJson.data[i].train_code+'</td><td class="train_db_name_'+getJson.data[i].id+'">'+getJson.data[i].train_name+'</td><td class="train_db_zipcode_'+getJson.data[i].id+'">'+getJson.data[i].train_zip_code+'</td><td><button class="btn btn-primary viewTrainDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].train_id+'">View</td></td><td><a class="btn btn-xs editTrainDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].train_id+'">Edit</a><a class="btn btn-xs deactivateTrainDb" seq="'+getJson.data[i].id+'" mainId="'+getJson.data[i].train_id+'">Deactivate</a></td></tr>';	
          }
    	  }

        }
        else{
          resultHTML+='<tr><td colspan="6">Data Not Found</td></tr>';
        }

    	  $('#train_db_table').html(resultHTML);
        $('.not_view_btn').on("click",function(){
          alert("The train station you are trying to view/work-on is not activated yet. Please click on Edit button and fill details to activate train station.");
        });
    	  $('.deactivateTrainDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var confirmValue=confirm("Warning!  Are you sure, you wants to deactivate zone train. Deactivating will erase all saved data for zone train from system.");
      		if(confirmValue){
            airportDbClass.delseSmaAirportInformation(mainIdLocal,getSeq);
          }
        })

      	$('.viewTrainDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var train_db_code=$('.train_db_code_'+getSeq).html().trim();
      		var train_db_name=$('.train_db_name_'+getSeq).html().trim();
      		var train_db_zipcode=$('.train_db_zipcode_'+getSeq).html().trim();	
      		var getJson={"train_db_id":getSeq,"mainIdLocal":mainIdLocal,"train_db_code":train_db_code,"train_db_name":train_db_name,"train_db_zipcode":train_db_zipcode,"type":"view"};
  			  window.localStorage.setItem("train_db_table_id",JSON.stringify(getJson));
  				window.location.href='sma_train.html';
      	})
      	$('.editTrainDb').on("click",function(){
      		var getSeq=$(this).attr("seq");
      		var mainIdLocal=$(this).attr("mainId");
      		var train_db_code=$('.train_db_code_'+getSeq).html().trim();
      		var train_db_name=$('.train_db_name_'+getSeq).html().trim();
      		var train_db_zipcode=$('.train_db_zipcode_'+getSeq).html().trim();	
      		var getJson={"train_db_id":getSeq,"mainIdLocal":mainIdLocal,"train_db_code":train_db_code,"train_db_name":train_db_name,"train_db_zipcode":train_db_zipcode,"type":"edit"};
  			  window.localStorage.setItem("train_db_table_id",JSON.stringify(getJson));
			    window.location.href='sma_train.html';
        })
      }
	  });
  },

  /*---------------------------------------------
    Function Name:  airport_activeFormSubmit()
    Input Parameter: rowId
    return:json data
  ---------------------------------------------*/
  airport_activeFormSubmit:function(rowId){

      $('#refresh_overlay').css("display","block");
      var getUserId=window.localStorage.getItem('companyInfo');
      if(typeof(getUserId)=="string")
        {
           getUserId=JSON.parse(getUserId);
        } 
      $.ajax({
      type: "POST",
      url:airportDbClass._Serverpath,
      data: "action=train_activeFormSubmit&user_id="+getUserId[0].id+"&aiportId="+rowId,
       }).done(function(resultData){
            $('#refresh_overlay').css("display","none");
            location.reload();
          });
  },


  /*---------------------------------------------
    Function Name:  setImportDatabaseFIleSeaport()
    Input Parameter: 
    return:json data
  ---------------------------------------------*/
	setImportDatabaseFIleSeaport:function(){
  	$("#refresh_overlay").css('display','block');
    var getUserId=window.localStorage.getItem('companyInfo');
    if(typeof(getUserId)=="string"){
        getUserId=JSON.parse(getUserId);
      }

    var newformdata=new FormData($('#getformdata')[0]);
  	newformdata.append("action","setImportDatabaseFIleTrain");  
    newformdata.append("user_id",getUserId[0].id);
    $.ajax({
        type: "POST",
        url:this._Serverpath,
        data:newformdata,
        enctype: 'multipart/form-data',
        processData: false,  
        contentType: false ,
        dataType:'json',
        success: function(response){
          $("#refresh_overlay").css('display','none');
          alert("Data Imported successfully");
          location.reload();
        }
    });
},

/*---------------------------------------------
  Function Name: setManualTrain()
  Input Parameter: 
  return:json data
---------------------------------------------*/
setManualTrain:function(){
  $("#refresh_overlay").css('display','block');
  var getUserId=window.localStorage.getItem('companyInfo');
  if(typeof(getUserId)=="string"){
    getUserId=JSON.parse(getUserId);
  }

  var newformdata=new FormData($('#add_train_manual')[0]);
  newformdata.append("action","setManualTrain");  
  newformdata.append("user_id",getUserId[0].id);
  $.ajax({
    type: "POST",
    url:this._Serverpath,
    data:newformdata, 
    processData: false,  
    contentType: false ,
    dataType:'json',
    success: function(response) {
      $("#refresh_overlay").css('display','none');
      alert("Data Inserted successfully");
      location.reload();
    }
  });
}

}
airportDbClass.getImportDatabaseFIleSeaPort();
airportDbClass.getCountryAirportDb();
$('#trainDbFormSubmit').on("submit",function(event1){
  event1.preventDefault();
  var get_airport_db_airport_code_value=$('#train_db_train_code').val();
  airportDbClass.airport_activeFormSubmit(get_airport_db_airport_code_value);
});



