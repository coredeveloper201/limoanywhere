var zoneToZoneTollMatix = {
    _Serverpath: "phpfile/rate_point_client.php",
    _SERVICEPATHCAR: "phpfile/service.php",
    pageLoadFunction: function() {
        var zoneCity = window.localStorage.getItem('zoneCity');

        $('#tollZone-A-smaSetup').on('change', function() {

            var getSmaId = $('#tollZone-A-smaSetup').val();
            var getUserId = window.localStorage.getItem('companyInfo');

            if (typeof(getUserId) == "string") {
                getUserId = JSON.parse(getUserId);
            }
            $.ajax({
                url: getPointToPointClass._Serverpath,
                type: 'POST',
                data: "action=getzoneTozoneCountySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
                success: function(response) {
                    var responseOBJ = '';
                    if (typeof(response) == "string") {
                        responseOBJ = JSON.parse(response);
                    }
                    var getHtml = '';
                    var getPostalHtml = '';
                    var availableTags = [];
                    if (responseOBJ.code == 1007) {

                        $.each(responseOBJ.data, function(index, result) {
                            getHtml += '<option value="' + result.county_id + '">' + result.county_name + '</option>';
                        });
                        $('#tollZone-A-countySetup').html(getHtml);
                        $("#tollZone-A-countySetup").multiselect('destroy');
                        $('#tollZone-A-countySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });



                        setTimeout(function() {
                            var zoneCounty = localStorage.getItem('zoneCounty');

                            if (typeof(zoneCounty) == "string") {

                                var zoneCountyLocal = JSON.parse(zoneCounty);
                                // window.localStorage.removeItem('zoneCity');
                                var getArrayJSOn = [];
                                $.each(zoneCountyLocal, function(index, result) {

                                    if (result.type == "from") {
                                        getArrayJSOn.push(result.county_id);

                                    }
                                });

                                $("#tollZone-A-countySetup").val(getArrayJSOn);
                                $("#tollZone-A-countySetup").multiselect('refresh');



                                setTimeout(function() {
                                    $("#toll_zone_a_county_check").click();
                                }, 500)
                            }


                        }, 500)





                    }
                }
            });
        })
        $('#tollZone-B-smaSetup').on('change', function() {
            var getSmaId = $('#tollZone-B-smaSetup').val();
            var getUserId = window.localStorage.getItem('companyInfo');

            if (typeof(getUserId) == "string") {
                getUserId = JSON.parse(getUserId);
            }
            $.ajax({
                url: getPointToPointClass._Serverpath,
                type: 'POST',
                data: "action=getzoneTozoneCountySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
                success: function(response) {
                    var responseOBJ = '';
                    if (typeof(response) == "string") {
                        responseOBJ = JSON.parse(response);
                    }
                    var getHtml = '';
                    var getPostalHtml = '';
                    var availableTags = [];
                    if (responseOBJ.code == 1007) {
                        $.each(responseOBJ.data, function(index, result) {
                            getHtml += '<option value="' + result.county_id + '">' + result.county_name + '</option>';
                        });
                        $('#tollZone-B-countySetup').html(getHtml);
                        $("#tollZone-B-countySetup").multiselect('destroy');
                        $('#tollZone-B-countySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });


                        setTimeout(function() {
                            var zoneTOCounty = localStorage.getItem('zoneCounty');

                            if (typeof(zoneTOCounty) == "string") {

                                var zoneTOCountyLocal = JSON.parse(zoneTOCounty);
                                // window.localStorage.removeItem('zoneCity');
                                var zoneTOCountyJSOn = [];
                                $.each(zoneTOCountyLocal, function(index, result) {

                                    if (result.type == "to") {
                                        zoneTOCountyJSOn.push(result.county_id);

                                    }
                                });

                                $("#tollZone-B-countySetup").val(zoneTOCountyJSOn);
                                $("#tollZone-B-countySetup").multiselect('refresh');



                                setTimeout(function() {
                                    $("#toll_zone_b_county_check").click();
                                }, 500)
                            }


                        }, 500)







                    }
                }
            });
        });

        $('#toll_zone_a_city_check').on('click', function() {
            var tollZoneCityA = $('#tollZone-A-citySetup option:selected');
            if (service_type.length != 0) {
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState) {
                    tollZoneCityArray.push($(this).text());
                });
                var countryId = $("#tollZone-A-smaSetup").val();

                var getUserId = window.localStorage.getItem('companyInfo');
                getUserId = JSON.parse(getUserId);
                var getJson = {
                    "tollZone_A_smaSetup": tollZoneCityArray,
                    "user_id": getUserId[0].id,
                    "countryId": countryId,
                    "action": "getZoneZipCodes"
                };
                $.ajax({
                    url: getPointToPointClass._Serverpath,
                    type: 'POST',
                    data: getJson,
                    dataType: 'json',
                    success: function(response) {
                        var getHtml = '';
                        $.each(response.data, function(index, result) {
                            getHtml += '<option value="' + result + '">' + result + '</option>';
                        });
                        $('#toll_zone_a_city_check img').attr("src", "images/green_check.png");
                        $('#tollZone-A-zipcodeSetup').html(getHtml);
                        $("#tollZone-A-zipcodeSetup").multiselect('destroy');
                        $('#tollZone-A-zipcodeSetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });
                        setTimeout(function() {
                            var getZipLocalValue = localStorage.getItem('zoneZip');
                            if (typeof getZipLocalValue == "string") {
                                getZipLocalValue = JSON.parse(getZipLocalValue);
                                var getALlZipCod = [];
                                $.each(getZipLocalValue, function(zipindex, zipResult) {
                                    if (zipResult.type == "from") {
                                        getALlZipCod.push(zipResult.postal_code);
                                    }
                                });;
                                $("#tollZone-A-zipcodeSetup").val(getALlZipCod);
                                $("#tollZone-A-zipcodeSetup").multiselect('refresh');
                            }
                        }, 1000)
                    }
                });
            }
        })

        $('#toll_zone_b_city_check').on('click', function() {
            var tollZoneCityA = $('#tollZone-B-citySetup option:selected');
            if (tollZoneCityA.length != 0) {
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState) {
                    tollZoneCityArray.push($(this).text());
                });
                var countryId = $("#tollZone-B-smaSetup").val();

                var getUserId = window.localStorage.getItem('companyInfo');
                getUserId = JSON.parse(getUserId);
                var getJson = {
                    "tollZone_A_smaSetup": tollZoneCityArray,
                    "user_id": getUserId[0].id,
                    "countryId": countryId,
                    "action": "getZoneZipCodes"
                };
                $.ajax({
                    url: getPointToPointClass._Serverpath,
                    type: 'POST',
                    data: getJson,
                    dataType: 'json',
                    success: function(response) {
                        var getHtml = '';
                        $.each(response.data, function(index, result) {
                            getHtml += '<option value="' + result + '">' + result + '</option>';
                        });
                        $('#toll_zone_b_city_check img').attr("src", "images/green_check.png");
                        $('#tollZone-B-zipcodeSetup').html(getHtml);
                        $("#tollZone-B-zipcodeSetup").multiselect('destroy');
                        $('#tollZone-B-zipcodeSetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });
                        setTimeout(function() {
                            var getZipTOLocalValue = localStorage.getItem('zoneZip');
                            if (typeof getZipTOLocalValue == "string") {
                                getZipTOLocalValue = JSON.parse(getZipTOLocalValue);
                                var getZipTOLocalValueArray = [];
                                $.each(getZipTOLocalValue, function(zipindex, zipResult) {
                                    if (zipResult.type == "to") {
                                        getZipTOLocalValueArray.push(zipResult.postal_code);
                                    }
                                });
                                console.log("getZipTOLocalValueArray...",getZipTOLocalValueArray);
                                $("#tollZone-B-zipcodeSetup").val(getZipTOLocalValueArray);
                                $("#tollZone-B-zipcodeSetup").multiselect('refresh');
                            }
                        }, 1000)
                    }
                });
            }
        })
        $('#toll_zone_a_county_check').on("click", function() {
            var tollZoneCityA = $('#tollZone-A-countySetup option:selected');
            var countryId = $('#tollZone-A-smaSetup').val();
            if (service_type.length != 0) {
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState) {
                    tollZoneCityArray.push($(this).text());
                });


                var getUserId = window.localStorage.getItem('companyInfo');
                getUserId = JSON.parse(getUserId);
                var getJson = {
                    "tollZone_A_smaSetup": tollZoneCityArray,
                    "user_id": getUserId[0].id,
                    "countryId": countryId,
                    "action": "getzoneTozoneCitySma"
                };
                $.ajax({
                    url: getPointToPointClass._Serverpath,
                    type: 'POST',
                    data: getJson,
                    dataType: 'json',
                    success: function(response) {
                        var getHtml = '';
                        $.each(response.data, function(index, result) {
                            getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';

                        });
                        $('#toll_zone_a_county_check img').attr("src", "images/green_check.png");
                        $('#tollZone-A-citySetup').html(getHtml);
                        $("#tollZone-A-citySetup").multiselect('destroy');
                        $('#tollZone-A-citySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });



                        setTimeout(function() {
                            var zoneCity = localStorage.getItem('zoneCity');

                            if (typeof(zoneCity) == "string") {

                                var zoneCityLocal = JSON.parse(zoneCity);
                                // window.localStorage.removeItem('zoneCity');
                                var zoneCityArrayJSOn = [];
                                $.each(zoneCityLocal, function(index, result) {

                                    if (result.type == "from") {
                                        zoneCityArrayJSOn.push(result.city_id);

                                    }
                                });
                                console.log("zoneCityArrayJSOn...", zoneCityArrayJSOn)
                                $("#tollZone-A-citySetup").val(zoneCityArrayJSOn);
                                $("#tollZone-A-citySetup").multiselect('refresh');



                                setTimeout(function() {
                                    $("#toll_zone_a_city_check").click();
                                }, 500)
                            }


                        }, 500)



                    }
                });
            }
        })

        $('#toll_zone_b_county_check').on("click", function() {
            var tollZoneCityA = $('#tollZone-B-countySetup option:selected');
            var countryId = $('#tollZone-B-smaSetup').val();
            if (service_type.length != 0) {
                var tollZoneCityArray = [];
                $(tollZoneCityA).each(function(index, selectedState) {
                    tollZoneCityArray.push($(this).text());
                });


                var getUserId = window.localStorage.getItem('companyInfo');
                getUserId = JSON.parse(getUserId);
                var getJson = {
                    "tollZone_A_smaSetup": tollZoneCityArray,
                    "user_id": getUserId[0].id,
                    "countryId": countryId,
                    "action": "getzoneTozoneCitySma"
                };
                $.ajax({
                    url: getPointToPointClass._Serverpath,
                    type: 'POST',
                    data: getJson,
                    dataType: 'json',
                    success: function(response) {
                        var getHtml = '';
                        $.each(response.data, function(index, result) {
                            getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';

                        });
                        $('#toll_zone_b_county_check img').attr("src", "images/green_check.png");
                        $('#tollZone-B-citySetup').html(getHtml);
                        $("#tollZone-B-citySetup").multiselect('destroy');
                        $('#tollZone-B-citySetup').multiselect({
                            maxHeight: 200,
                            buttonWidth: '178px',
                            includeSelectAllOption: true
                        });


                        setTimeout(function(){
                            var zoneToCity=localStorage.getItem('zoneCity');

                           if (typeof(zoneToCity) == "string") {

                            var zoneToCityLocal = JSON.parse(zoneToCity);
                            // window.localStorage.removeItem('zoneCity');
                            var zoneToCityArrayJSOn = [];
                            $.each(zoneToCityLocal, function(index, result) {

                                if (result.type == "to") {
                                    zoneToCityArrayJSOn.push(result.city_id);

                                }
                            });
                            
                            $("#tollZone-B-citySetup").val(zoneToCityArrayJSOn);
                            $("#tollZone-B-citySetup").multiselect('refresh');


                        
                            setTimeout(function(){
                                $("#toll_zone_b_city_check").click();
                            },500)
                        }


                     },500)






                    }
                });
            }
        })


        // $('#tollZone-A-smaSetup').on('change', function() {



        //     var getSmaId = $('#tollZone-A-smaSetup').val();
        //     var getUserId = window.localStorage.getItem('companyInfo');



        //     if (typeof(getUserId) == "string") {
        //         getUserId = JSON.parse(getUserId);
        //     }
        //     $.ajax({
        //         url: getPointToPointClass._Serverpath,
        //         type: 'POST',
        //         data: "action=getzoneTozoneCitySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
        //         success: function(response) {
        //             var responseOBJ = '';
        //             if (typeof(response) == "string") {
        //                 responseOBJ = JSON.parse(response);
        //             }
        //             var getHtml = '';
        //             var getPostalHtml = '';
        //             var availableTags = [];
        //             if (responseOBJ.code == 1007) {
        //                 $.each(responseOBJ.data, function(index, result) {

        //                     availableTags.push(result.city_name);
        //                     getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';
        //                     getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
        //                 });




        //                      $('#tollZone-A-citySetup').html(getHtml);
        //                 $("#tollZone-A-citySetup").multiselect('destroy');
        //                 $('#tollZone-A-citySetup').multiselect({
        //                     maxHeight: 200,
        //                     buttonWidth: '178px',
        //                     includeSelectAllOption: true
        //                 });


        //                 $('#tollZone-A-zipcodeSetup').html(getPostalHtml);
        //                 $("#tollZone-A-zipcodeSetup").multiselect('destroy');
        //                 $('#tollZone-A-zipcodeSetup').multiselect({
        //                     maxHeight: 200,
        //                     buttonWidth: '178px',
        //                     includeSelectAllOption: true
        //                 });



        //              setTimeout(function(){

        //                    if (typeof(zoneCity) == "string") {

        //                     var zoneCityLocal = JSON.parse(zoneCity);
        //                     // window.localStorage.removeItem('zoneCity');
        //                     var getArrayJSOn = [];
        //                     $.each(zoneCityLocal, function(index, result) {

        //                         if (result.type == "from") {
        //                             getArrayJSOn.push(result.city_id);

        //                         }
        //                     });

        //                     $("#tollZone-A-citySetup").val(getArrayJSOn);
        //                     $("#tollZone-A-citySetup").multiselect('refresh');


        //                 }

        //              },1500)




        //             }
        //         }
        //     });
        // });
        // $('#tollZone-B-smaSetup').on('change', function() {
        //     var getSmaId = $('#tollZone-B-smaSetup').val();
        //     var getUserId = window.localStorage.getItem('companyInfo');
        //     if (typeof(getUserId) == "string") {
        //         getUserId = JSON.parse(getUserId);
        //     }
        //     $.ajax({
        //         url: getPointToPointClass._Serverpath,
        //         type: 'POST',
        //         data: "action=getzoneTozoneCitySma&sma_id=" + getSmaId + "&user_id=" + getUserId[0].id,
        //         success: function(response) {
        //             var responseOBJ = '';
        //             if (typeof(response) == "string") {
        //                 responseOBJ = JSON.parse(response);
        //             }
        //             var getHtml = '';
        //             var getPostalHtml = '';
        //             var availableTags = [];
        //             if (responseOBJ.code == 1007) {
        //                 $.each(responseOBJ.data, function(index, result) {
        //                     availableTags.push(result.city_name);
        //                     getHtml += '<option value="' + result.city_id + '">' + result.city_name + '</option>';
        //                     getPostalHtml += '<option value="' + result.postal_code + '">' + result.postal_code + '</option>';
        //                 });
        //                 $('#tollZone-B-citySetup').html(getHtml);
        //                 $("#tollZone-B-citySetup").multiselect('destroy');
        //                 $('#tollZone-B-citySetup').multiselect({
        //                     maxHeight: 200,
        //                     buttonWidth: '178px',
        //                     includeSelectAllOption: true
        //                 });

        //                 $('#tollZone-B-zipcodeSetup').html(getPostalHtml);
        //                 $("#tollZone-B-zipcodeSetup").multiselect('destroy');
        //                 $('#tollZone-B-zipcodeSetup').multiselect({
        //                     maxHeight: 200,
        //                     buttonWidth: '178px',
        //                     includeSelectAllOption: true
        //                 });


        //                    setTimeout(function(){

        //                    if (typeof(zoneCity) == "string") {

        //                     var zoneCityLocal = JSON.parse(zoneCity);
        //                     // window.localStorage.removeItem('zoneCity');
        //                     var getArrayJSOn = [];
        //                     $.each(zoneCityLocal, function(index, result) {

        //                         if (result.type == "to") {
        //                             getArrayJSOn.push(result.city_id);

        //                         }
        //                     });

        //                     $("#tollZone-B-citySetup").val(getArrayJSOn);
        //                     $("#tollZone-B-citySetup").multiselect('refresh');


        //                 }

        //              },1500);




        //             }
        //         }
        //     });
        // });





    }
};
zoneToZoneTollMatix.pageLoadFunction();