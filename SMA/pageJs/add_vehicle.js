
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Add vehicle page
  Author: Mylimoproject
---------------------------------------------*/ 
/*AddVehicle class start*/
var AddVehicle={
    /*set the path of php file for call the web services*/
	 _Serverpath:"phpfile/addVehicle_client.php",

   /*---------------------------------------------
    Function Name: deleteVehicleInformation
    Input Parameter:rowId
    return:json data
   ---------------------------------------------*/
    deleteVehicleInformation:function(rowId)
    {
        
        $('#refresh_overlay').css("display","block");
        var getForm={"action":"deleteVehicleInformation","rowId":rowId};
         $.ajax({
            url:AddVehicle._Serverpath,
            type:'POST',
            dataType:'json',
            data:getForm
         }).done(function(response) {

             $('#refresh_overlay').css("display","none");
             location.reload();

         });

     },
    
   /*---------------------------------------------
    Function Name: editVehicleInformation
    Input Parameter:rowId
    return:json data
   ---------------------------------------------*/
    editVehicleInformation:function(rowId){
      $('#refresh_overlay').css("display","block");
       var getForm={"action":"editVehicleInformation","rowId":rowId};
          $.when(AddVehicle.viewVehicleInformation(rowId,'edit')).done(function(){
              $('.SaveBtn').val("update");

                setTimeout(function(){

                    $("html, body").animate({ scrollTop: 0 }, "slow");  
                    $('#refresh_overlay').css("display","none");

                  },200);
           }).fail(function(){
              alert("unable to fetch value");
              $('#refresh_overlay').css("display","none");
           });

    },

  /*---------------------------------------------
    Function Name: getVehicleInformation
    Input Parameter:user_id
    return:json data
   ---------------------------------------------*/
  	getVehicleInformation:function(){
     
      var getuserId=window.localStorage.getItem("companyInfo");
      if(typeof(getuserId)=="string")
       {
        getuserId=JSON.parse(getuserId);
      }

       $('#refresh_overlay').css("display","block");
  	   var getForm={"action":"viewAllVehicleInformation","user_id":getuserId[0].id};
  	   $.ajax({
          url:AddVehicle._Serverpath,
          type:'POST',
          dataType:'json',
          data:getForm
       }).done(function(response) {

          var responseHTML=''; 
          console.log(response);
          if(response.code==1003)
          {
              
          	$.each(response.data,function(index,value){
              if(value.imageName == true){

                 responseHTML+='<tr><td>'+(parseInt(index)+1)+'</td><td>'+value.code+'</td><td>'+value.vehicle_type+'</td><td><img width="90px" height="50px" src="phpfile/vehicle_image/noimage.jpg"/></td><td>'+value.passenger_capacity+'</td><td>'+value.luggage_capacity+'</td><td><button class="btn btn-primary viewItem" seq="'+value.id+'">View</button></td><td><button class="btn btn-primary editItem" style="margin-right: 5%;" seq="'+value.id+'">Edit</button><button class="btn btn-primary deleteVehicle" seq="'+value.id+'">Delete</button></td></tr>';

              }else if(value.imageName[0].img_name){
                  if(value.vehicle_type === 'Limo'){
                      responseHTML+='<tr><td>'+(parseInt(index)+1)+'</td><td>'+value.code+'</td><td>'+value.vehicle_type+'</td><td><img width="70px" height="50px" src="'+value.imageName[0].img_name+'"/></td><td>'+value.passenger_capacity+'</td><td>'+value.luggage_capacity+'</td><td><button class="btn btn-primary viewItem" seq="'+value.id+'">View</button></td><td><button class="btn btn-primary editItem" style="margin-right: 5%;" seq="'+value.id+'">Edit</button><button class="btn btn-primary deleteVehicle" seq="'+value.id+'">Delete</button></td></tr>';
                  } else {
                      responseHTML+='<tr><td>'+(parseInt(index)+1)+'</td><td>'+value.code+'</td><td>'+value.vehicle_type+'</td><td><img width="70px" height="50px" src="phpfile/vehicle_image/'+value.imageName[0].img_name+'"/></td><td>'+value.passenger_capacity+'</td><td>'+value.luggage_capacity+'</td><td><button class="btn btn-primary viewItem" seq="'+value.id+'">View</button></td><td><button class="btn btn-primary editItem" style="margin-right: 5%;" seq="'+value.id+'">Edit</button><button class="btn btn-primary deleteVehicle" seq="'+value.id+'">Delete</button></td></tr>';
                  }
          		}
     
          	});

          }else{

          	responseHTML+="<tr><td>Data Not Found</td></tr>";

          }
           
          $('#vehicleItem').html(responseHTML);
          $('#refresh_overlay').css("display","none");
          $('.editItem').on("click",function(){

              var getSeq=$(this).attr("seq");
              $('.SaveBtn').show();
              AddVehicle.editVehicleInformation(getSeq);
            
          });

          $('.viewItem').on("click",function(){

              var getSeq=$(this).attr("seq");
              $.when(AddVehicle.viewVehicleInformation(getSeq)).then(function(result){
   

          })
              
              $('.SaveBtn').hide();
          });


          $('.deleteVehicle').on("click",function(){

              var getSeq=$(this).attr("seq"); 
              var confirmBox=confirm("Are you sure . you want to delete this item");
                  if(confirmBox)
                  {
                     var getSeq=$(this).attr("seq");
                      AddVehicle.deleteVehicleInformation(getSeq);
                  }
                  
               });

             
      	});
  		
  	},

   /*---------------------------------------------
    Function Name: viewVehicleInformation
    Input Parameter:rowId,ischeck
    return:json data
    ---------------------------------------------*/
     viewVehicleInformation:function(rowId,ischeck){
           $('#refresh_overlay').css("display","block");
           var getSeq=$(this).attr("seq");
           var getForm={"action":"viewVehicleInformation","rowId":rowId};

            $.ajax({
            url:AddVehicle._Serverpath,
            type:'POST',
            dataType:'json',
            data:getForm,
            }).done(function(response) {

                    $('#vehicle_code').val(response.data[0].code);
                    $('#vehicle_type').val(response.data[0].vehicle_type);
                    $('#vehicle_title').val(response.data[0].vehicle_title);
                    $('#vehicle_passenger_capacity').val(response.data[0].passenger_capacity);
                    $('#vehicle_luggage_capacity').val(response.data[0].luggage_capacity);
                    $('.SaveBtn').attr("seq",response.data[0].id);
                    var responseHTML='';
                    var i=1;
                    $('.commanUpload,.commanShowImage').html('');
                    $.each(response.data[0].imageName,function(index,value){
                        $('.newFile'+i).attr("value","phpfile/vehicle_image/"+value.img_name);
                        $('.showImageBeforeUpload_'+i).html('<img src="phpfile/vehicle_image/'+value.img_name+'" seq="img2" image_number="'+i+'">');
                        $('.imageAction_'+i).html('<button class="btn btn-primary deleteImageClass" seq="'+i+'" >delete</button>');
                         i++;
                         $('.deleteImageClass').off();

                        $('.deleteImageClass').on("click",function(){
                            var getseq=$(this).attr("seq");
                            $('.showImageBeforeUpload_'+getseq).html('');
                            $('.imageAction_'+getseq).html('');

                            $('input[seq='+getseq+']').val('');

                         });

                    });

                    if(ischeck!='edit'){

                           $('.deleteImageClass').prop("disabled",true);
                           $('.Backbtn').css('display',"block");
                           $('.fileUpload').prop( "disabled", true );

                     }else
                      {
                          $('.deleteImageClass').prop("disabled",false);
                          $('.Backbtn').css('display',"block");
                          $('.fileUpload').prop( "disabled", false );

                      }

                      setTimeout(function(){

                        $("html, body").animate({ scrollTop: 0 }, "slow")

                       },200);

                      $('#refresh_overlay').css("display","none");
            
            });

    },

   /*---------------------------------------------
     Function Name: setVehicleManual
     Input Parameter:all form data
     return:json data
    ---------------------------------------------*/
    	setVehicleManual:function(){
          $('#addVehicleForm').submit(function(event){
          $('#refresh_overlay').css("display","block");
          var getSeq=$(this).attr("seq");
    	   	event.preventDefault();
    		  var getUserId=window.localStorage.getItem('companyInfo');
      		if(typeof(getUserId)=="string")
      		 {
      			 getUserId=JSON.parse(getUserId);
      		 }

           if($('.SaveBtn').val()!="update")
            {   
               
                var getForm=new FormData($('#addVehicleForm')[0]);
                var getImageNumber=[];
                    for(var i=1; i<=5; i++)
                    {
                        if($('.file_type_'+i).val()!='')
                        {
                          getImageNumber.push($('.file_type_'+i).val());

                        }
                        
                    }
                
                    getForm.append("imageNumber",JSON.stringify(getImageNumber));   
                    getForm.append("action","addVehicleManualy");
                    getForm.append("user_id",getUserId[0].id);   
                    $.ajax({
                       url:AddVehicle._Serverpath,
                       type:'POST',
                       dataType:'json',
                       data:getForm,
                       contentType: false,
                       processData: false,
                    }).done(function(response) {
                       
                        console.log(response);
                        if(response.code!=1003)
                         {
                             alert('Successfully Inserted.');
                             location.reload();

                         }
                         else
                         {
                            alert("Vehicle code must be unique.");
                         }

                          $('#refresh_overlay').css("display","none");

                       });


            }
            else
            {

                var getForm=new FormData($('#addVehicleForm')[0]);
                getForm.append("action","updateVehicleManualy");
                getForm.append("user_id",getUserId[0].id);
                var getImageUrl=[];
                var imageSeq=[];
                var imageNumber=1;
                var getImageNumber=[];

                   for(var i=1; i<=5; i++)
                    {
                        if($('.file_type_'+i).val()!='')
                        {
                          getImageNumber.push($('.file_type_'+i).val());
                            
                        }
                        
                    }
                    
                 $('.commanUpload img').each(function(){

                    if($(this).attr("seq")=="img2")
                    {
                        var imgUrl=$(this).attr("src");
                        var imageSeqAttr=$(this).attr("image_number");
                        getImageUrl.push(encodeURIComponent(imgUrl));

                        imageSeq.push(imageSeqAttr);

                    }
                    
                });
               
                getForm.append("imageNumber",JSON.stringify(getImageNumber));
                getForm.append("imageNumberUpload",JSON.stringify(imageSeq));   
                getForm.append("externalUrl",JSON.stringify(getImageUrl));
                getForm.append("saveSeq",$('.SaveBtn').attr('seq'));
                   $.ajax({
                    url:AddVehicle._Serverpath,
                    type:'POST',
                    dataType:'json',
                    data:getForm,
                    contentType: false,
                    processData: false,
                    }).done(function(response) {

                      $('#refresh_overlay').css("display","none");      
                        alert("Successfully Update");
                        location.reload();

                    });

              }
    		
    		});


    }

}

/*call the all vehicle information on page load*/
AddVehicle.setVehicleManual();
AddVehicle.getVehicleInformation();