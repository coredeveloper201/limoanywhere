
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Blackout Date
  Author: Mylimoproject
---------------------------------------------*/

/*php file link to call the webservice on this page */
var _SERVICEPATH="phpfile/service.php";
var _SERVICEPATHServer="phpfile/client.php";
var _SERVICEPATHSma="phpfile/sma_client.php";
var _SERVICEPATHBlackOut="phpfile/blackout_client.php";
var ADDMILEAGE =0;
/*documnt ready function call */
$(function(){
   
    /*call back button on page*/
	$('#back_button').on("click",function(){

	    location.reload(true);

	});
   
    /*set time out function call*/
	setTimeout(function () {
        
        /*time picker function call*/  
        $('.time-picker').datetimepicker({

            pickDate: false,
            useCurrent: false,
            language: 'ru'

        });

		/*date picker function call*/ 

		$(".datepicker").datetimepicker({

		    pickTime: false,
            changeMonth: true,
            changeYear: true,
            "dateFormat": "dd-mm-yy"
   		});
    }, 1500);

   	
   /*on page load this function called*/
	getSMA();
	getVehicleType();	
	getRateMatrixList();

    /*click function on edid of blackout date*/ 
	$('.rate_matrix_onedit').on("click",function(){

	    getSequence=$(this).attr("seq");
		editRateMatrix(getSequence);

	});
				
});

/*call to check the is api key function*/
  isUserApiKey();
       
      /*---------------------------------------------
        Function Name: isUserApiKey()
        Input Parameter: rowId,categoryId
        return:json data
       ---------------------------------------------*/
        
		function isUserApiKey(){	

			var userInfo=window.localStorage.getItem("companyInfo");
			var userInfoObj=userInfo;	

			if(typeof(userInfo)=="string")
			{
			   userInfoObj=JSON.parse(userInfo);
			}

			$.ajax({
				url: _SERVICEPATHServer,
				type: 'POST',
				data: "action=getApiKey&user_id="+userInfoObj[0].id,
				success: function(response) {		

					var responseObj=response;

					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);			
					}

					if(responseObj.code=="1017")
					{

					    window.localStorage.setItem("apiInformation",JSON.stringify(responseObj.data[0]));		  		  
					    var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");

				        if(typeof(getLocalStoragevalueUserInformation)=="string")
					    {
					   	    getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
					    }

						$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);

					    $('#add_sma_button').on('click',function(){

						    $('#add_sma').show();								

						});	

						$("#popup_close_add_sma").off();

						$('#popup_close_add_sma').on("click",function(){
						   $('#add_sma').hide();

					    });	

					}else{		

						window.location.href="connectwithlimo.html";			
					}			

			    }

		    });

		}


       /*---------------------------------------------
         Function Name: getSMA()
         Input Parameter: rowId,categoryId
         return:json data
       ---------------------------------------------*/
	    function getSMA()
		{
			var userInfo=window.localStorage.getItem("companyInfo");
			var userInfoObj=userInfo;
		    if(typeof(userInfo)=="string")
			{
			   userInfoObj=JSON.parse(userInfo);
			}

			$.ajax({
				url: _SERVICEPATHSma,
				type: 'POST',
				data: "action=getSMA&user_id="+userInfoObj[0].id,
				success: function(response) {

					var responseObj=response;
					var responseHTML='';
					if(typeof(response)=="string")
						{
						  responseObj=JSON.parse(response);
						}	

					var responseHTML='';
					for(var i=0; i<responseObj.data.length; i++)
						{	
		                    if(responseObj.data[i].id!=null){

							    responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
		                    }
						}

					$('#apply_sma').html(responseHTML);

					setTimeout(function(){	

						$("#apply_sma").multiselect('destroy');

						$('#apply_sma').multiselect({

							maxHeight: 200,
							buttonWidth: '155px',
							includeSelectAllOption: true

						});

					},400);
						
				},
				error:function(){

					alert("Some Error");
				}

			});

		}

	   /*---------------------------------------------
         Function Name: getSMA()
         Input Parameter: rowId,categoryId
         return:json data
       ---------------------------------------------*/
		function getVehicleType()
		{

		    var getLocalStoragevalue=window.localStorage.getItem("apiInformation");

			if(typeof(getLocalStoragevalue)=="string")
			{
			   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);

			}

	       var getuserId=window.localStorage.getItem("companyInfo");
		    if(typeof(getuserId)=="string")
			{
			   getuserId=JSON.parse(getuserId);
			}

			$.ajax({
				url: _SERVICEPATH,
				type: 'POST',
				data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
				success: function(response) {
					var responseHTML='';
					var responseObj=response;
					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);					
					}
					responseHTML='';
					for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
					 {

					    responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
					}

					$('#apply_vehicle_type').html(responseHTML);

					setTimeout(function(){

					    $("#apply_vehicle_type").multiselect('destroy');
		            	$('#apply_vehicle_type').multiselect({
		                    maxHeight: 200,
		                    buttonWidth: '155px',
		                    includeSelectAllOption: true

		                });

					},400);

			    }

			});

	    }

	  /*---------------------------------------------
         Function Name: click on save rate button
         Input Parameter: form value
         return:json data
       ---------------------------------------------*/

	    $('#save_rate').on("click",function(){

			$('#refresh_overlay').css("display","block");
			var calender=$('#JQdatepicker').val();
			var toCalender=$('#endCalenderSet').val();
			var userInfo=window.localStorage.getItem("companyInfo");
			var userInfoObj=userInfo;
			if(typeof(userInfo)=="string")
				{
					userInfoObj=JSON.parse(userInfo);

				}
            /*select the vehicle type*/
			var valueObject = $('#value').val();
			var selectedVehicle = $('#apply_vehicle_type option:selected');
			var selectedVehicleObject = [];
			$(selectedVehicle).each(function(index, selectedVehicle){

			    selectedVehicleObject .push($(this).val());

			});
            /*select the sma*/
			var selectedSma = $('#apply_sma option:selected');
			var selectedSmaObject = [];
			$(selectedSma).each(function(index, selectedSma){
			    selectedSmaObject .push($(this).val());
			});

		    var msg=$('#blackout_msg').val();

		    if(valueObject=='')

				{
					alert("Please Enter Value");
				    $('#refresh_overlay').css("display","none");

				}

			else if(calender=='')
				{
					alert("Please Select  Start Date");
					$('#refresh_overlay').css("display","none");

				}
			else if(toCalender=='')
				{
					alert("Please Select  End Date");
			   		$('#refresh_overlay').css("display","none");

			    }
			else if(selectedVehicleObject=='')
				{
				    alert("Please Select At Least One Vehicle Code");
					$('#refresh_overlay').css("display","none");

				}

			else if(selectedSmaObject=='')
				{

					alert("Please Select At Least One SMA ");
				    $('#refresh_overlay').css("display","none");
				}

			else
				{
					var start_time=$('#start_time').val();
					var end_time=$('#end_time').val();
					var startTime = new Date(calender+" "+start_time);
					var endTime = new Date(toCalender+" "+end_time);
				    var getSubtractValue=endTime-startTime;
								
					if(getSubtractValue<0)
					   {
							$('#refresh_overlay').css("display","none");
							alert("End Date/Time should be greter than Start Date/Time");
							return 0;
						}


					if(start_time!='' && start_time!="null" && start_time!=null && start_time!=undefined && end_time!='' && end_time!="null" && end_time!=null && end_time!=undefined)

						{
							var start_time_array=start_time.split(':');
							var end_time_array=end_time.split(':');
							var start_hour=start_time_array[1].split(' ');
							var end_hour=end_time_array[1].split(' ');
							if(end_hour[1]!=start_hour[1])
							{

							}else{

								if(start_hour[1]=="PM"){

									start_time_array[0]=parseInt(start_time_array[0])+12;
							    }
								if(end_hour[1]=="PM"){

									end_time_array[0]=parseInt(end_time_array[0])+12;
								}
								var initial_time=parseInt(start_time_array[0])*60+parseInt(start_hour[0]);
								var final_time=parseInt(end_time_array[0])*60+parseInt(end_hour[0]);
							}

						}

					    if($('#save_rate').html()=='Update')
						{
						    $('#save_rate').html('Processing...');
						    $('#save_rate').attr("disable","true");
							var bd_id=$('#save_rate').attr('seq');

							$.ajax({
								url: _SERVICEPATHBlackOut,
								type: 'POST',
							    data: "action=deleteBlackOutDate&bd_id="+bd_id,
								success: function(response) {
									$('#refresh_overlay').css("display","none");
									if(typeof(response)=="string")
							        {
								      responseObj=JSON.parse(response);
							        }

							        if(responseObj.code==1010)
							        {

									    $.ajax({
											url: _SERVICEPATHBlackOut,
											type: 'POST',
											data: "action=setBlackoutDate&user_id="+userInfoObj[0].id+"&value="+valueObject+"&vehicle_code="+selectedVehicleObject+"&calender="+calender+"&start_time="+start_time+"&end_time="+end_time+"&msg="+msg+"&sma_id="+selectedSmaObject+"&toCalender="+toCalender,
										    success: function(response) {
												$('#refresh_overlay').css("display","none");
												alert("Blackout Date Updated Successfully");
											    location.reload(true);
											},
											error:function(){

												$('#refresh_overlay').css("display","none");
												$('#save_rate').html('Update');
							                    $('#save_rate').attr("disable","false");
								                alert("Some Error");
											}
											
								        });

					                }else{    

								        $('#refresh_overlay').css("display","none");
								        alert("Some Server Error while Updating.");
								        $('#save_rate').html('Update');
						    	        $('#save_rate').attr("disable","false");
							        }

						

								},
								error:function(){

								   $('#refresh_overlay').css("display","none");
							       $('#save_rate').html('Update');
						           $('#save_rate').attr("disable","false");
							       alert("Some Error");

							    }
								
						    });//Main Ajax

						}else{

							$('#save_rate').html('Processing...');
						    $('#save_rate').attr("disable","true");
							$.ajax({

								url: _SERVICEPATHBlackOut,
								type: 'POST',
								data: "action=setBlackoutDate&user_id="+userInfoObj[0].id+"&value="+valueObject+"&vehicle_code="+selectedVehicleObject+"&calender="+calender+"&start_time="+start_time+"&end_time="+end_time+"&msg="+msg+"&sma_id="+selectedSmaObject+"&toCalender="+toCalender,
								success: function(response) {

									$('#refresh_overlay').css("display","none");
									alert("Blackout Date Added Successfully");
									location.reload(true);

								},
								error:function(){

								    $('#refresh_overlay').css("display","none");
									$('#save_rate').html('Save');
						            $('#save_rate').attr("disable","false");
							        alert("Some Error");

								}

											
						    });

						}

				
	              }

		    });

	
	    /*---------------------------------------------
         Function Name: click on save rate button
         Input Parameter: form value
         return:json data
       ---------------------------------------------*/

		function getRateMatrixList(){
            $('#refresh_overlay').css("display","block");
			var userInfo=window.localStorage.getItem("companyInfo");
			var userInfoObj=userInfo;

			if(typeof(userInfo)=="string")
			{
			    userInfoObj=JSON.parse(userInfo);
			}

			$.ajax({

				url:_SERVICEPATHBlackOut,
				type:'POST',
				data:"action=getRateMatrixList&user_id="+userInfoObj[0].id,
				success:function(response){

					var responseObj=response;
					var responseHTML='';
					var responseVeh='';
					var responseSma='';
					var vehicle_code='';
					var sma_name='';
					var vehicle_code_array='';
					var sma_name_array='';
					var value_array='';
					var sma_id='';

					if(typeof(response)=="string")
					{
						responseObj=JSON.parse(response);

					}

					if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
					responseObj.data!="")
					{

						for(var i=0; i<responseObj.data.length; i++){

							responseVeh=0;
							responseSma=0;
							responseValue=0;
							vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
							vehicle_code_array=vehicle_code.split(',');
							sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
							sma_name_array=sma_name.split(',');
							sr_value = responseObj.data[i].value.replace(/,\s*$/, "");
							value_array=sr_value.split(',');
							var s=0;
							for(s=0;s<vehicle_code_array.length;s++)
							{
								responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';
							}
							var t=0;
							for(t=0;t<sma_name_array.length;t++)
							{
								responseSma +='<option selected disabled>'+sma_name_array[t]+'</option>';
							}
							var end_time =responseObj.data[i].end_time;
				                    end_time =end_time.split(':');
				            if(end_time[0]>12){
				                var end_time1=end_time[0]-12;
				                   end_time1='0'+end_time1+":"+end_time[1]+" "+"pm"
			                }else{
			                     end_time1=responseObj.data[i].end_time+" "+"am"
			                }

			                var start_time =responseObj.data[i].start_time;
				                start_time =start_time.split(':');
				            if(start_time[0]>12){

				                 var start_time1=start_time[0]-12;
				                 start_time1='0'+start_time1+":"+start_time[1]+" "+"pm"

			                }else{

			                     start_time1=responseObj.data[i].start_time+" "+"am"
			                }

			                var start_date=responseObj.data[i].calender;
				            var start_date=start_date.split('-');
				            var start_date=start_date[1]+"/"+start_date[2]+"/"+start_date[0];
			                var end_date=responseObj.data[i].tocalender;
				            var end_date=end_date.split('-');
				            var end_date=end_date[1]+"/"+end_date[2]+"/"+end_date[0];
								sma_id=responseObj.data[i].sma_id.replace(/,\s*$/, "");
							 	responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="rate_matrix_val_'+responseObj.data[i].id+'" msg="'+responseObj.data[i].msg+'">'+responseObj.data[i].value+'</td><td style="text-align:center;" id="rate_matrix_cal_'+responseObj.data[i].id+'" sr_val="'+sr_value +'">'+start_date+'</td><td style="text-align:center" id="cd_start_'+responseObj.data[i].id+'">'+start_time1+'</td><td style="text-align:center" id="to_calender'+responseObj.data[i].id+'">'+end_date+'</td><td style="text-align:center" id="cd_end_'+responseObj.data[i].id+'">'+end_time1+'</td><td style="text-align:center;" id="rate_matrix_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;" id="rate_matrix_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+'  onclick="editRateMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deleteBlackOutDate('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';

					    } 

					}else{

		                responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
				    }

					$('#rate_matrix_list').html(responseHTML)

					setTimeout(function(){

						$(".refresh_multi_select").multiselect('destroy');
						$('.refresh_multi_select').multiselect({

								maxHeight: 200,
								buttonWidth: '155px',
								includeSelectAllOption: true

						});

					},800);
                    $('#refresh_overlay').css("display","none");
			    },
                error:function(){

					alert("Some Error");
					$('#refresh_overlay').css("display","none");					

				}

						

		    });

	    }

	    /*---------------------------------------------
	        Function Name: editRateMatrix
	        Input Parameter: getSequence, status
	        return:json data
	    ---------------------------------------------*/
		function editRateMatrix(getSequence,status)
		{

			$('#refresh_overlay').css("display","block");
			$('#save_rate').html("Update");
		    $('#save_rate').attr("seq",getSequence);
			$('#back_button').css("display","inline-block");
			var package_code=$('#rate_matrix_'+getSequence).html();
			var amount=$('#sr_amount_'+getSequence).html();
			var rate_matrix_veh=$('#rate_matrix_veh_'+getSequence).attr("veh_code");
			    rate_matrix_veh=rate_matrix_veh.split(',');
			$("#apply_vehicle_type").val(rate_matrix_veh);
			$("#apply_vehicle_type").multiselect('refresh');

			var rate_matrix_sma=$('#rate_matrix_sma_'+getSequence).attr("sma_id");
				rate_matrix_sma=rate_matrix_sma.split(',');
			$("#apply_sma").val(rate_matrix_sma);
			$("#apply_sma").multiselect('refresh');
			if(status==1)
			{

			}

			$("#value").val($('#rate_matrix_val_'+getSequence).html());
			$("#JQdatepicker").val($('#rate_matrix_cal_'+getSequence).html());
		    $("#start_time").val($('#cd_start_'+getSequence).html());
			$("#end_time").val($('#cd_end_'+getSequence).html());
			$("#blackout_msg").val($('#rate_matrix_val_'+getSequence).attr('msg'));
			$('#endCalenderSet').val($('#to_calender'+getSequence).html());
			setTimeout(function(){

				$("html, body").animate({ scrollTop: 0 }, "slow");	
				$('#refresh_overlay').css("display","none");

			},200);
					
		}

	     /*---------------------------------------------
	        Function Name: deleteBlackOutDate
	        Input Parameter: getSequence,
	        return:json data
	     ---------------------------------------------*/

		function deleteBlackOutDate(getSequence){

			var res = confirm("Do You Really Want To Delete this Matrix?");
		    if (res == true) {

			    $('#refresh_overlay').css("display","block");

				$.ajax({

					url: _SERVICEPATHBlackOut,
					type: 'POST',
				    data:"action=deleteBlackOutDate&bd_id="+getSequence,
					success: function(response) {
						$('#refresh_overlay').css("display","none");
						getRateMatrixList();
							
					},
					error:function(){
						alert("Some Error");
						$('#refresh_overlay').css("display","none");

				    }

				});
			}else{

			}

		}


   /*---------------------------------------------
	  Function Name: datepicker
	  Input Parameter:pick date,
	  return:json data
	---------------------------------------------*/

    $(function() {

       $( "#JQdatepicker,#endCalenderSet" ).datepicker();
 	   $('#start_time, #end_time').timepicker({timeFormat: "hh:mm tt"});
	
    });
	setTimeout(function(){

		$("#apply_vehicle_type").multiselect('destroy');
			$('#apply_vehicle_type').multiselect({
			maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});

		$("#apply_sma").multiselect('destroy');
			$('#apply_sma').multiselect({
			maxHeight: 200,
			buttonWidth: '155px',
			includeSelectAllOption: true
		});					
	},900);
