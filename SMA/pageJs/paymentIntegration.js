/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: Airport
    Author: Mylimoproject
---------------------------------------------*/
var _SERVICEPATH = "phpfile/service.php";
var _SERVICEPATHServer = "phpfile/client.php";
var _SERVICEPATHSma = "phpfile/sma_client.php";
var _SERVICEPATHHoliday = "phpfile/holiday_client.php";
var _SERVICEPATHPAYMENTINTEG = "phpfile/paymentintegration_client.php";
var ADDMILEAGE = 0;
/*onload function call on page load*/
$(function () {


    /*click on back button*/
    $('#back_button').on("click", function () {
        location.reload(true);
    });
    /*set out function start*/
    /*setTimeout(function () {
        $('.time-picker').datetimepicker({
            pickDate: false,
            useCurrent: false,
            language: 'ru'
        });

        $(".datepicker").datetimepicker({
            pickTime: false,
            changeMonth: true,
            changeYear: true,
            "dateFormat": "dd-mm-yy"
        });

    },1500);*/

    $("#amount").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            return false;
        }

    });

    /*call the function on page load*/


    getPaymentMatrixList();

    /***************************************************************
     @Method Name    :On clicking edit button of the list
     @Desc            :Populate All the details of the Rate matrix
     ****************************************************************/
    $('.rate_matrix_onedit').on("click", function () {
        getSequence = $(this).attr("seq");
        editPaymentMatrix(getSequence);

    });
});

/*click on the onload */
isUserApiKey();

/*---------------------------------------------
    Function Name: isUserApiKey()
    Input Parameter: user_id
    return:json data
 ---------------------------------------------*/
function isUserApiKey() {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=getApiKey&user_id=" + userInfoObj[0].id,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.code == "1017") {
                window.localStorage.setItem("apiInformation", JSON.stringify(responseObj.data[0]));
                var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
                if (typeof(getLocalStoragevalueUserInformation) == "string") {
                    getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
                }
                $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
                $('#add_sma_button').on('click', function () {
                    $('#add_sma').show();
                })
                $("#popup_close_add_sma").off();
                $('#popup_close_add_sma').on("click", function () {
                    $('#add_sma').hide();
                });

            }
            else {
                window.location.href = "connectwithlimo.html";
            }
        }

    });

}


$('#checkout_option').on("change", function () {


    if ($('#checkout_option').val() == 'capture-deposit-and-store') {

        $('.auth_pay_option').css('display', 'block');

    } else {
        $('.auth_pay_option').css('display', 'none');
        $('#auth_capt_deposit_amt').val('');
    }


});

$('#gatewaySetup').on("click", function () {

    $('#refresh_overlay').css("display", "block");
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }


    var checkout_option = $('#checkout_option').val();
    var stripe_publishable_key = $('#stripe_publishable_key').val();
    var stripe_secret_key = $('#stripe_secret_key').val();
    var auth_capt_deposit_amt = $('#auth_capt_deposit_amt').val();
    var amount_type = $('#amount_type').val();
    var paymentGatewayOptions = $('#paymentGatewayOptions').val();


    var selectedVehicle = $('#apply_vehicle_type option:selected');
    var selectedVehicleObject = [];
    $(selectedVehicle).each(function (index, selectedVehicle) {
        selectedVehicleObject.push($(this).val());
    });
    var service_type = $('#service_type option:selected');
    var service_typeObj = [];
    $(service_type).each(function (index, service_type) {
        service_typeObj.push($(this).val());
    });
    var selectedSma = $('#apply_sma option:selected');
    var selectedSmaObject = [];
    $(selectedSma).each(function (index, selectedSma) {
        selectedSmaObject.push($(this).val());
    });


    var paymentGatewayOptions = $('#paymentGatewayOptions').val();
    var apiID = $('#apiID').val();
    var apiToken = $('#apiToken').val();
    var payment_gateway = $('#paymentGatewayOptions').val();
    var saveName = $('#payment_gateway').val();
    var is_default = $("#is_default").prop('checked') ? 1 : 0;


    if (checkout_option == '-1') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Select Checkout Option");
    }
    else if (stripe_publishable_key == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Enter stripe publishable key");
    }
    else if (stripe_secret_key == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Enter stripe stripe secret key ");
    }
    else if (auth_capt_deposit_amt == '' && checkout_option == 'capture-deposit-and-store') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Enter Authorize and Capture Deposit Amount ");
    }
    else if (amount_type == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Select Authorize and Capture Deposit Amount Type ");
    }
    else if (service_typeObj == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Enter Package Code");
    }
    else if (selectedVehicleObject == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Select At Least One Vehicle Code");
    }
    else if (selectedSmaObject == '') {
        $('#refresh_overlay').css("display", "none");
        alert("Please Select At Least One SMA ");
    }
    else {


        var update_Setup_id = $('#update_Setup_id').val();


        if ($('#gatewaySetup').val() == 'Update') {
            $('#gatewaySetup').html('Processing...');
            $('#gatewaySetup').attr("disable", "true");
            var holiday_id = $('#gatewaySetup').attr('seq');

            $.ajax({
                url: _SERVICEPATHPAYMENTINTEG,
                type: 'POST',
                data: "action=editPaymentGateway&is_default="+is_default+"&user_id=" + userInfoObj[0].id + "&checkout_option=" + checkout_option + "&stripe_publishable_key=" + stripe_publishable_key + "&stripe_secret_key=" + stripe_secret_key + "&auth_capt_deposit_amt=" + auth_capt_deposit_amt + "&amount_type=" + amount_type + "&update_Setup_id=" + update_Setup_id + "&vehicle_code=" + selectedVehicleObject + "&service_typeObj=" + service_typeObj + "&sma_id=" + selectedSmaObject + "&paymentGatewayOptions=" + paymentGatewayOptions + "&apiID=" + apiID + "&apiToken=" + apiToken + "&payment_gateway=" + payment_gateway + "&saveName=" + saveName + "&paymentGatewayOptions=" + paymentGatewayOptions,
                success: function (response) {
                    console.log(response);
                    $('#refresh_overlay').css("display", "none");
                    if (typeof(response) == "string") {
                        responseObj = JSON.parse(response);
                    }
                    if (responseObj.code == 1010) {
                        alert("Record Updated Succesfully.");
                        location.reload(true);
                    }
                    else {
                        alert("Some Server Error while Updating.");
                        $('#gatewaySetup').html('Update');
                        $('#gatewaySetup').attr("disable", "false");
                    }
                },
                error: function () {
                    $('#refresh_overlay').css("display", "none");
                    $('#gatewaySetup').html('Update');
                    $('#gatewaySetup').attr("disable", "false");
                    alert("Some Error");

                }


            });

        }
        else if ($('#gatewaySetup').val() == 'Back') {
            location.reload(true);
        }
        else {
            $('#gatewaySetup').html('Processing...');
            $('#gatewaySetup').attr("disable", "true");
            $.ajax({
                url: _SERVICEPATHPAYMENTINTEG,
                type: 'POST',
                data: "action=setPaymentGateway&is_default="+is_default+"&user_id=" + userInfoObj[0].id + "&checkout_option=" + checkout_option + "&stripe_publishable_key=" + stripe_publishable_key + "&stripe_secret_key=" + stripe_secret_key + "&auth_capt_deposit_amt=" + auth_capt_deposit_amt + "&amount_type=" + amount_type + "&vehicle_code=" + selectedVehicleObject + "&service_typeObj=" + service_typeObj + "&sma_id=" + selectedSmaObject + "&paymentGatewayOptions=" + paymentGatewayOptions + "&apiID=" + apiID + "&apiToken=" + apiToken + "&payment_gateway=" + payment_gateway + "&saveName=" + saveName + "&paymentGatewayOptions=" + paymentGatewayOptions,
                success: function (response) {
                    console.log(response);
                    $('#refresh_overlay').css("display", "none");
                    if (response == "Duplicate entry 'store-for-later' for key 'checkout_option'") {
                        alert("Eroor in adding payment setup or you are already added same payment setup");
                        location.reload(true);
                    } else if (typeof(response) == "string") {
                        responseObj = JSON.parse(response);
                    }
                    if (responseObj.code == 1008) {
                        alert("inserted Successfully");
                        location.reload(true);
                    }


                },
                error: function () {
                    $('#refresh_overlay').css("display", "none");
                    $('#gatewaySetup').html('Save');
                    $('#gatewaySetup').attr("disable", "false");
                    alert("Some Error");

                }

            });

        }

    }

});

/*********************************************************
 @MethodName            :getRateMatrix
 @Description        :Get list of all Rate matrixes
 @param                :user_id
 @return                :list of rate matrix
 *********************************************************/
function getPaymentMatrixList() {
    $('#refresh_overlay').css("display", "block");
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    $.ajax({
        url: _SERVICEPATHPAYMENTINTEG,
        type: 'POST',
        data: "action=getPaymentMatrixList&user_id=" + userInfoObj[0].id,
        success: function (response) {


            var responseObj = response;
            var responseHTML = '';
            var responseVeh = '';
            var responseSma = '';
            var responseValue = '';
            var vehicle_code = '';
            var sr_value = '';
            var sma_name = '';
            var vehicle_code_array = '';
            var sma_name_array = '';
            var value_array = '';
            var sma_id = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.data != null && responseObj.data != "null" && responseObj.data != undefined && responseObj.data != "undefined" &&
                responseObj.data != "") {
                for (var i = 0; i < responseObj.data.length; i++) {
                    responseVeh = 0;
                    responseSma = 0;
                    responseValue = 0;
                    vehicle_code = responseObj.data[i].vehicle_code.replace(/,\s*$/, "");
                    vehicle_code_array = vehicle_code.split(',');
                    sma_name = responseObj.data[i].sma_name.replace(/,\s*$/, "");
                    sma_name_array = sma_name.split(',');
                    // sr_value = responseObj.data[i].value.replace(/,\s*$/, "");
                    // value_array=sr_value.split(',');
                    sr_service = responseObj.data[i].service_type.replace(/,\s*$/, "");
                    var s = 0;
                    for (s = 0; s < vehicle_code_array.length; s++) {
                        responseVeh += '<option selected disabled>' + vehicle_code_array[s] + '</option>';
                    }
                    var t = 0;
                    for (t = 0; t < sma_name_array.length; t++) {
                        responseSma += '<option selected disabled>' + sma_name_array[t] + '</option>';
                    }
                    var u = 0;

                    sma_id = responseObj.data[i].sma_id.replace(/,\s*$/, "");


                    // responseHTML +='<td><a class="btn btn-xs view_payment_code" style="margin-left: 24%;" onclick="viewPaymentMatrix('+responseObj.data[i].id+',2)" id="view_'+responseObj.data[i].id+'" vehicle_seq="" rate_seq="" >View</a></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_'+responseObj.data[i]['id']+' inc_percent="'+responseObj.data[i].miles_increase_percent+'" onclick="editPaymentMatrix('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="deletePaymentSetup('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';
                    // responseObj.data[i].amount_type
                    responseHTML += '<tr seq="' + responseObj.data[i].id + '" data-default="' + responseObj.data[i].is_default + '"><td style="text-align:center" id="package' + responseObj.data[i].id + '">' + responseObj.data[i].payment_setup_as + '</td><td style="text-align:center;" checkoutoption="' + responseObj.data[i].checkout_option + '" gatewaytype="' + responseObj.data[i].gateway_type + '" api_id="' + responseObj.data[i].api_id + '" api_token="' + responseObj.data[i].api_token + '" payment_gateway_type="' + responseObj.data[i].payment_gateway_type + '" payment_setup_as="' + responseObj.data[i].payment_setup_as + '" sr_service="' + sr_service + '" id="stripe_publishable_key_' + responseObj.data[i].id + '">' + responseObj.data[i].stripe_publishable_key + '</td><td style="text-align:center" id="stripe_secret_key_' + responseObj.data[i].id + '">' + responseObj.data[i].stripe_secret_key + '</td><td style="text-align:center" id="auth_capt_deposit_amt_' + responseObj.data[i].id + '">' + responseObj.data[i].auth_capt_deposit_amt + '</td><td style="text-align:center" id="amount_type_' + responseObj.data[i].id + '">' + responseObj.data[i].amount_type + '</td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_veh_' + responseObj.data[i].id + '" veh_code="' + vehicle_code + '"><select class="refresh_multi_select" multiple>' + responseVeh + '</select></td><td style="text-align:center;width: 21%; text-align: justify;" id="rate_matrix_sma_' + responseObj.data[i].id + '" sma_id="' + sma_id + '" ><select class="refresh_multi_select" multiple>' + responseSma + '</select></td>{ <td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_rate_' + responseObj.data[i]['id'] + ' inc_percent="' + responseObj.data[i].miles_increase_percent + '" onclick="editPaymentMatrix(' + responseObj.data[i].id + ',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq=' + responseObj.data[i]['id'] + ' onclick="deletePaymentSetup(' + responseObj.data[i].id + ')">Delete</a> </div></td></tr>';

                }
            } else {
                responseHTML = '<tr><td colspan="1">Data not Found.</td></tr>';
            }

            $('#rate_matrix_list').html(responseHTML);
            $('#refresh_overlay').css("display", "none");
            setTimeout(function () {
                $(".refresh_multi_select").multiselect('destroy');
                $('.refresh_multi_select').multiselect({
                    maxHeight: 200,
                    buttonWidth: '155px',
                    includeSelectAllOption: true
                });

            }, 800);


        },
        error: function () {
            alert("Some Error");
            $('#refresh_overlay').css("display", "none");
        }

    });
}


/*********************************************************
 @MethodName            :editPaymentMatrix
 @Description        :edit the rate matrix
 @param                :user_id
 @return                :list of rate matrix
 *********************************************************/
function editPaymentMatrix(getSequence, status) {
    $('#update_Setup_id').val(getSequence);

    var rate_matrix_veh = $('#rate_matrix_veh_' + getSequence).attr("veh_code");
    rate_matrix_veh = rate_matrix_veh.split(',');

    $("#apply_vehicle_type").val(rate_matrix_veh);
    $("#apply_vehicle_type").multiselect('refresh');

    var rate_matrix_sma = $('#rate_matrix_sma_' + getSequence).attr("sma_id");
    rate_matrix_sma = rate_matrix_sma.split(',');
    $("#apply_sma").val(rate_matrix_sma);
    $("#apply_sma").multiselect('refresh');


    var is_default = $('#stripe_publishable_key_' + getSequence).closest('tr').attr("data-default");
    if(is_default == 1){
        $("#is_default").prop('checked', true);
    } else {
        $("#is_default").prop('checked', false);
    }

    var sr_service = $('#stripe_publishable_key_' + getSequence).attr("sr_service");
    sr_service = sr_service.split(',');

    $("#service_type").val(sr_service);
    $("#service_type").multiselect('refresh');


    $('#checkout_option').val($('#stripe_publishable_key_' + getSequence).attr("checkoutoption"));
    $('#paymentGatewayOptions').val($('#stripe_publishable_key_' + getSequence).attr("gatewaytype"));


    $('#paymentGatewayOptions').val($('#stripe_publishable_key_' + getSequence).attr("payment_gateway_type"));
    $('#apiID').val($('#stripe_publishable_key_' + getSequence).attr("api_id"));
    $('#apiToken').val($('#stripe_publishable_key_' + getSequence).attr("api_token"));
    $('#payment_gateway').val($('#stripe_publishable_key_' + getSequence).attr("payment_setup_as"));

    if ($('#stripe_publishable_key_' + getSequence).attr('checkoutoption') == 'capture-deposit-and-store') {
        $('.auth_pay_option').css('display', 'block');
    } else {
        $('.auth_pay_option').css('display', 'none');
    }


    $('#stripe_publishable_key').val($('#stripe_publishable_key_' + getSequence).html());
    $('#stripe_secret_key').val($('#stripe_secret_key_' + getSequence).html());
    $('#auth_capt_deposit_amt').val($('#auth_capt_deposit_amt_' + getSequence).html());
    $('#amount_type').val($('#amount_type_' + getSequence).html());
    $('#gatewaySetup').val('Update');
    setTimeout(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#refresh_overlay').css("display", "none");
    }, 200);


}

function viewPaymentMatrix(getSequence, status) {

    var getVehicleCode = $('#view_' + getSequence).attr('vehicle_seq');
    var getVehicleRate = $('#view_' + getSequence).attr('rate_seq');
    $('#checkout_option').val($('#checkout_option_' + getSequence).html());
    $('#stripe_publishable_key').val($('#stripe_publishable_key_' + getSequence).html());
    $('#stripe_secret_key').val($('#stripe_secret_key_' + getSequence).html());
    $('#auth_capt_deposit_amt').val($('#auth_capt_deposit_amt_' + getSequence).html());
    $('#amount_type').val($('#amount_type_' + getSequence).html());
    $('#gatewaySetup').val('Back');
    setTimeout(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        $('#refresh_overlay').css("display", "none");
    }, 200);
}


/*---------------------------------------------
       Function Name: getSMA()
       Input Parameter: user_id
       return:json data
   ---------------------------------------------*/
function getSMA() {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    $.ajax({
        url: _SERVICEPATHSma,
        type: 'POST',
        data: "action=getSMA&user_id=" + userInfoObj[0].id,
        success: function (response) {
            var responseObj = response;
            var responseHTML = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            var responseHTML = '';
            if (responseObj.data[0].sma_name != null) {
                for (var i = 0; i < responseObj.data.length; i++) {
                    responseHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].sma_name + '</option>';
                }
            }
            $('#apply_sma').html(responseHTML);

            setTimeout(function () {

                $("#apply_sma").multiselect('destroy');
                $('#apply_sma').multiselect({
                    maxHeight: 200,
                    buttonWidth: '155px',
                    includeSelectAllOption: true
                });
            }, 400);

        },
        error: function () {

            alert("Some Error");
        }

    });

}

/*---------------------------------------------
   Function Name: getAiportCityAndCode()
   Input Parameter: rowId,categoryId
   return:json data
---------------------------------------------*/
function getServiceTypes() {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    var serviceTypeData = [];
    $.ajax({
        url: "phpfile/service_type.php",
        type: 'post',
        data: 'action=GetServiceTypes&user_id=' + userInfoObj[0].id,
        dataType: 'json',
        success: function (data) {
            if (data.ResponseText == 'OK') {
                var ResponseHtml = '';
                $.each(data.ServiceTypes.ServiceType, function (index, result) {
                    ResponseHtml += "<option value='" + result.SvcTypeCode + "'>" + result.SvcTypeDescription + "</option>";
                });

                $('#service_type').html(ResponseHtml);
            }
            $("#service_type").multiselect('destroy');
            $('#service_type').multiselect({
                maxHeight: 200,
                bttonWidth: '178px',
                includeSelectAllOption: true

            });
        }
    });
}

/*---------------------------------------------
   Function Name: getVehicleType()
   Input Parameter: user_id
   return:json data
---------------------------------------------*/
function getVehicleType() {
    var getLocalStoragevalue = window.localStorage.getItem("apiInformation");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);

    }

    var getuserId = window.localStorage.getItem("companyInfo");
    if (typeof(getuserId) == "string") {
        getuserId = JSON.parse(getuserId);
    }

    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=GetVehicleTypes&limoApiID=" + getLocalStoragevalue.limo_any_where_api_id + "&user_id=" + getuserId[0].id + "&limoApiKey=" + getLocalStoragevalue.limo_any_where_api_key,
        success: function (response) {
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);

            }
            responseHTML = '';
            for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {
                responseHTML += '<option value="' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '">' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '</option>';
            }

            $('#apply_vehicle_type').html(responseHTML);
            setTimeout(function () {
                $("#apply_vehicle_type").multiselect('destroy');
                $('#apply_vehicle_type').multiselect({
                    maxHeight: 200,
                    buttonWidth: '155px',
                    includeSelectAllOption: true
                });
            }, 400);
        }

    });
}


/*********************************************************
 @MethodName            :deletePaymentSetup
 @Description        :delete the holiday
 @param                :getSequence
 @return                :list of rate matrix
 *********************************************************/
function deletePaymentSetup(getSequence) {
    var res = confirm("Do You Really Want To Delete This?");
    if (res == true) {
        $('#refresh_overlay').css("display", "block");
        $.ajax({
            url: _SERVICEPATHPAYMENTINTEG,
            type: 'POST',
            data: "action=deletePaymentSetup&setup_id=" + getSequence,
            success: function (response) {
                $('#refresh_overlay').css("display", "none");
                getPaymentMatrixList();
            },
            error: function () {
                alert("Some Error");
                $('#refresh_overlay').css("display", "none");
            }

        });

    }
    else {

    }
}


getSMA();
getServiceTypes();
getVehicleType();


