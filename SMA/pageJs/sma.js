/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: SMA
    Author: Mylimoproject
---------------------------------------------*/
var _SERVICEPATH = "phpfile/service.php";
var _SERVICEPATHServer = "phpfile/client.php";
var _SERVICEPATHSma = "phpfile/sma_client.php";


/*load the function on onload of page*/
$(function () {

    /*click on the on change function of of add sam*/
    $('.add_sma_country').on("change", function () {
        $('#refresh_overlay').css("display", "block");
        $('#sma_state_check').find('img').attr("src", "images/ok_checkmark_red_T.png");
        $('#sma_city_check').find('img').attr("src", "images/checked-yellow.png");
        $('#sma_county_check').find('img').attr("src", "images/checked-yellow.png");
        var country_id = $(this).val();
        country_id = country_id.split('a@a');
        country_id = country_id[0];
        getSmaState(country_id, 0);
    })

    /*click on sma city check*/
    $('#sma_city_check').on("click", function () {
        $('#refresh_overlay').css("display", "block");
        var selectedCity = $('#add_sma_city_1 option:selected');
        if (selectedCity.length != 0) {
            var selectedCityObject = '';
            $(selectedCity).each(function (index, selectedCity) {
                selectedCityObject += ($(this).attr('seq')) + ',';
            });
            selectedCityObject = selectedCityObject.replace(/,\s*$/, "");
            $('#postal_code').val(selectedCityObject);
            getPostal(selectedCityObject, 0);
        }
        else {
            alert("Please Select City");
        }
    });
    /*set the set time out function start*/
    setTimeout(function () {
        $("#add_sma_city_1").multiselect('destroy');
        $('#add_sma_city_1').multiselect({
            maxHeight: 800,
            buttonWidth: '155px',
            includeSelectAllOption: true
        });

        $('#add_sma_city_1').next().find('.multiselect').css("width", "145px");
        $("#add_sma_state_1").multiselect('destroy');
        $('#add_sma_state_1').multiselect({
            maxHeight: 200,
            buttonWidth: '155px',
            includeSelectAllOption: true,
            id: 'select_sma_state'
        });

        $('#add_sma_state_1').next().find('.multiselect').css("width", "145px");
        $("#add_sma_county_1").multiselect('destroy');
        $('#add_sma_county_1').multiselect({
            maxHeight: 800,
            buttonWidth: '155px',
            includeSelectAllOption: true
        });
        $('#add_sma_county_1').next().find('.multiselect').css("width", "145px");
    }, 900);
    /*click on sma state check*/
    $("#sma_state_check").on("click", function () {
        var selectedState = $('#add_sma_state_1 option:selected');
        if (selectedState.length != 0) {
            var selectedStateObject = [];
            $(selectedState).each(function (index, selectedState) {
                selectedStateObject.push($(this).attr('seq'));
            });
            $('#refresh_overlay').css("display", "block");
            getSmaCounty(selectedStateObject, 0);
        }

        else {
            alert("Please Select State");
        }
    });
    /*click on sma country ckeck */
    $('#sma_county_check').on("click", function () {
        $('#refresh_overlay').css("display", "block");
        var selectedCounty = $('#add_sma_county_1 option:selected');
        if (selectedCounty.length != 0) {
            var selectedCountyObject = [];
            $(selectedCounty).each(function (index, selectedCounty) {
                selectedCountyObject.push($(this).attr('seq'));
            });
            getSmaCity(selectedCountyObject, 0);
        }
        else {

            alert("Please Select County");
        }
    });


    /*on click add sma location*/
    $('#add_sma_location').on("click", function () {
        $('#refresh_overlay').css("display", "black");
        var sma_name = $('#add_sma_name').val();
        sma_name = sma_name.trim();
        var isDefault = 0;
        if (document.getElementById('set_default').checked == "true" || document.getElementById('set_default').checked == true) {
            isDefault = 1;
        }
        var postal_code = $('#postal_code').val();
        var selectedCity = $('#add_sma_city_1 option:selected');
        var selectedCounty = $('#add_sma_county_1 option:selected');
        var selectedState = $('#add_sma_state_1 option:selected');
        var selectedCountry = $('#add_sma_country_1').val();
        var selectedStateObject = [];
        var selectedCountryObject = [];
        var selectedCountyObject = [];
        var selectedCityObject = [];
        var add_sma_city = '';
        var add_sma_county = '';
        var add_sma_state = '';
        var add_sma_country = '';
        var sma_json = "";
        var add_sma_county = $(this).val();
        if (selectedCity.length != 0 && selectedCounty.length != 0 && selectedState.length != 0) {
            $(selectedCity).each(function (index, selectedCity) {
                add_sma_city = $(this).val();
                add_sma_county = $(this).attr('cnty_detail');
                add_sma_county = add_sma_county !== undefined ? add_sma_county.split('a@a') : '';
                add_sma_city = add_sma_city.split('a@a');
                add_sma_state = $(this).attr('st_detail');
                add_sma_state = add_sma_state !== undefined ? add_sma_state.split('a@a') : '';
                add_sma_country = $(this).attr('cntry_detail');
                add_sma_country = add_sma_country !== undefined ? add_sma_country.split('a@a') : '';
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: add_sma_county[0],
                    county_name: add_sma_county[1],
                    city_id: add_sma_city[0],
                    city_name: add_sma_city[1],
                    postal_code: postal_code,
                    sma_name: sma_name,
                    isDefault: isDefault
                };

                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }
        else if (selectedCounty.length != 0 && selectedState.length != 0) {
            $(selectedCounty).each(function (index, selectedCounty) {
                add_sma_county = $(this).val();
                add_sma_county = add_sma_county.split('a@a');
                add_sma_state = $(this).attr('st_detail');
                add_sma_state = add_sma_state.split('a@a');
                add_sma_country = selectedCountry;
                add_sma_country = add_sma_country.split('a@a');
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: add_sma_county[0],
                    county_name: add_sma_county[1],
                    city_id: 0,
                    city_name: '',
                    postal_code: '',
                    sma_name: sma_name,
                    isDefault: isDefault
                };
                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }
        else {
            $(selectedState).each(function (index, selectedState) {
                add_sma_state = $(this).val();
                add_sma_state = add_sma_state.split('a@a');
                add_sma_country = selectedCountry;
                add_sma_country = add_sma_country.split('a@a');
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: 0,
                    county_name: '',
                    city_id: 0,
                    city_name: '',
                    postal_code: '',
                    sma_name: sma_name,
                    isDefault: isDefault
                };
                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }

        if (selectedCountry != "") {
            if (add_sma_state != "") {
                if (sma_name != "") {
                    var type = '';
                    if ($('#add_sma_location').html() == "Update") {
                        type = "Update";
                        if (sma_name == $('#old_sma_name').val()) {
                            $.ajax({
                                url: _SERVICEPATHSma,
                                type: 'POST',
                                data: "action=deletesmaLoc&smaLoc_id=" + getSequence,
                                success: function (response) {
                                    var responseObj = response;
                                    var responseHTML = '';
                                    if (typeof(response) == "string") {
                                        responseObj = JSON.parse(response);
                                        setSmaLocation(JSON.stringify(selectedCityObject), isDefault);

                                    }
                                }

                            });
                        }
                        else {
                            checkUniqueSma(sma_name, type, selectedCityObject, $('#hidden_sma_id').val(), isDefault);
                        }
                    }
                    else {
                        type = "add";
                        checkUniqueSma(sma_name, type, selectedCityObject, $('#hidden_sma_id').val(), isDefault)
                    }
                }
                else {
                    alert("Please Give Name to Service Market to Proceed");
                }
            }
            else {
                alert("Please Select State/Province to Proceed");
            }
        }
        else {
            alert("Please Select Country to Proceed");
        }
    });

    /*on click back sma location*/
    $('#back_sma_location').on("click", function () {

        alert('3');
        location.reload();
    });

});

isUserApiKey();

/*---------------------------------------------
	   Function Name: isUserApiKey()
	   Input Parameter:
	   return:json data
	---------------------------------------------*/

function isUserApiKey() {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=getApiKey&user_id=" + userInfoObj[0].id,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.code == "1017") {
                window.localStorage.setItem("apiInformation", JSON.stringify(responseObj.data[0]));
                var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
                if (typeof(getLocalStoragevalueUserInformation) == "string") {
                    getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
                }
                $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
                getSmaCountry();
                // getSmaLocList();
                $('#add_sma_button').on('click', function () {
                    $('#add_sma').show();
                })
                $("#popup_close_add_sma").off();
                $('#popup_close_add_sma').on("click", function () {
                    $('#add_sma').hide();
                });
            }
            else {
                window.location.href = "connectwithlimo.html";
            }
        }
    });
}

/*---------------------------------------------
	   Function Name: uploadSmaCsv()
	   Input Parameter:
	   return:json data
	---------------------------------------------*/
function uploadSmaCsv() {
    $('.uploadSmaCsv').attr('disabled', 'true');
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    var fd = new FormData();
    var profile_pic = $('#smaCsvFile').prop('files')[0];
    fd.append('profile_pic', profile_pic);
    fd.append('action', "uploadSmaCsv");
    fd.append('user_id', userInfoObj[0].id);
    $.ajax({
        url: _SERVICEPATHSma,
        type: "POST",
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
                if (responseObj.code == 1003) {
                    alert("Location database Successfully Uploaded");
                }
                else {
                    alert("Some error");
                }
            }

            $('.uploadSmaCsv').removeAttr('disabled');
            $('#smaCsvFile').val('');
        },

        error: function () {
            alert("Some Error.");
            $('.uploadSmaCsv').removeAttr('disabled');
            $('#smaCsvFile_' + sma_id).val('');
        }
    });
}


/*---------------------------------------------
	   Function Name: pointToPointAllOnclickFunction()
	   Input Parameter:
	   return:json data
	---------------------------------------------*/
function pointToPointAllOnclickFunction() {
    $('.show_data_sma').slideUp("slow");
    var sma_id = '';
    $(".show_details_sma").on("click", function () {
        sma_id = $(this).attr("seq");
        getSmaCountry();
        $('.save_sma_details').attr("seq", sma_id);
        if ($(this).hasClass('active')) {
            $(this).removeClass("active");
            $('#show_data_sma_' + sma_id).slideUp("slow");
            $(".change_icon i").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
        }
        else {
            $(this).addClass("active");
            $('.show_data_sma').slideUp("slow");
            $('#show_data_sma_' + sma_id).slideDown("slow");
            $(".change_icon i").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
            // getSmaLocList(sma_id);
        }
    });

    $('#sma_city_check').on("click", function () {
        var selectedCity = $('#add_sma_city_1 option:selected');
        var selectedCityObject = [];
        $(selectedCity).each(function (index, selectedCity) {
            selectedCityObject += ($(this).attr('seq')) + ',';
        });
        getPostal(selectedCityObject);
    });

    $('#sma_state_check').on("click", function () {
        var selectedState = $('#add_sma_state_1 option:selected');
        var selectedStateObject = [];
        $(selectedState).each(function (index, selectedState) {
            selectedStateObject.push($(this).attr('seq'));
        });
        getSmaCounty(selectedStateObject, 0);
    });

    $('#sma_county_check').on("click", function () {
        var selectedCounty = $('#add_sma_county_1 option:selected');
        var selectedCountyObject = [];
        $(selectedCounty).each(function (index, selectedCounty) {
            selectedCountyObject.push($(this).attr('seq'));
        });
        getSmaCity(selectedCountyObject, 0);
    })

    /*on click popup close button*/
    $('#popup_close_vrc_air_rate').on("click", function () {
        $('#signup_form_overlay_airport_rate').hide();
    })

    $('#popup_close_vrc_sea_rate').on("click", function () {
        $('#signup_form_overlay_seaport_rate').hide();
    })

    $('#add_sma_location').on("click", function () {
        $('#refresh_overlay').css("display", "block");
        var sma_name = $('#add_sma_name').val();
        sma_name = sma_name.trim();
        var isDefault = 1;
        var postal_code = $('#postal_code').val();
        postal_code = postal_code.split(',');
        var selectedCity = $('#add_sma_city_1 option:selected');
        var selectedStateObject = [];
        var selectedCountryObject = [];
        var selectedCountyObject = [];
        var selectedCityObject = [];
        var add_sma_city = '';
        var add_sma_county = '';
        var add_sma_state = '';
        var add_sma_country = '';
        var sma_json = "";
        $(selectedCity).each(function (index, selectedCity) {
            if (postal_code[index] == 0) {
                postal_code = '';
            }
            add_sma_city = $(this).val();
            add_sma_county = $(this).attr('cnty_detail');
            add_sma_county = add_sma_county.split('a@a');
            add_sma_city = add_sma_city.split('a@a');
            add_sma_state = $(this).attr('st_detail');
            add_sma_state = add_sma_state.split('a@a');
            add_sma_country = $(this).attr('cntry_detail');
            add_sma_country = add_sma_country.split('a@a');
            sma_json = {
                country_id: add_sma_country[0],
                country_name: add_sma_country[1],
                state_id: add_sma_state[0],
                state_name: add_sma_state[1],
                county_id: add_sma_county[0],
                county_name: add_sma_county[1],
                city_id: add_sma_city[0],
                city_name: add_sma_city[1],
                postal_code: postal_code[index],
                sma_name: sma_name,
                isDefault: isDefault
            };
            selectedCityObject.push(JSON.stringify(sma_json));
        });

        if (add_sma_country != "" && add_sma_state != "" && add_sma_city != "" && add_sma_county != "" && sma_name != "" && postal_code != "") {
            setSmaLocation(JSON.stringify(selectedCityObject));
            $('#signup_form_overlay_airport_rate').hide();
        }
        else {
            $('#refresh_overlay').css("display", "none");
            alert("All fields are required");

        }

    });
    /*clik on save details button*/
    $('.save_sma_details').on("click", function () {
        var sma_id = $(this).attr("seq");
        if ($('.save_sma_details').html() == "Update") {
            var add_sma_country = $('#add_sma_country_1').val();
            add_sma_country = add_sma_country.split('a@a');
            sma_country_name = add_sma_country[1];
            sma_country_id = add_sma_country[0];

            var add_sma_county = $('#add_sma_county_1').val();
            add_sma_county = add_sma_county.split('a@a');
            sma_county_id = add_sma_county[0];
            sma_county_name = add_sma_county[1];

            var add_sma_state = $('#add_sma_state_1').val();
            add_sma_state = add_sma_state.split('a@a');
            sma_state_id = add_sma_state[0];
            sma_state_name = add_sma_state[1];

            var add_sma_city = $('#add_sma_city_1').val();
            add_sma_city = add_sma_city.split('a@a');
            sma_city_id = add_sma_city[0];
            sma_city_name = add_sma_city[1];

            var postal_code = $('#postal_code').val();
            if (sma_country_name != "" && sma_state_name != "" && sma_city_name != "") {
                updateSmaLocation(sma_country_name, sma_country_id, sma_county_id, sma_county_name, sma_state_id, sma_state_name, sma_city_id, sma_city_name, postal_code, sma_id);
                $('#signup_form_overlay_airport_rate').hide();
            }
            else {
                alert("All fields are required");
            }

        }
        else {

        }
    });

    $('#add_airport_rate_city').on("change", function () {

        var city_id = $('#add_airport_rate_city').val();
        onChangePointFromzone(city_id);
    });

    $('#service_airport').on("change", function () {
        var airport = $('.show_airport').html();
        var zone = $('.show_zone').html();
        if ($(this).val() == "AIRD") {
            var state_code = $('#add_sma_country').val();
            onChangePointFromCity(state_code);
            var city_id = $('#add_airport_rate_city').val();
            onChangePointFromzone(city_id);
            $('.show_zone').html(airport);
            $('.show_airport').html(zone);

        }
        else {
            $('.show_airport').html(zone);
            $('.show_zone').html(airport);
        }
        $('#add_airport_rate_city').on("change", function () {
            var city_id = $('#add_airport_rate_city').val();
            onChangePointFromzone(city_id);
        });

    });

    $('#service_seaport').on("change", function () {
        var seaport = $('.show_seaport').html();
        var zone = $('.show_zone_seaport').html();
        if ($(this).val() == "SEAD") {
            var state_code = $('#add_seaport_state_to').val();
            onChangePointtoCity(state_code);
            var city_id = $('#add_seaport_city_to').val();
            onChangePointtozone(city_id);
            $('.show_zone_seaport').html(seaport);
            $('.show_seaport').html(zone);
        }
        else {
            $('.show_seaport').html(zone);
            $('.show_zone_seaport').html(seaport);

        }
        $('#add_seaport_state_to').on("change", function () {
            var state_code = $('#add_seaport_state_to').val();
            onChangePointtoCity(state_code);
        });

        $('#add_seaport_city_to').on("change", function () {
            var city_id = $('#add_seaport_city_to').val();
            onChangePointtozone(city_id);

        });

    });
}


/*---------------------------------------------
       Function Name: onChangePointtoCity()
       Input Parameter: state_code
       return:json data
    ---------------------------------------------*/
function onChangePointtoCity(state_code) {

    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=getCityPoitAirport&user_id=" + userInfoObj[0].id + "&state_code=" + state_code,
        success: function (response) {
            var responsetoHTML = '';
            var responseSeaporttoHTML = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            responsetoHTML += '<option>Select City</option>';
            responseSeaporttoHTML += '<option>Select City</option>';
            for (var i = 0; i < responseObj.data.length; i++) {

                responsetoHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].city_name + '</option>';
                responseSeaporttoHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].city_name + '</option>';
            }
            $('#add_point_rate_to_city').html(responsetoHTML);
            $('#add_seaport_city_to').html(responseSeaporttoHTML);
        }

    });

}

/*---------------------------------------------
       Function Name: onChangePointFromzone()
       Input Parameter: city_id
       return:json data
    ---------------------------------------------*/
function onChangePointFromzone(city_id) {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }

    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=getZonePoitAirport&user_id=" + userInfoObj[0].id + "&city_id=" + city_id,
        success: function (response) {
            var responsetoHTML = '';
            var responseAirportFromHTML = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            for (var i = 0; i < responseObj.data.length; i++) {

                responsetoHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].zone_name + '</option>';
                responseAirportFromHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].zone_name + '</option>';
            }
            $('#add_point_rate_from_zone').html(responsetoHTML);
            $('#add_airport_rate_zone').html(responseAirportFromHTML);
        }

    });

}


/*---------------------------------------------
       Function Name: deletesmaLoc()
       Input Parameter: getSequence
       return:json data
   ---------------------------------------------*/
function deletesmaLoc(getSequence) {
    $.ajax({
        url: _SERVICEPATHSma,
        type: 'POST',
        data: "action=deletesmaLoc&smaLoc_id=" + getSequence,
        success: function (response) {
            var responseObj = response;
            var responseHTML = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
                $('#refresh_overlay').css("display", "none");
            }
            alert('SMA has been removed successfully');
            window.location.href = '';
        }

    });
}


/*---------------------------------------------
       Function Name: commanFunction()
       Input Parameter: getSequence
       return:add active class
   ---------------------------------------------*/
function commanFunction() {
    $(this).addClass("active");
}

/*---------------------------------------------
       Function Name: getPerPassengerCar()
       Input Parameter: getSequence
       return:add active class
   ---------------------------------------------*/
function getPerPassengerCar() {
    var getLocalStoragevalue = window.localStorage.getItem("apiInformation");
    if (typeof(getLocalStoragevalue) == "string") {
        getLocalStoragevalue = JSON.parse(getLocalStoragevalue);
    }
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: "action=GetVehicleTypes&limoApiID=" + getLocalStoragevalue.limo_any_where_api_id + "&limoApiKey=" + getLocalStoragevalue.limo_any_where_api_key,
        success: function (response) {
            var responseHTML = '';
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }

            responseHTML = '<div class="row"><div class="col-sm-3" style="margin-left: 407px; margin-top: 12px;">Select Per Passenger rate to upload:</div><div class="col-sm-3"><input type="file" name="fileToUploadPerPessanger" class="btn btn-default add_color" value="browse" id="fileToUploadPerPessanger" style="padding: 1%;"></div><div class="col-sm-3"><input type="submit" class="btn btn-default add_color" value="Upload rate" name="submit" style="padding:1%;margin-left:396%;margin-top: -20%;" onclick="uploadCsvFilePerPeasonger();"></div></div>';
            for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {

                responseHTML += '<div class="panel-heading"> <h4 class="panel-title"> <a class="accordion-toggle ng-binding" ng-click="isOpen = !isOpen" accordion-transclude="heading"> <div class="row ng-scope show_per_passenger" seq=' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '> <div class="col-sm-6"> <span class="vehicle-name text-black ng-binding"> <i class="fa fa-truck"></i> ' + responseObj.VehicleTypes.VehicleType[i].VehTypeTitle + '- ' + responseObj.VehicleTypes.VehicleType[i].PassengerCapacity + 'Passengers </span> </div> <div class="col-sm-5"> <span class="ng-binding">1 hourly rate tiers</span> </div> <div class="change_icon col-xs-1"> <i class="pull-right glyphicon text-muted glyphicon-chevron-right" id="glyphiconIcon_' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '"></i> </div> </div> </a> </h4></div><div class=" show_data_per_passenger" id="show_data_per_passenger_' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '"> <accordion-heading class="ng-scope"></accordion-heading> <div class="col-sm-12"> <div class="top-links"> <a class="btn btn-default popup_passenger_price" style="background-color: #32323A;padding: 1.3%; margin-top: 11px;" seq=' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '> <i class="fa fa-plus"></i> Add rate </a> </div> </div> <table class="table table-condensed ng-scope"> <thead > <tr > <th>Number of Passenger</th> <th>Rate</th> <th>Applied to Location</th> <th>Action</th> <th class="table-action"> <span class="table-action"></span> </th> </tr> </thead > <tbody id="Passenger_price_' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '"></tbody> </table></div>';
            }

            $('#all_PerPessanger_rate').html(responseHTML);
            $('.popup_passenger_price').on("click", function () {
                var getSequence = $(this).attr("seq");
                $('#save_per_passanger_rate').html("Save");
                $('#save_per_passanger_rate').attr("seq", getSequence);
                $('#per_passenger_price').show();
                $('#add_passenger_rate').val('');
                $('#add_multiselect_location').val('');
                $('#add_multiselect_location').multiselect("refresh");

            });

            $('#save_per_passanger_rate').on("click", function () {
                if ($('#save_per_passanger_rate').html() == "Update") {
                    var sequence = $(this).attr("seq");
                    var addPassengerRate = $('#add_passenger_rate').val();
                    var addMultiselectLocation = $('#add_multiselect_location').val();
                    if (addPassengerRate != "" && addMultiselectLocation != null) {
                        updatePassengerRate(sequence, addPassengerRate, addMultiselectLocation);
                        $('#per_passenger_price').hide();

                    }
                    else {

                        alert("All fields are required");
                    }

                }
                else {

                    var sequence = $(this).attr("seq");
                    var add_base_rate_name = $('#add_passenger_rate').val();
                    var add_multiselect_location = $('#add_multiselect_location').val();
                    if (add_base_rate_name != "" && add_multiselect_location != null) {
                        setPassengerRate(1, add_base_rate_name, add_multiselect_location, sequence);
                        $('#per_passenger_price').hide();

                    }
                    else {

                        alert("All fields are required");

                    }

                }

            });

            $('#popup_close_passenger_price').on("click", function () {

                $('#per_passenger_price').hide();

            });
            $(".show_per_passenger").off();
            $(".show_per_passenger").on("click", function () {
                var sequence = $(this).attr("seq");
                if ($(this).hasClass('active')) {

                    $(this).removeClass("active");
                    $('#show_data_per_passenger_' + sequence).slideUp("slow");
                    $(".change_icon i").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
                }
                else {

                    $('.show_details_distance').removeClass("active");
                    $(this).addClass("active");
                    $('.show_data_per_passenger').slideUp("slow");
                    $('#show_data_per_passenger_' + sequence).slideDown("slow");
                    $(".change_icon i").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
                    getPassengerRate(sequence);
                }

            });

            commanFunction();

        }

    });

}


/*---------------------------------------------
       Function Name: getPerPassengerCar()
       Input Parameter: getSequence
       return:add active class
   ---------------------------------------------*/

function getPassengerRate(sequence) {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }

    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=getPassengerRate&user_id=" + userInfoObj[0].id + "&vehicle_code=" + sequence,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            var responseHTML = '';
            for (var i = 0; i < responseObj.data.length; i++) {
                responseHTML += '<tr id="passenger_' + sequence + '_' + responseObj.data[i].id + '"><td id="no_of_passenger_' + sequence + '_' + responseObj.data[i].id + '">' + responseObj.data[i].no_of_passenger + '</td> <td>$<span id="passenger_rate_' + sequence + '_' + responseObj.data[i].id + '">' + responseObj.data[i].rate + '</span></td> <td id="passenger_city_' + sequence + '_' + responseObj.data[i].id + '">' + responseObj.data[i].code + '</td> <td><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs edit_passenger_price" seq="' + sequence + '_' + responseObj.data[i].id + '" >Edit</a> <a class="btn btn-xs delete_passenger_price" seq="' + sequence + '_' + responseObj.data[i].id + '">Delete</a></div> </td> </tr>';
            }
            $('#Passenger_price_' + sequence).html(responseHTML);
            $('.edit_passenger_price').off();
            $('.edit_passenger_price').on("click", function () {
                var sequence = $(this).attr("seq");
                var noOfPassenger_ = $.trim($('#no_of_passenger_' + sequence).html());
                var passengerRate = $.trim($('#passenger_rate_' + sequence).html());
                var passengerCity = $.trim($('#passenger_city_' + sequence).html());
                var cities_stateArray = passengerCity.split(",");
                var cities_state_mixArray = [];
                var j = 0;
                for (var i = 0; i < cities_stateArray.length; i++) {
                    cities_state_mixArray[j] = "state_" + cities_stateArray[i];
                    j++;
                }

                for (var i = 0; i < cities_stateArray.length; i++) {
                    cities_state_mixArray[j] = "city_" + cities_stateArray[i];
                    j++;
                }

                $('#add_passenger_rate').val(passengerRate);
                setTimeout(function () {
                    $('#add_multiselect_location').val(cities_state_mixArray);
                    $('#add_multiselect_location').multiselect("refresh");
                    $('#save_per_passanger_rate').html("Update");
                    $('#save_per_passanger_rate').attr("seq", sequence);
                    $('#per_passenger_price').show();
                }, 400);

            });

            $('.delete_passenger_price').off();
            $('.delete_passenger_price').on("click", function () {
                var sequence = $(this).attr("seq");
                $('#passenger_' + sequence).remove();
                deletePassengerRate(sequence);
            });

        }

    });

}


$('#popup_edit_additional').on("click", function () {

    $('#signup_form_overlay_additional_adddd').show();

});

$('#popup_edit_additional').on("click", function () {

    $('#signup_form_overlay_additional_adddd').hide();
})
$('#Miles_check').on("change", function () {
    if ($("#Miles_check").is(':checked')) {
        $("#next_mile").hide();
        $('#add_miles').val("Remaining");
    }
    else {
        $("#next_mile").show();
        $('#add_miles').val("Remaining ");
    }

});


$(".show_detail_peakhours").on("click", function () {

    if ($(this).hasClass('active')) {
        $(this).removeClass("active");
        $('.show_data_peakhours').slideUp("slow");
        $(".change_icon i").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");

    }
    else {

        $(this).addClass("active");
        $('.show_data_peakhours').slideDown("slow");
        ;
        $(".change_icon i").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
    }

});
$('#hours_check').on("change", function () {
    if ($("#hours_check").is(':checked')) {

        $('#cities_hourly').val("");
        $('#cities_hourly').multiselect("refresh");
        $('#remaining_hr').css(" margin-left", "-15%");
        $("#hide_selected_box").hide();  // checked
    }
    else {

        $("#hide_selected_box").show();
        $('#cities_hourly').val("");
        $('#cities_hourly').multiselect("refresh");
    }

});

/*---------------------------------------------
       Function Name: perPassengerPrice()
       Input Parameter: smaId
       return:json data
    ---------------------------------------------*/
function perPassengerPrice() {
    $('#save_pointtopoint_rate').on("click", function () {

        var add_passenger = $('#add_passenger_rate').val();
        var multiselect_location = $('#add_multiselect_location').val();

    });

}


$(document).ready(function () {

    //called when key is pressed in textbox

    $("#amount_fix_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_base_rate_amount").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_base_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#no_of_seats").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_no_of_seats").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#fixed_rate_child").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_fixed_rate_child").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_passenger_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_passenger_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_airport_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_airport_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_seaport_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_seaport_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_point_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_point_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#start_miles").keypress(function (e) {



        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_start_miles").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_miles").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_miles").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_rate_mile").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_rate_mile").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#Start_hours").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_Start_hours").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_hours").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_hours").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#add_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_add_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#minimum_hour").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_minimum_hour").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

    $("#minimum_base_rate").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything

        if (e.which != 46 && e.which != 45 && e.which != 46 &&

            !(e.which >= 48 && e.which <= 57)) {

            //display error message

            $("#errmsg_minimum_base_rate").html("Digits Only").show().fadeOut("slow");

            return false;

        }

    })

})

/*###################  */

/*###################################### code to become input time hourly in rate setup page#####################################*/


function getSmaCountry() {

    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);


    }


    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=getSmaCountry&user_id=" + userInfoObj[0].id,

        success: function (response) {

            var smaCountry = [];

            var responseObject = '';

            var j = 0;

            if (typeof(response) == "string") {

                responseObject = JSON.parse(response);

            }

            var respsonseHTML = '';

            respsonseHTML = '<option value="">Select Country</option>';

            for (var i = 0; i < responseObject.data.length; i++) {

                if (responseObject.data[i].country_name != "state") {

                    smaCountry[j] = responseObject.data[i];

                    respsonseHTML += '<option value="' + responseObject.data[i].id + 'a@a' + responseObject.data[i].country_name + '" id="country_option_' + responseObject.data[i].id + '" seq=' + responseObject.data[i].id + '>' + responseObject.data[i].country_name + '</option>';

                    j++;

                }

            }

            $('.add_sma_country').html(respsonseHTML);


        },

        error: function () {

            alert("Some Error");
        }

    });

}


function getSmaCounty(state_id, status, county) {

    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=getSmaCounty&state_id=" + state_id,

        success: function (response) {

            var responseHTML = '';

            if (typeof(response) == "string") {

                responseObj = JSON.parse(response);

            }

            if (responseObj.data != "null" && responseObj.data != null) {

                $.each(responseObj.data, function (i, data) {

                    var state_name = responseObj.data[i]['state_name'];

                    var state_id = responseObj.data[i]['state_id'];

                    responseHTML += '<optgroup label="' + state_name + '">';

                    $.each(responseObj.data[i]['county'], function (index, data) {

                        var data1 = responseObj.data[i]['county'];

                        if (data1[index]['id'] != "null" && data1[index]['id'] != null) {

                            $('#sma_state_check').find('img').attr("src", "images/green_check.png");

                            responseHTML += '<option value="' + data1[index]['id'] + 'a@a' + data1[index]['county_name'] + '" id="county_option_' + data1[index]['id'] + '" seq="' + data1[index]['id'] + '" st_detail="' + state_id + 'a@a' + state_name + '">' + data1[index]['county_name'] + '</option>';

                        }

                    });


                    responseHTML += '</optgroup>';

                });

            }


            $('#add_sma_county_1').html(responseHTML);

            setTimeout(function () {
                if(county !== null){
                    $('#add_sma_county_1').val(county);
                } else {
                    $("#add_sma_county_1").val('');
                }


                $("#add_sma_county_1").multiselect('destroy');

                $('#add_sma_county_1').multiselect({

                    maxHeight: 800,

                    buttonWidth: '155px',

                    includeSelectAllOption: true

                });

                $('#add_sma_county_1').next().find('.multiselect').css("width", "145px");

                if (status != 1) {

                    $('#refresh_overlay').css("display", "none");

                }


            }, 400);


            $("#add_sma_city_1").val('');

            $("#add_sma_city_1").multiselect('refresh');

            $('#postal_code').val('');


        }

    })

}


function getSmaState(country_id, status, states) {

    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=getSmaState&country_id=" + country_id,

        success: function (response) {
            var smaCountry = [];
            var responseObject = '';
            var j = 0;
            if (typeof(response) == "string") {
                responseObject = JSON.parse(response);
            }
            var respsonseHTML = '';
            if (responseObject.data != "null" && responseObject.data != false) {
                for (var i = 0; i < responseObject.data.length; i++) {
                    if (responseObject.data[i].state_name != "") {
                        respsonseHTML += '<option value="' + responseObject.data[i].id + 'a@a' + responseObject.data[i].state_name + '" seq="' + responseObject.data[i].id + '" id="state_option_' + responseObject.data[i].id + '">' + responseObject.data[i].state_name + '</option>';
                    }
                }
            }
            else {
                respsonseHTML += '<option value="">No Value Exists</option>';
            }
            $('#add_sma_state_1').html(respsonseHTML);
            setTimeout(function () {
                if(states !== null){
                    $('#add_sma_state_1').val(states);
                } else {
                    $("#add_sma_state_1").val('');
                }
                $("#add_sma_state_1").multiselect('destroy');
                $('#add_sma_state_1').multiselect({

                    maxHeight: 200,

                    buttonWidth: '155px',

                    includeSelectAllOption: true,

                    id: 'select_sma_state'

                });
                $('#add_sma_state_1').next().find('.multiselect').css("width", "145px");
                if (status != 1) {
                    $('#refresh_overlay').css("display", "none");
                }
            }, 400);
            $("#add_sma_county_1").val('');
            $("#add_sma_county_1").multiselect('refresh');
            $("#add_sma_city_1").val('');
            $("#add_sma_city_1").multiselect('refresh');
            $('#postal_code').val('');
        },

        error: function () {

            $('#refresh_overlay').css("display", "none");

            alert("Some Error");
        }

    });

}


function getSmaCityUpdate(county_id, status, City) {
    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);


    }

    var getSequenceUpdateCity = $('#add_sma_location').attr("sma_id");


// var getSequenceUpdateCity=$('#sma_city_'+getSequenceUpdateCity).attr('city_val');


    var editedCityUpdate = $('#sma_city_' + getSequenceUpdateCity).attr('city_val');

    editedCityUpdate = editedCityUpdate.split(',');
    var finalCityNameUpdate = [];
    $.each(editedCityUpdate, function (index, result) {

        var getSelectBoxTextValue = result.split('a@a');

        if ($.inArray(getSelectBoxTextValue[1], finalCityNameUpdate) == -1 && getSelectBoxTextValue[1] != undefined) {

            finalCityNameUpdate.push(getSelectBoxTextValue[1]);


        }


    });


    finalCityNameUpdate = JSON.stringify(finalCityNameUpdate);


    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=getSmaCityUpdate&county_id=" + county_id + "&citySelected=" + finalCityNameUpdate + "&user_id=" + userInfoObj[0].id,

        success: function (response) {

            var smaCountry = [];

            var responseObject = '';

            var j = 0;

            if (typeof(response) == "string") {

                responseObject = JSON.parse(response);

            }

            var responseHTML = '';

            if (responseObject.data != "null" && responseObject.data != null) {

                $('#sma_county_check').find('img').attr("src", "images/green_check.png");

                console.log(responseObject.data);
                $.each(responseObject.data, function (i, data) {

                    var country_id = responseObject.data[i]['country_id'];

                    var country_name = responseObject.data[i]['country_name'];

                    var state_id = responseObject.data[i]['state_id'];

                    var state_name = responseObject.data[i]['state_name'];

                    var county_id = responseObject.data[i]['county_id'];

                    var county_name = responseObject.data[i]['county_name'];

                    responseHTML += '<optgroup label="' + county_name + '">';


                    /* new code start here */

                    var getCityName = ''
                    var getCityIDArray = [];
                    var getArrayValue = [];
                    var getCityID = '';

                    /*$.each(responseObject.data[i]['city'], function (index, result) {

                        if (result['id'] != "null" && result['id'] != null) {


                            getCityName = result.city_name;

                            if ($.inArray(getCityName, getArrayValue) == -1) {


                                getArrayValue.push(getCityName);
                                if (getCityID != '') {
                                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));
                                }
                                getCityID = '';


                            }
                            getCityID += result.id + ',';


                        }


                    });*/


                    var count = 0;
                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));


                    /* new code end here  */


                    $.each(getArrayValue, function (index, result12) {

                        responseHTML += '<option value="' + getCityIDArray[count] + 'a@a' + result12 + '" allCityName="' + result12 + '" id="county_option_' + getCityIDArray[count] + '" seq="' + getCityIDArray[count] + '" cnty_detail="' + county_id + 'a@a' + county_name + '" st_detail="' + state_id + 'a@a' + state_name + '" cntry_detail="' + country_id + 'a@a' + country_name + '">' + result12 + '</option>';
                        count++;

                    });


                    $.each(responseObject.data[i]['city'], function (index, data) {
                        if(i < 2){
                            var data1 = responseObject.data[i]['city'];
                            if (data1[index]['id'] != "null" && data1[index]['id'] != null) {
                                responseHTML += '<option value="' + data1[index]['id'] + 'a@a' + data1[index]['city_name'] + '" id="county_option_' + data1[index]['id'] + '" seq="' + data1[index]['id'] + '" cnty_detail="' + county_id + 'a@a' + county_name + '" st_detail="' + state_id + 'a@a' + state_name + '" cntry_detail="' + country_id + 'a@a' + country_name + '">' + data1[index]['city_name'] + '</option>';


                            }
                        }
                    });


                    responseHTML += '</optgroup>';


                });


            }

            $('#add_sma_city_1').html(responseHTML);

            setTimeout(function () {
console.log(City);
                if(City !== null){
                    $('#add_sma_city_1').val(City);
                } else {
                    $("#add_sma_city_1").val('');
                }
                //getState(cityAndState);

                // $("#add_sma_city_1").val('');

                $("#add_sma_city_1").multiselect('destroy');

                $('#add_sma_city_1').multiselect({

                    maxHeight: 800,

                    buttonWidth: '155px',

                    includeSelectAllOption: true

                });

                $('#add_sma_city_1').next().find('.multiselect').css("width", "145px");

                // Code For Selecting Optgroup


                if (status != 1) {

                    $('#refresh_overlay').css("display", "none");

                }

            }, 400);

            $('#postal_code').val('');

            if (status == 1) {

                var getSequence = $('#add_sma_location').attr("sma_id");

                setTimeout(function () {


                    var editedState = $('#sma_state_' + getSequence).attr('stt_val');

                    editedState = editedState.split(',');

                    $("#add_sma_state_1").val(editedState);

                    $("#add_sma_state_1").multiselect('refresh');

                    var editedCounty = $('#sma_county_' + getSequence).attr('cnty_val');

                    editedCounty = editedCounty.split(',');

                    $("#add_sma_county_1").val(editedCounty);

                    $("#add_sma_county_1").multiselect('refresh');

                    var editedCity = $('#sma_city_' + getSequence).attr('city_val');

                    editedCity = editedCity.split(',');
                    var finalCityName = [];
                    $.each(editedCity, function (index, result) {

                        var getSelectBoxTextValue = result.split('a@a');

                        if ($.inArray(getSelectBoxTextValue[1], finalCityName) == -1 && getSelectBoxTextValue[1] != undefined) {

                            finalCityName.push(getSelectBoxTextValue[1]);


                        }


                    });


                    /* text end here */

                    var varValue = finalCityName;
                    var j = 0, size2 = varValue.length;

                    $('#add_sma_city_1').val('')
                    $("#add_sma_city_1").multiselect("refresh");


                    $options2 = $('#add_sma_city_1 option');
                    for (j; j < size2; j++) {

                        $options2.filter('[allcityname="' + varValue[j] + '"]').prop('selected', true);


                    }


                    var valArr = editedCity; // array of option values
                    var i = 0, size = valArr.length, // index and array size declared here to avoid overhead
                        $options = $('#add_sma_city_1 option'); // options cached here to avoid overhead of fetching inside loop

                    // run the loop only for the given values
                    for (i; i < size; i++) {
                        // filter the options with the specific value and select them


                        $options.filter('[value="' + valArr[i] + '"]').prop('selected', true);
                    }


                    $("#add_sma_city_1").multiselect("refresh");


                    var postal_code = $('#sma_postal_' + getSequence).attr('seq');

                    if (postal_code != undefined && postal_code != "undefined") {

                        $('#postal_code').val(postal_code);

                    }
                    $('#add_sma_name').val($("#sma_name_" + getSequence).html());

                    $('#hidden_sma_id').val(getSequence);

                    $('#old_sma_name').val($('#sma_name_' + getSequence).html());

                    var to_edit_country_id = $('#country_name_' + getSequence).attr('cntry_id');

                    $('#add_sma_country_1').find($('#country_option_' + to_edit_country_id)).prop("selected", "selected");

                    $('#back_sma_location').css("display", "inline-block");


                    $("html, body").animate({scrollTop: 0}, "slow");

                    $('#refresh_overlay').css("display", "none");

                }, 6800);

            }//if update


        },

        error: function () {

            $('#refresh_overlay').css("display", "none");

            alert("Some Error");
        }

    });


}

function getSmaCity(county_id, status) {

    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);


    }

    var sma_id = $('#hidden_sma_id').val();
    if(sma_id !== undefined && sma_id > 0){
        var param = {
            action : 'getSmaCity',
            county_id : county_id.join(),
            user_id : userInfoObj[0].id,
            sma_id : sma_id
        };
    } else {
        var param = {
            action : 'getSmaCity',
            county_id : county_id.join(),
            user_id : userInfoObj[0].id
        };
    }


    console.log(param);
    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: param,

        success: function (response) {

            var smaCountry = [];

            var responseObject = '';

            var j = 0;

            if (typeof(response) == "string") {

                responseObject = JSON.parse(response);

            }

            var responseHTML = '';

            if (responseObject.data != "null" && responseObject.data != null) {

                $('#sma_county_check').find('img').attr("src", "images/green_check.png");

                $.each(responseObject.data, function (i, data) {

                    var country_id = responseObject.data[i]['country_id'];

                    var country_name = responseObject.data[i]['country_name'];

                    var state_id = responseObject.data[i]['state_id'];

                    var state_name = responseObject.data[i]['state_name'];

                    var county_id = responseObject.data[i]['county_id'];

                    var county_name = responseObject.data[i]['county_name'];

                    responseHTML += '<optgroup label="' + county_name + '">';


                    /* new code start here */

                    var getCityName = ''
                    var getCityIDArray = [];
                    var getArrayValue = [];
                    var getCityID = '';

                    $.each(responseObject.data[i]['city'], function (index, result) {

                        if (result['id'] != "null" && result['id'] != null) {


                            getCityName = result.city_name;

                            if ($.inArray(getCityName, getArrayValue) == -1) {


                                getArrayValue.push(getCityName);
                                if (getCityID != '') {
                                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));
                                }
                                getCityID = '';


                            }
                            getCityID += result.id + ',';


                        }

                    });


                    var count = 0;
                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));

                    /* new code end here  */

                    $.each(getArrayValue, function (index, result12) {

                        responseHTML += '<option value="' + getCityIDArray[count] + 'a@a' + result12 + '" allCityName="' + result12 + '" id="county_option_' + getCityIDArray[count] + '" seq="' + getCityIDArray[count] + '" cnty_detail="' + county_id + 'a@a' + county_name + '" st_detail="' + state_id + 'a@a' + state_name + '" cntry_detail="' + country_id + 'a@a' + country_name + '">' + result12 + '</option>';
                        count++;

                    })


                    responseHTML += '</optgroup>';


                });


            }

            $('#add_sma_city_1').html(responseHTML);

            setTimeout(function () {

                //getState(cityAndState);

                $("#add_sma_city_1").val('');

                $("#add_sma_city_1").multiselect('destroy');

                $('#add_sma_city_1').multiselect({

                    maxHeight: 800,

                    buttonWidth: '155px',

                    includeSelectAllOption: true

                });

                $('#add_sma_city_1').next().find('.multiselect').css("width", "145px");

                // Code For Selecting Optgroup


                if (status != 1) {

                    $('#refresh_overlay').css("display", "none");

                }

            }, 400);

            $('#postal_code').val('');

            if (status == 1) {

                var getSequence = $('#add_sma_location').attr("sma_id");

                setTimeout(function () {


                    var editedState = $('#sma_state_' + getSequence).attr('stt_val');

                    editedState = editedState.split(',');

                    $("#add_sma_state_1").val(editedState);

                    $("#add_sma_state_1").multiselect('refresh');

                    var editedCounty = $('#sma_county_' + getSequence).attr('cnty_val');

                    editedCounty = editedCounty.split(',');

                    $("#add_sma_county_1").val(editedCounty);

                    $("#add_sma_county_1").multiselect('refresh');

                    var editedCity = $('#sma_city_' + getSequence).attr('city_val');

                    editedCity = editedCity.split(',');
                    var finalCityName = [];
                    $.each(editedCity, function (index, result) {

                        var getSelectBoxTextValue = result.split('a@a');

                        if ($.inArray(getSelectBoxTextValue[1], finalCityName) == -1 && getSelectBoxTextValue[1] != undefined) {

                            finalCityName.push(getSelectBoxTextValue[1]);


                        }


                    });


                    /* text end here */


                    var varValue = finalCityName;
                    var j = 0, size2 = varValue.length;
                    $options2 = $('#add_sma_city_1 option');
                    for (j; j < size2; j++) {

                        $options2.filter('[allcityname="' + varValue[j] + '"]').prop('selected', true);


                    }


                    var valArr = editedCity; // array of option values
                    var i = 0, size = valArr.length, // index and array size declared here to avoid overhead
                        $options = $('#add_sma_city_1 option'); // options cached here to avoid overhead of fetching inside loop

                    // run the loop only for the given values
                    for (i; i < size; i++) {
                        // filter the options with the specific value and select them
                        // alert(valArr[i]);
                        $options.filter('[value="' + valArr[i] + '"]').prop('selected', true);
                    }


                    $("#add_sma_city_1").multiselect("refresh");


                    var postal_code = $('#sma_postal_' + getSequence).attr('seq');

                    if (postal_code != undefined && postal_code != "undefined") {

                        $('#postal_code').val(postal_code);

                    }
                    $('#add_sma_name').val($("#sma_name_" + getSequence).html());

                    $('#hidden_sma_id').val(getSequence);

                    $('#old_sma_name').val($('#sma_name_' + getSequence).html());

                    var to_edit_country_id = $('#country_name_' + getSequence).attr('cntry_id');

                    $('#add_sma_country_1').find($('#country_option_' + to_edit_country_id)).prop("selected", "selected");

                    $('#back_sma_location').css("display", "inline-block");


                    $("html, body").animate({scrollTop: 0}, "slow");

                    $('#refresh_overlay').css("display", "none");

                }, 6800);

            }//if update


        },

        error: function () {

            $('#refresh_overlay').css("display", "none");

            alert("Some Error");
        }

    });

}


function getPostal(city_id, status) {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }

    $.ajax({
        url: _SERVICEPATHSma,
        type: 'POST',
        data: "action=getPostal&city_id=" + city_id,
        success: function (response) {
            var smaCountry = [];
            var responseObject = '';
            var j = 0;
            if (typeof(response) == "string") {
                responseObject = JSON.parse(response);
            }
            var respsonseHTML = ''
            $('#sma_city_check').find('img').attr("src", "images/green_check.png");
            if (responseObject.data != "null" && responseObject.data != null) {
                respsonseHTML = responseObject.data;
            }
            respsonseHTML = respsonseHTML.replace(/,\s*$/, "");
            if(respsonseHTML != ''){
                $('#postal_code').val(respsonseHTML);
            }
            if (status != 1) {
                $('#refresh_overlay').css("display", "none");
            }
            $('#add_sma_location').removeAttr('disabled');
            $('#update_sma_location').removeAttr('disabled');
            setTimeout(function () {

            }, 400);

        },
        error: function () {
            $('#refresh_overlay').css("display", "none");
            alert("Some Error");
        }

    });

}


function setSmaLocation(selectedCityObject, isDefault) {

    var bttn_html = $('#add_sma_location').html();

    $('#add_sma_location').attr("disabled", "true");

    $('#add_sma_location').html("Processing....");

    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);

    }

    if (isDefault == 1) {

        $.ajax({

            url: _SERVICEPATHSma,

            type: 'POST',

            data: "action=checkDefaultSma&user_id=" + userInfoObj[0].id,

            success: function (response) {

                var responseObj = response;

                if (typeof(response) == "string") {

                    responseObj = JSON.parse(response);


                }

                if (responseObj.data != null && responseObj.data != false && responseObj.data != undefined && responseObj.data != "undefined") {


                    var res = confirm("Another SMA is Already Set as Default.Do You Really want To make it as Default");

                    if (res == true) {

                        var sma_id = responseObj.data;

                        $.ajax({

                            url: _SERVICEPATHSma,

                            type: 'POST',

                            data: "action=updateDefaultSma&sma_id=" + sma_id + "&user_id=" + userInfoObj[0].id,

                            success: function (response) {

                                $.ajax({

                                    url: _SERVICEPATHSma,

                                    type: 'POST',

                                    data: "action=setSmaLocation&selected_loc=" + selectedCityObject + "&user_id=" + userInfoObj[0].id + "&isDefault=1",

                                    success: function (response) {


                                        $('#add_sma_location').removeAttr("disabled");

                                        $('#add_sma_location').html("Submit");

                                        location.reload();


                                    },

                                    error: function () {

                                        alert("Some Error");

                                        $('#add_sma_location').removeAttr("disabled");

                                        $('#add_sma_location').html(bttn_html);

                                    }

                                });

                            },

                            error: function () {

                                alert("Some Error");

                                $('#add_sma_location').removeAttr("disabled");

                                $('#add_sma_location').html(bttn_html);

                            }

                        });

                    }//if


                    else {

                        $.ajax({

                            url: _SERVICEPATHSma,

                            type: 'POST',

                            data: "action=setSmaLocation&selected_loc=" + selectedCityObject + "&user_id=" + userInfoObj[0].id + "&isDefault=1",

                            success: function (response) {


                                $('#add_sma_location').removeAttr("disabled");

                                $('#add_sma_location').html("Submit");

                                location.reload();

                                // getSmaLocList();

                            },

                            error: function () {

                                alert("Some Error");

                                $('#add_sma_location').removeAttr("disabled");

                                $('#add_sma_location').html(bttn_html);

                            }

                        });

                    }

                }

                else {

                    $.ajax({

                        url: _SERVICEPATHSma,

                        type: 'POST',

                        data: "action=setSmaLocation&selected_loc=" + selectedCityObject + "&user_id=" + userInfoObj[0].id + "&isDefault=1",

                        success: function (response) {

                            $('#add_sma_location').removeAttr("disabled");
                            location.reload();


                            // getSmaLocList();

                        },

                        error: function () {

                            alert("Some Error");

                            $('#add_sma_location').removeAttr("disabled");

                            $('#add_sma_location').html(bttn_html);

                        }

                    });

                }

            }//success

        });

    }//isDefault==1

    else {

        $.ajax({

            url: _SERVICEPATHSma,

            type: 'POST',

            data: "action=setSmaLocation&selected_loc=" + selectedCityObject + "&user_id=" + userInfoObj[0].id + "&isDefault=0",

            success: function (response) {
                $('#refresh_overlay').css("display", "none");

                $('#add_sma_location').removeAttr("disabled");

                $('#add_sma_location').html("Submit");

                location.reload();

                // getSmaLocList();

            },

            error: function () {
                $('#refresh_overlay').css("display", "none");

                alert("Some Error");

                $('#add_sma_location').removeAttr("disabled");

                $('#add_sma_location').html(bttn_html);

            }

        });

    }


}


// var _PAGENO = 1;
// var _PAGINATION = 0;

var pageNo = 1;
var perPage = 1;
var keyword = '';
function getSmaLocListWithPage(trigger) {
    var trigger = $(trigger);
    pageNo = trigger.val();
    getSmaLocList();
}
function getSmaLocListWithPerPage(trigger) {
    var trigger = $(trigger);
    perPage = trigger.val();
    pageNo = 1;
    getSmaLocList();
}

function getStateWithSearch(trigger) {
    var trigger = $(trigger);
    keyword = trigger.val();
    getSmaLocList();
}

function getSmaLocList() {
    $('#refresh_overlay').css("display", "block");
    var getUserInfo = window.localStorage.getItem("companyInfo");
    var getUserInfoObj = getUserInfo;
    if (typeof(getUserInfo) == "string") {
        getUserInfoObj = JSON.parse(getUserInfo);
    }
    var paramPost = {
        action: 'getSmaLocList',
        user_id: getUserInfoObj[0].id,
        pageNo: pageNo,
        keyword: keyword,
        perPage: perPage
    };
    $.ajax({
        url: _SERVICEPATHSma,
        type: 'POST',
        data: paramPost,
        success: function (response) {
            var responseObj = response;
            var responseHTML = '';
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.data[1] !== undefined) {
                var totalC = responseObj.data[1];
                var limit = perPage;
                var totalPage = totalC / limit;
                totalPage = Number.isInteger(totalPage) ? totalPage : (parseInt(totalPage) + 1);
                totalPage = parseInt(totalPage);
                var paginatoionC = '';
                for (var pg = 0; pg < totalPage; pg++) {
                    if (parseInt(pageNo - 1) === parseInt(pg)) {
                        paginatoionC += '<option value="' + (pg + 1) + '" selected>' + (pg + 1) + '</option>';
                    } else {
                        paginatoionC += '<option value="' + (pg + 1) + '">' + (pg + 1) + '</option>';
                    }
                }
                $('#cityPagination').html(paginatoionC);
            }
            if (responseObj.data[0] !== undefined) {
                responseObj.data = responseObj.data[0];
                for (var i = 0; i < responseObj.data.length; i++) {
                    var responseState = '';
                    var responseCounty = '';
                    var responseCity = '';
                    var stt_id = '';
                    var stt_val = '';
                    var cnty_id = '';
                    var cnty_val = '';
                    var city_id = '';
                    var city_val = '';
                    var sma_id_strings = '';
                    sma_id_strings += responseObj.data[i]['sma_id'] + ',';
                    var postal = responseObj.data[i]['Postal'];
                    if (postal != "undefined" && postal != undefined) {
                        postal = postal.replace(/,\s*$/, "");
                        postal = postal.split(",")
                        var postalArr = [];
                        $.each(postal, function (index, postalValue) {
                            if ($.inArray($.trim(postalValue), postalArr) == -1) {
                                postalArr.push(postalValue);
                            }
                        });

                        postal = postalArr.join(", ");
                    }

                    $.each(responseObj.data[i]['State'], function (index, data) {
                        responseState += '<option selected disabled>' + responseObj.data[i]['State'][index]['state_name'] + '</option>';
                        stt_id += responseObj.data[i]['State'][index]['state_id'] + ',';
                        stt_val += responseObj.data[i]['State'][index]['state_id'] + 'a@a' + responseObj.data[i]['State'][index]['state_name'] + ',';

                    });
                    $.each(responseObj.data[i]['County'], function (index, data) {
                        if (responseObj.data[i]['County'][index]['county_name'] != "null" && responseObj.data[i]['County'][index]['county_name'] != null && responseObj.data[i]['County'][index]['county_name'] != undefined && responseObj.data[i]['County'][index]['county_name'] != "undefined"
                            && responseObj.data[i]['County'][index]['county_id'] != 0) {
                            responseCounty += '<option selected disabled>' + responseObj.data[i]['County'][index]['county_name'] + '</option>';
                            cnty_id += responseObj.data[i]['County'][index]['county_id'] + ',';
                            cnty_val += responseObj.data[i]['County'][index]['county_id'] + 'a@a' + responseObj.data[i]['County'][index]['county_name'] + ',';
                            ;
                        }

                    })

                    var getArrayValue = [];
                    var getCityName = '';
                    var getCityIDArray = [];
                    var getCityID = '';
                    $.each(responseObj.data[i]['City'], function (index, result) {
                        if (result.city_name != "null" && result.city_name != null && result.city_name != undefined && result.city_name != "undefined" && result.city_id != 0) {

                            getCityName = result.city_name;
                            if ($.inArray(getCityName, getArrayValue) == -1) {
                                getArrayValue.push(getCityName);
                                if (getCityID != '') {
                                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));
                                }

                                getCityID = '';
                            }

                            getCityID += result.city_id + ',';
                        }

                    });
                    var count = 0;
                    getCityIDArray.push(getCityID.substring(0, getCityID.length - 1));

                    $.each(getArrayValue, function (index, result) {
                        responseCity += '<option selected disabled>' + result + '</option>';
                        city_id += getCityIDArray[count] + ',';
                        city_val += getCityIDArray[count] + 'a@a' + result + ',';
                        count++;
                    });
                    responseHTML += ' <tr class="ratesetup_point_to_point_airport" id="smaloc_' + responseObj.data[i]['sma_id'] + '">' +
                        '<td style="display: none;">' + postal + '</td> ' +
                        '<td><span id="sma_name_' + responseObj.data[i]['sma_id'] + '"  ' +
                        ' sm_id="' + responseObj.data[i]['sma_id'] + '">'+ responseObj.data[i]['Country'][0]['sma_name'] + '</span></td>' +
                        '<td> <span id="country_name_' + responseObj.data[i]['sma_id'] + '"  ' +
                        ' cntry_id="' + responseObj.data[i]['Country'][0]['country_id'] + '"> ' +
                        ' ' + responseObj.data[i]['Country'][0]['country_name'] + '</span> </td> ' +
                        '<td>' +
                        '<select id="sma_state_' + responseObj.data[i]['sma_id'] + '" class="form-control select-multi" ' +
                        'style="background-color:#b0b5b9;color:#fff;" multiple="multiple" ' +
                        'stt_id="' + stt_id + '" stt_val="' + stt_val + '" >' + responseState + '</select></td> ' +
                        '<td>' +
                        '<select id="sma_county_' + responseObj.data[i]['sma_id'] + '" class="form-control select-multi" ' +
                        'style="background-color:#b0b5b9;color:#fff;" multiple="multiple" ' +
                        'cnty_id="' + cnty_id + '" cnty_val="' + cnty_val + '" >' + responseCounty + '</select></td> ' +
                        '<td>' +
                        '<select id="sma_city_' + responseObj.data[i]['sma_id'] + '" class="form-control select-multi" ' +
                        'style="background-color:#b0b5b9;color:#fff;" multiple="multiple" city_id="' + city_id + '" ' +
                        'city_val="' + city_val + '">' + responseCity + '</select></td> ' +
                        '<td style=" width: 21%; text-align: center;"> ' +
                        '<button id="sma_postal_' + responseObj.data[i]['sma_id'] + '" ' +
                        'class="btn btn-primary postal_popup" seq="' + postal + '">View</button> </td>' +
                        '<td> <div class="as" ng-show="!rowform.$visible"> ' +
                        '<a class="btn btn-xs sma_loc_onedit" ' +
                        'is_default=' + responseObj.data[i]['Country'][0]['isDefault'] + ' ' +
                        'seq=' + responseObj.data[i]['sma_id'] + '>Edit</a> <a class="btn btn-xs sma_loc_ondelete"  ' +
                        'seq=' + responseObj.data[i]['sma_id'] + '>Delete</a> </div> <a class="_dataFull" style="display: none">'+JSON.stringify(responseObj.data[i])+'</a></td> ' +
                        '</tr> ';
                }
                setTimeout(function () {
                    var sma_array = sma_id_strings.split(',');
                    $(".select-multi").multiselect('destroy');
                    $('.select-multi').multiselect({
                        maxHeight: 200,
                        buttonWidth: '155px',
                        includeSelectAllOption: true

                    });
                }, 400);
            }
            else {
                responseHTML = '<tr><td colspan="1">Data not Found.</td></tr>';
            }
            $('#sma_loc_list').html(responseHTML);
            $('#refresh_overlay').css("display", "none");

        }

    });

}

$(function () {
    getSmaLocList();
    /*document.onscroll = function () {
        if ($(window).scrollTop() + $(window).height() + 200 > $(document).height()) {
            if (_PAGENO !== 0) {
                getSmaLocList(_PAGENO);
            }
        }
    };*/

    $('body').on("click",'.sma_loc_ondelete', function () {
        var getSequence = $(this).attr("seq");
        var res = confirm("Do You Really Want To Delete SMA");
        if (res == true) {
            $('#refresh_overlay').css("display", "block");
            deletesmaLoc(getSequence);
        }
        else {

        }


    });
    $('body').on("click",'.sma_loc_onedit', function () {
        // $('#add_sma_location').html("Update");
        $('#add_sma_location').hide();
        $('#update_sma_location').show();
        $('#refresh_overlay').css("display", "block");
        var is_default = $(this).attr("is_default");
        if (is_default == 1) {
            document.getElementById('set_default').checked = true;
        }
        else {
            document.getElementById('set_default').checked = false;
        }

        var trigger = $(this);
        var data = trigger.closest('td').find('._dataFull').html();
        data = JSON.parse(data);

        var getSequence = $(this).attr("seq");
        $('#add_sma_location').attr("sma_id", getSequence);

        $('#add_sma_country_1').val(data.Country[0].country_id+'a@a'+data.Country[0].country_name);

        var states = [];
        $.each(data.State, function (i, v) {
            states.push(v.state_id+'a@a'+v.state_name);
        });
        getSmaState(data.Country[0].country_id, 1, states);

        var selectedStateObject = [];
        var selectedState = $('#sma_state_' + getSequence).attr('stt_id');
        selectedState = selectedState.split(',');
        for (var l = 0; l < selectedState.length; l++) {
            if (selectedState[l] !== '' && selectedState[l] !== undefined) {
                selectedStateObject.push(selectedState[l]);
            }

        }
        var county = [];
        $.each(data.County, function (i, v) {
            county.push(v.county_id+'a@a'+v.county_name);
        });
        getSmaCounty(selectedStateObject, 1, county);


        var selectedCounty = $('#sma_county_' + getSequence).attr('cnty_id');
        selectedCounty = selectedCounty.split(',');
        var selectedCountyObject = [];
        for (var b = 0; b < selectedCounty.length; b++) {
            if (selectedCounty[b] != '' && selectedCounty[b] != undefined) {
                selectedCountyObject.push(selectedCounty[b]);
            }
        }
        $('#sma_county_check').find('img').attr("src", "images/green_check.png");


        console.log(data);
        setTimeout(function () {
            var checkCity = [];
            var sma_city = $('#sma_city_'+data.sma_id).attr('city_val');
            sma_city = sma_city.split(',');
            $.each(sma_city, function (i, v) {
                var vC = v.split('a@a');
                if(vC[1] !== undefined){
                    checkCity.push(vC[0]);
                }
            });
            var CityCheck = [];
            var CityChecked = [];
            var CityCheckedPostal = [];
            var responseHTML = '';
            $.each(data.City, function (i, v) {
                if(v !== ''){
                    var index = CityCheck.map(function(e) { return e.allcityname; }).indexOf(v.city_name);
                    if(index == -1){
                        var eachCity = {
                            cityId : 'county_option_'+v.city_id,
                            allcityname : v.city_name,
                            seq : v.city_id,
                            cnty_detail : v.county_id+'a@a'+v.county_name,
                            st_detail : v.state_id+'a@a'+v.state_name,
                            cntry_detail : v.country_id+'a@a'+v.country_name,
                            postal_code : v.postal_code
                        };
                        CityCheck.push(eachCity);
                    } else {
                        CityCheck[index].cityId += ','+v.city_id;
                        CityCheck[index].seq += ','+v.city_id;
                        CityCheck[index].postal_code += ','+v.postal_code;
                    }
                    /*if(CityCheck.indexOf(v.city_name) === -1){
                        CityCheck.push(v.city_name);
                        var v_name = v.city_id+'a@a'+v.city_name;
                        CityChecked.push(v_name);
                        // if(checkCity.indexOf(v.city_id) > -1){
                            // id="county_option_281156" seq="281156"
                            responseHTML += '<option seq="'+v.postal_code+'" value="' + v_name + '" cnty_detail="'+v.county_id+'a@a'+v.county_name+'" st_detail="'+v.state_id+'a@a'+v.state_name+'" cntry_detail="'+v.country_id+'a@a'+v.country_name+'">' + v.city_name + '</option>';
                        // } else {
                        //     responseHTML += '<option seq="'+v.postal_code+'" value="' + v_name + '" cnty_detail="'+v.county_id+'a@a'+v.county_name+'" st_detail="'+v.state_id+'a@a'+v.state_name+'" cntry_detail="'+v.country_id+'a@a'+v.country_name+'">' + v.city_name + '</option>';
                        // }
                    }*/
                }
            });
            $.each(CityCheck, function(i, v){
                var vl = v.seq+'a@a'+v.allcityname;
                CityChecked.push(vl);
                CityCheckedPostal.push(v.postal_code);
                responseHTML += '<option id="'+v.cityId+'" value="' + vl + '" allcityname="'+v.allcityname+'" seq="'+v.seq+'" cnty_detail="'+v.cnty_detail+'" st_detail="'+v.st_detail+'" cntry_detail="'+v.cntry_detail+'">' + v.allcityname + '</option>';
            });
            $('#add_sma_city_1').html(responseHTML);
            $('#hidden_sma_id').val(data.sma_id);
            $('#add_sma_name').val($("#sma_name_" + getSequence).html());
            setTimeout(function () {
                $('#add_sma_city_1').val(CityChecked);
                $("#add_sma_city_1").multiselect('destroy');
                $('#add_sma_city_1').multiselect({

                    maxHeight: 800,

                    buttonWidth: '155px',

                    includeSelectAllOption: true

                });
                $('#add_sma_city_1').next().find('.multiselect').css("width", "145px");
                $('#refresh_overlay').css("display", "none");
                $('#sma_city_check').find('img').attr("src", "images/green_check.png");
                // var postal_code = $('#sma_postal_' + getSequence).attr('seq');
                var postal_code = CityCheckedPostal.join();
                $('#postal_code').val(postal_code);
            }, 1000);

            $('#add_sma_name').val($("#sma_name_" + getSequence).html());
            $('#hidden_sma_id').val(getSequence);
            $('#old_sma_name').val($('#sma_name_' + getSequence).html());
            var to_edit_country_id = $('#country_name_' + getSequence).attr('cntry_id');
            $('#add_sma_country_1').find($('#country_option_' + to_edit_country_id)).prop("selected", "selected");
            $('#back_sma_location').css("display", "inline-block");
            $("html, body").animate({scrollTop: 0}, "slow");
        }, 2000);

    });
    $('body').on('click',"#update_sma_location", function () {
        // alert('Development Running. Please test later');
        $('#refresh_overlay').show();
        var sma_name = $('#add_sma_name').val();
        sma_name = sma_name.trim();
        var isDefault = 0;
        if (document.getElementById('set_default').checked === true) {
            isDefault = 1;
        }
        var postal_code = $('#postal_code').val();
        var selectedCity = $('#add_sma_city_1 option:selected');
        var selectedCounty = $('#add_sma_county_1 option:selected');
        var selectedState = $('#add_sma_state_1 option:selected');
        var selectedCountry = $('#add_sma_country_1').val();
        var selectedStateObject = [];
        var selectedCountryObject = [];
        var selectedCountyObject = [];
        var selectedCityObject = [];
        var add_sma_city = '';
        var add_sma_state = '';
        var add_sma_country = '';
        var sma_json = "";
        var add_sma_county = $(this).val();
        var postal_code = $('#postal_code').val();
        if (selectedCity.length !== 0 && selectedCounty.length !== 0 && selectedState.length !== 0) {
            $(selectedCity).each(function (index, selectedCity) {
                add_sma_city = $(this).val();
                add_sma_county = $(this).attr('cnty_detail');
                add_sma_county = add_sma_county !== undefined ? add_sma_county.split('a@a') : '';
                add_sma_city = add_sma_city.split('a@a');
                add_sma_state = $(this).attr('st_detail');
                add_sma_state = add_sma_state !== undefined ? add_sma_state.split('a@a') : '';
                add_sma_country = $(this).attr('cntry_detail');
                add_sma_country = add_sma_country !== undefined ? add_sma_country.split('a@a') : '';
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: add_sma_county[0],
                    county_name: add_sma_county[1],
                    city_id: add_sma_city[0],
                    city_name: add_sma_city[1],
                    postal_code: postal_code,
                    sma_name: sma_name,
                    isDefault: isDefault
                };

                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }
        else if (selectedCounty.length !== 0 && selectedState.length !== 0) {
            $(selectedCounty).each(function (index, selectedCounty) {
                add_sma_county = $(this).val();
                add_sma_county = add_sma_county.split('a@a');
                add_sma_state = $(this).attr('st_detail');
                add_sma_state = add_sma_state.split('a@a');
                add_sma_country = selectedCountry;
                add_sma_country = add_sma_country.split('a@a');
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: add_sma_county[0],
                    county_name: add_sma_county[1],
                    city_id: 0,
                    city_name: '',
                    postal_code: '',
                    sma_name: sma_name,
                    isDefault: isDefault
                };
                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }
        else {
            $(selectedState).each(function (index, selectedState) {
                add_sma_state = $(this).val();
                add_sma_state = add_sma_state.split('a@a');
                add_sma_country = selectedCountry;
                add_sma_country = add_sma_country.split('a@a');
                sma_json = {
                    country_id: add_sma_country[0],
                    country_name: add_sma_country[1],
                    state_id: add_sma_state[0],
                    state_name: add_sma_state[1],
                    county_id: 0,
                    county_name: '',
                    city_id: 0,
                    city_name: '',
                    postal_code: '',
                    sma_name: sma_name,
                    isDefault: isDefault
                };
                selectedCityObject.push(JSON.stringify(sma_json));
            });
        }

        console.log(selectedCityObject, isDefault);
        var userInfo = window.localStorage.getItem("companyInfo");
        userInfo = JSON.parse(userInfo);
        var sma_id = $('#hidden_sma_id').val();
        var postal_code = $('#postal_code').val();
        postal_code = postal_code.split(',');

        $.ajax({
            url: _SERVICEPATHSma,
            type: 'POST',
            data : {
                action : 'updateSetSmaLocation',
                selected_loc : selectedCityObject,
                user_id : userInfo[0].user_id !== undefined ? userInfo[0].user_id : userInfo[0].id,
                isDefault : isDefault,
                postal_code : postal_code,
                sma_id : sma_id
            },
            success: function (response) {
                $('#refresh_overlay').hide();
                $('#add_sma_location').removeAttr("disabled");
                $('#update_sma_location').removeAttr("disabled");
                alert("SMA information updated successfully");
                window.location.href = '';
            },
            error: function () {
                $('#refresh_overlay').hide();
                alert("Some Error");
                $('#add_sma_location').removeAttr("disabled");
                $('#update_sma_location').removeAttr("disabled");
            }

        });
    });

    //##################################  Edit SMA Ends ##############################//

    $('body').on("click",'.postal_popup', function () {

        $('#postal_code_modal').css("display", "block");
        var view_postal_code = $(this).attr('seq');
        if (view_postal_code == undefined || view_postal_code == "undefined" || view_postal_code == "") {
            view_postal_code = "No Postal Code Exist."
        }
        $('.show_postal').html(view_postal_code);
    });
    $().on("click",'#close_postal_popup', function () {
        $('#postal_code_modal').hide();

    })
});

function getSMA() {

    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);


    }


    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=getSMA&user_id=" + userInfoObj[0].id,

        success: function (response) {

            var responseObj = response;

            var responseHTML = '';

            if (typeof(response) == "string") {

                responseObj = JSON.parse(response);


            }


            for (var i = 0; i < responseObj.data.length; i++) {

                responseHTML += '<div class="acc-content" style="display:block"><div><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle ng-binding" ng-click="isOpen = !isOpen" accordion-transclude="heading"><div class="row ng-scope  show_details_sma" seq="' + responseObj.data[i].id + '"><div class="col-sm-6"><span class="vehicle-name text-black ng-binding"><i class="fa fa-truck"></i>' + responseObj.data[i].sma_name + '</span></div><div class="change_icon col-xs-6"><i class="pull-right glyphicon text-muted glyphicon-chevron-right"></i></div></div></a></h4></div><div class="show_data_sma" id="show_data_sma_' + responseObj.data[i].id + '"><div ><accordion-heading class="ng-scope"></accordion-heading><div class="row"><div class="col-sm-3" style="margin-left: 406px; margin-top: 12px;">Select Location CSV to upload:</div><div class="col-sm-3"><input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" value="browse" id="smaCsvFile_' + responseObj.data[i].id + '" style="padding: 1%;" seq="' + responseObj.data[i].id + '"></div><div class="col-sm-3"><input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:1%;margin-left:396%;margin-top: -20%;" onclick="uploadSmaCsv()"></div></div><div class="row mb-sm ng-scope"><div class="col-sm-12"><div class="top-links"><a class="btn btn-default popup_edit1_airport add_color_rate "><i class="fa fa-plus"></i> Add Location</a></div></div></div><div style="overflow:scroll;"><table class="table table-condensed ng-scope"><thead ><tr ><th>Country</th><th>County</th><th>State</th><th>City</th><th>Postal Code</th><th>Action</th><th class="table-action"><span class="table-action"></span></th></tr></thead><tbody id="sma_loc_list_' + responseObj.data[i].id + '"></tbody></table></div></div></div></div></div>';

            }


            setTimeout(function () {

                pointToPointAllOnclickFunction();

            }, 800);


        },

        error: function () {

            alert("Some Error");
        }

    });

}


$('#add_sma_details').on("click", function () {
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);
    }
    var sma_name = $('#add_sma_name').val();
    var sma_id = $('#hidden_sma_id').val();
    sma_name = sma_name.trim();
    if (sma_name != "" && sma_name != null) {
        $('#add_sma_details').attr("disabled", "true");
        $('#add_sma_details').html("Processing....");

        var paramData = {
            action: 'checkUniqueSma',
            sma_name: sma_name,
            user_id: userInfoObj[0].id
        };

        $.ajax({
            url: _SERVICEPATHSma,
            type: 'POST',
            data: paramData,
            success: function (response) {
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                if (responseObj.data[0].id != "null" && responseObj.data[0].id != null && responseObj.data[0].id != undefined) {
                    alert("SMA name already exists");
                    $('#add_sma_details').removeAttr("disabled");
                    $('#add_sma_details').html("Save");
                }
                else {
                    $.ajax({
                        url: _SERVICEPATHSma,
                        type: 'POST',
                        data: "action=setSma&sma_name=" + sma_name + "&user_id=" + userInfoObj[0].id,
                        success: function (response) {
                            $('#add_sma_details').removeAttr("disabled");
                            $('#add_sma_details').html("Save");
                            $('#add_sma').hide();
                            getSMA();
                        },
                        error: function () {
                            alert("Some Error");
                            $('#add_sma_details').removeAttr("disabled");
                            $('#add_sma_details').html("Save");
                        }
                    });
                }
            },
            error: function () {
                alert("Some Error");
                $('#add_sma_details').removeAttr("disabled");
                $('#add_sma_details').html("Save");
            }
        });

    }
    else {
        alert("Please Enter Sma Name");
    }

});


function updateSmaLocation(country_name, country_id, county_id, county_name, state_id, state_name, city_id, city_name, postal_code, sma_id) {

    $('.save_sma_details').attr("disabled", "true");

    $('.save_sma_details').html("Processing.....");

    $.ajax({

        url: _SERVICEPATHSma,

        type: 'POST',

        data: "action=updateSmaLocation&country_id=" + country_id + "&country_name=" + country_name + "&county_id=" + county_id + "&county_name=" + county_name + "&state_id=" + state_id + "&state_name=" + state_name + "&city_id=" + city_id + "&city_name=" + city_name + "&postal_code=" + postal_code + "&id=" + sma_id,

        success: function (response) {

            $('.save_sma_details').removeAttr("disabled");

            $('.save_sma_details').html("Update");

            // getSmaLocList();

        },

        error: function () {

            alert("Some Error");

            $('.save_sma_details').removeAttr("disabled");

            $('.save_sma_details').html("Update");

        }

    });

}


function checkUniqueSma(sma_name, type, selectedCityObject, sma_id, isDefault) {

    sma_name = sma_name.trim();

    var userInfo = window.localStorage.getItem("companyInfo");

    var userInfoObj = userInfo;

    if (typeof(userInfo) == "string") {

        userInfoObj = JSON.parse(userInfo);


    }

    if (sma_name != "" && sma_name != null) {

        if(sma_id !== undefined && sma_id !== null && sma_id !== ''){
            var paramData = {
                action : 'checkUniqueSma',
                sma_name : sma_name,
                user_id : userInfoObj[0].id,
                sma_id : sma_id
            };
        } else {
            var paramData = {
                action : 'checkUniqueSma',
                sma_name : sma_name,
                user_id : userInfoObj[0].id,
            };
        }
        $.ajax({

            url: _SERVICEPATHSma,

            type: 'POST',

            data: paramData,

            success: function (response) {

                var responseObj = response;

                if (typeof(response) == "string") {

                    responseObj = JSON.parse(response);


                }


                if (responseObj.data[0].id != "null" && responseObj.data[0].id != null && responseObj.data[0].id != undefined) {

                    alert("SMA name already exists");

                }

                else {

                    if (type == "Update") {

                        deletesmaLoc(sma_id);

                        setSmaLocation(JSON.stringify(selectedCityObject), isDefault);

                    }

                    else if (type == "add") {


                        setSmaLocation(JSON.stringify(selectedCityObject), isDefault);

                    }

                }


            },

            error: function () {

                alert("Some Error");


            }

        })//outer ajax

    }//if smaname not null


}

$(function(){
    if($('form').length > 0){

        $('form')[0].reset();
        $('#add_sma_state_1').on('change', function(){
            $('#sma_state_check').find('img').attr("src", "images/ok_checkmark_red_T.png");
            $('#sma_city_check').find('img').attr("src", "images/checked-yellow.png");
            $('#sma_county_check').find('img').attr("src", "images/checked-yellow.png");

            $('#add_sma_county_1').val('');
            $('#add_sma_city_1').val('');
            $('#postal_code').val('');
            $('#add_sma_location').attr('disabled', 'true');
            $('#update_sma_location').attr('disabled', 'true');
        });
        $('#add_sma_county_1').on('change', function(){
            // $('#sma_state_check').find('img').attr("src", "images/ok_checkmark_red_T.png");
            $('#sma_city_check').find('img').attr("src", "images/checked-yellow.png");
            $('#sma_county_check').find('img').attr("src", "images/checked-yellow.png");

            // $('#add_sma_county_1').val('');
            $('#add_sma_city_1').val('');
            $('#postal_code').val('');
            $('#add_sma_location').attr('disabled', 'true');
            $('#update_sma_location').attr('disabled', 'true');
        });
        $('#add_sma_city_1').on('change', function(){
            // $('#sma_state_check').find('img').attr("src", "images/ok_checkmark_red_T.png");
            $('#sma_city_check').find('img').attr("src", "images/checked-yellow.png");
            // $('#sma_county_check').find('img').attr("src", "images/checked-yellow.png");

            // $('#add_sma_county_1').val('');
            // $('#add_sma_city_1').val('');
            $('#postal_code').val('');
            $('#add_sma_location').attr('disabled', 'true');
            $('#update_sma_location').attr('disabled', 'true');
        });




        $('#cityPerPage').val(perPage);
        $('#cityPagination').val(pageNo);
        $('#add_sma_location').removeAttr('disabled');
    }
});